local lib_stub = LibStub
local TOCNAME, addon = GroupBulletinBoard_Loader.Main()

if addon.VersionBroadcast then return end

local M = {}

local ace_comm = lib_stub( "AceComm-3.0" )
local comm_prefix = TOCNAME

local function strip_dots( v )
  local result, _ = v:gsub( "%.", "" )
  return result
end

function M.new( db, version )
  local was_in_group = false

  local function version_recently_reminded()
    if not db.last_new_version_reminder_timestamp then return false end

    local time = addon.lua.time()

    -- Only remind once a day
    if time - db.last_new_version_reminder_timestamp > 3600 * 24 then
      return false
    else
      return true
    end
  end

  local function update_group_status()
    was_in_group = addon.api.IsInGroup() or addon.api.IsInRaid()
  end

  local function broadcast_version( target )
    ace_comm:SendCommMessage( comm_prefix, "VERSION::" .. version, target )
  end

  local function broadcast_version_to_the_guild()
    if not addon.api.IsInGuild() then return end
    broadcast_version( "GUILD" )
  end

  local function is_new_version( v )
    local myVersion = tonumber( strip_dots( version ) )
    local theirVersion = tonumber( strip_dots( v ) )

    return theirVersion > myVersion
  end

  local function broadcast_version_to_the_group()
    if not addon.api.IsInGroup() and not addon.api.IsInRaid() then return end
    broadcast_version( addon.api.IsInRaid() and "RAID" or "PARTY" )
  end

  local function on_joined_group()
    if not was_in_group then
      broadcast_version_to_the_group()
    end

    update_group_status()
  end

  local function on_left_group()
    update_group_status()
  end

  -- OnComm(prefix, message, distribution, sender)
  local function on_comm( prefix, message, _, _ )
    if prefix ~= comm_prefix then return end

    local cmd, value = addon.lua.strmatch( message, "^(.*)::(.*)$" )

    if cmd == "VERSION" and is_new_version( value ) and not version_recently_reminded() then
      db.last_new_version_reminder_timestamp = addon.lua.time()
      addon.pretty_print( string.format( "New version (%s) is available!",
        addon.colors.highlight( string.format( "v%s", value ) ) ) )
    end
  end

  local function broadcast()
    broadcast_version_to_the_guild()
    broadcast_version_to_the_group()
    update_group_status()
  end

  ace_comm:RegisterComm( comm_prefix, on_comm )

  return {
    on_joined_group = on_joined_group,
    on_left_group = on_left_group,
    broadcast = broadcast
  }
end

addon.VersionBroadcast = M
return M
