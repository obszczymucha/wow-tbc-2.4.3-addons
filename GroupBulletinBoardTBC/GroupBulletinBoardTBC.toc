## Interface: 20400
## Title: Group Bulletin Board TBC
## Notes: A bulletin board for LFG/LFM messages.
## Version: 1.4
## Author: Obszczymucha
## OG-Author: Vyscî-Whitemane
## OG-Repo: https://github.com/Vysci/LFG-Bulletin-Board
## OG-Git-Tag: v2.64
## DefaultState: enabled
## SavedVariables: GroupBulletinBoardTbcDb
## SavedVariablesPerCharacter: GroupBulletinBoardTbcDbChar
## LoadOnDemand: 0

Libs\Libs.xml
Loader.lua
Backport.lua
VersionBroadcast.lua
InterfaceOptionsFrame.lua
LibGPIOptions.lua
LibGPIMinimapButton.lua
LibGPIToolBox.lua
Chat.lua
Localization.lua
Dungeons.lua
Tags.lua
RequestList.lua
Options.lua
GroupList.Lua
GroupBulletinBoard.lua
GroupBulletinBoard.xml
