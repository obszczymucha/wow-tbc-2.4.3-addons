---@diagnostic disable: undefined-global, undefined-field
local _, addon = GroupBulletinBoard_Loader.Main()

-- This is how you expose WoW API properly.
-- Now you can mock this shit in tests.
-- Everything that uses WoW API should read from GPP.api.
addon.api = _G
addon.lua = {
  format = format,
  time = time,
  strmatch = strmatch
}

if not UnitFullName then UnitFullName = UnitName end

if not SetSize then
  SetSize = function( frame, width, height )
    if not frame then return end

    frame:SetWidth( width )
    frame:SetHeight( height )
  end
end

---@diagnostic disable-next-line: lowercase-global
function wipe( tab )
  for k, _ in pairs( tab ) do tab[ k ] = nil end
  return tab;
end

local chat_frame = addon.api.DEFAULT_CHAT_FRAME

function print( arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16,
                arg17, arg18, arg19, arg20 )
  if (arg1 or arg2 or arg3 or arg4 or arg5 or arg6 or arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg1 ) )
  end
  if (arg2 or arg3 or arg4 or arg5 or arg6 or arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg2 ) )
  end
  if (arg3 or arg4 or arg5 or arg6 or arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg3 ) )
  end
  if (arg4 or arg5 or arg6 or arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg4 ) )
  end
  if (arg5 or arg6 or arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg5 ) )
  end
  if (arg6 or arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg6 ) )
  end
  if (arg7 or arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg7 ) )
  end
  if (arg8 or arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg8 ) )
  end
  if (arg9 or arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg9 ) )
  end
  if (arg10 or arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring( arg10 ) )
  end
  if (arg11 or arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame
        :AddMessage( tostring( arg11 ) )
  end
  if (arg12 or arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage(
      tostring( arg12 ) )
  end
  if (arg13 or arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then
    chat_frame:AddMessage( tostring(
      arg13 ) )
  end
  if (arg14 or arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then chat_frame:AddMessage( tostring( arg14 ) ) end
  if (arg15 or arg16 or arg17 or arg18 or arg19 or arg20) then chat_frame:AddMessage( tostring( arg15 ) ) end
  if (arg16 or arg17 or arg18 or arg19 or arg20) then chat_frame:AddMessage( tostring( arg16 ) ) end
  if (arg17 or arg18 or arg19 or arg20) then chat_frame:AddMessage( tostring( arg17 ) ) end
  if (arg18 or arg19 or arg20) then chat_frame:AddMessage( tostring( arg18 ) ) end
  if (arg19 or arg20) then chat_frame:AddMessage( tostring( arg19 ) ) end
  if (arg20) then chat_frame:AddMessage( tostring( arg20 ) ) end
end

SLASH_RL1 = "/rl"
SlashCmdList[ "RL" ] = ReloadUI

function IsInParty() return GetNumRaidMembers() == 0 and GetNumPartyMembers() > 0 end

function IsInRaid() return GetNumRaidMembers() > 0 end

function IsInGroup() return IsInParty() or IsInRaid() end

-- Defined in WeakAuras.
_G.RAID_CLASS_COLORS = {
  [ "HUNTER" ] = { r = 0.67, g = 0.83, b = 0.45, colorStr = "ffabd473" },
  [ "WARLOCK" ] = { r = 0.58, g = 0.51, b = 0.79, colorStr = "ff9482c9" },
  [ "PRIEST" ] = { r = 1.0, g = 1.0, b = 1.0, colorStr = "ffffffff" },
  [ "PALADIN" ] = { r = 0.96, g = 0.55, b = 0.73, colorStr = "fff58cba" },
  [ "MAGE" ] = { r = 0.41, g = 0.8, b = 0.94, colorStr = "ff69ccf0" },
  [ "ROGUE" ] = { r = 1.0, g = 0.96, b = 0.41, colorStr = "fffff569" },
  [ "DRUID" ] = { r = 1.0, g = 0.49, b = 0.04, colorStr = "ffff7d0a" },
  [ "SHAMAN" ] = { r = 0.0, g = 0.44, b = 0.87, colorStr = "ff0070de" },
  [ "WARRIOR" ] = { r = 0.78, g = 0.61, b = 0.43, colorStr = "ffc79c6e" },
  [ "DEATHKNIGHT" ] = { r = 0.77, g = 0.12, b = 0.23, colorStr = "ffc41f3b" },
}
-- This is localized in WeakAuras, maybe I can port it from there.
_G.LOCALIZED_CLASS_NAMES_MALE = {
  [ "WARRIOR" ] = "Warrior",
  [ "PALADIN" ] = "Paladin",
  [ "HUNTER" ] = "Hunter",
  [ "ROGUE" ] = "Rogue",
  [ "PRIEST" ] = "Priest",
  [ "DEATHKNIGHT" ] = "Deathknight",
  [ "SHAMAN" ] = "Shaman",
  [ "MAGE" ] = "Mage",
  [ "WARLOCK" ] = "Warlock",
  [ "DRUID" ] = "Druid",
}
