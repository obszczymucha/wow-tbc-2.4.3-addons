local _G = getfenv(0);

TTT_Cache = {};
local cache = TTT_Cache;
local points = {};
local uToken, val;
local TALENTS_PREFIX = "Talents:|cffffffff ";

-- GatherTalents
local function GatherTalents(inspect)
	for i = 1, 3 do
		points[i] = select(3,GetTalentTabInfo(i,inspect));
		if (i == 1) or (points[i] > points[val]) then
			val = i;
		end
	end
	-- Set "val" to the desired format
	val = GetTalentTabInfo(val,inspect).." ("..points[1].."/"..points[2].."/"..points[3]..")";
--	val = points[1].."/"..points[2].."/"..points[3];
--	val = GetTalentTabInfo(val,true);
	cache.a, cache.b, cache.c = points[1], points[2], points[3];
	for i = 2, GameTooltip:NumLines() do
		if ((_G["GameTooltipTextLeft"..i]:GetText() or ""):find("^"..TALENTS_PREFIX)) then
			_G["GameTooltipTextLeft"..i]:SetText(TALENTS_PREFIX..val);
			GameTooltip:Show(); -- resize
			break;
		end
	end
	-- Organise Cache
	for i = 1, #cache do
		if (cache.name == cache[i].name) then
			tremove(cache,i);
			break;
		end
	end
	if (#cache >= 10) then	-- Change cache size here (Default 10)
		tremove(cache,1);
	end
	-- Insert the new entry
	tinsert(cache,{ name = cache.name, talents = val });
end

-- Event Frame
local f = CreateFrame("Frame");
f:SetScript("OnEvent",function(self,event,...)
	f:UnregisterEvent("INSPECT_TALENT_READY");
	GatherTalents(1);
--	ClearInspectPlayer();
end);

-- HOOK: OnTooltipSetUnit
GameTooltip:HookScript("OnTooltipSetUnit",
function(self,...)
	uToken = select(2,self:GetUnit());
	if (UnitIsPlayer(uToken)) and (UnitLevel(uToken) > 9 or UnitLevel(uToken) == -1) then
		cache.name = UnitName(uToken);
		if (UnitIsUnit(uToken,"player")) then
			self:AddLine(TALENTS_PREFIX.." Loading...");
			GatherTalents();
		else
			if (CheckInteractDistance(uToken,1) and CanInspect(uToken)) then
				f:RegisterEvent("INSPECT_TALENT_READY");
				NotifyInspect(uToken);
			end
			cache.a, cache.b, cache.c = nil, nil, nil;
			for i = 1, #cache do
				if (cache.name == cache[i].name) then
					self:AddLine(TALENTS_PREFIX..cache[i].talents);
					cache.a, cache.b, cache.c = cache[i].talents:match("(%d+)/(%d+)/(%d+)");
					return;
				end
			end
			if (CheckInteractDistance(uToken,1) and CanInspect(uToken)) then
				self:AddLine(TALENTS_PREFIX.." Loading...");
			end
		end
	end
end
);