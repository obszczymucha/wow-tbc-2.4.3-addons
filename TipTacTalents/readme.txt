TipTacTalents - Show Player Talents in Tooltips
-----------------------------------------------
An addon to show the talents of players in the tooltip.
A cache up to 10 is stored for the last players, so you quickly can see their talents again without having to wait for talents to load.

Despite its name, this addon works completely independent of TipTac. But for further tooltip customisation and enchancements, try out TipTac, which also carries this addon as part of the package.

There is no command line switch or configureable options for TipTacTalents.