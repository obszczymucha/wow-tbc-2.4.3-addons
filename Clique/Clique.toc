## Interface: 20400
## Title: Clique
## Author: Cladhaire
## Version: 102
## Notes: Simply powerful click-casting interface
## SavedVariables: CliqueDB
## OptionalDeps: Dongle

Dongle.lua
Clique.xml
Clique.lua
Localization.enUS.lua
Localization.frFR.lua
Localization.deDE.lua
Localization.koKR.lua
Localization.zhCN.lua
Localization.zhTW.lua
CliqueOptions.lua
CliqueUtils.lua
