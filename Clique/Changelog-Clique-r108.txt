------------------------------------------------------------------------
r108 | Cladhaire-15704 | 2008-10-13 20:45:19 +0000 (Mon, 13 Oct 2008) | 2 lines
Changed paths:
   M /trunk/CliqueOptions.lua

Fixed an error when mousing over an empty spellbook slot

------------------------------------------------------------------------
r107 | Cladhaire-15704 | 2008-10-13 20:28:09 +0000 (Mon, 13 Oct 2008) | 2 lines
Changed paths:
   M /trunk/Clique.lua
   M /trunk/CliqueOptions.lua

Removed conditional WoTLK code

------------------------------------------------------------------------
r106 | Cladhaire-15704 | 2008-10-13 19:19:52 +0000 (Mon, 13 Oct 2008) | 8 lines
Changed paths:
   M /trunk/Clique.xml
   M /trunk/CliqueOptions.lua

* Fixed deprecated use of 'this' in a number of places
* Fixed an issue where the icon selection screen gave an error
* Added a tooltip to the dropdown selector button and the Clique tab button
* Fixed the toggling behavior of Clique when entering combat
* Altered frame levels of the Clique UI so the windows should be usable in all situations
* Double-clicking an entry will edit it
* Rephrased "Stop Casting" to "Cancel Pending Spellcast", since it reflects what the option does

------------------------------------------------------------------------
r105 | Cladhaire-15704 | 2008-10-13 18:18:20 +0000 (Mon, 13 Oct 2008) | 2 lines
Changed paths:
   M /trunk/Clique.lua

Only specify a spell rank when the "Show all spell ranks" button is selected

------------------------------------------------------------------------
r104 | Cladhaire-15704 | 2008-10-13 18:17:20 +0000 (Mon, 13 Oct 2008) | 2 lines
Changed paths:
   M /trunk/CliqueOptions.lua

Make the spellbook tab/button disappear when we're on the glyph screen

------------------------------------------------------------------------
r103 | Cladhaire-15704 | 2008-10-13 18:16:55 +0000 (Mon, 13 Oct 2008) | 2 lines
Changed paths:
   M /trunk/Clique.toc

Update table of contents

------------------------------------------------------------------------
r102 | Cladhaire-15704 | 2008-08-11 20:16:01 +0000 (Mon, 11 Aug 2008) | 2 lines
Changed paths:
   M /trunk/Clique.lua

Added reference to IsWrathBuild()

------------------------------------------------------------------------
