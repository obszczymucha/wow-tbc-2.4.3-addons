## Interface: 20300
## Title: IgnoreMore
## Notes: Ignore more than 49 people, autoshared between your alts
## Author: Mikk
## Version: 1.03
## eMail: dpsgnome@mail.com
## DefaultState: Enabled
## LoadOnDemand: 0
## OptionalDeps: Chatr, ForgottenChat
## SavedVariables: IgM_SV
## SavedVariablesPerCharacter: 
## X-RelSite-WoWI: 5186
## X-Category: Chat/Communication

IgnoreMore.lua
