---@diagnostic disable: undefined-global
local M = LibStub:NewLibrary( "ModUiFacade-1.0", 1 )
if not M then return end

local function GetNumGroupMembers()
  local result = GetNumRaidMembers()

  if result == 0 then
    return GetNumPartyMembers()
  end

  return result
end

local function IsInParty() return GetNumRaidMembers() == 0 and GetNumPartyMembers() > 0 end

local function IsInRaid() return GetNumRaidMembers() > 0 end

local function IsInGroup() return IsInParty() or IsInRaid() end

SetShown = function( frame, shown )
  if shown then frame:Show() else frame:Hide() end
end

C_Timer = {}

function C_Timer.After( duration, f )
  ModUi:ScheduleTimer( f, duration )
end

M.api = _G

M.lua = {
  format = format,
  time = time,
  strmatch = strmatch
}
