local version = 4
---@diagnostic disable-next-line: undefined-global
local libStub = LibStub

local M = libStub:NewLibrary( "ModUi-1.0", version )
if not M then return end

ModUi = M
ModUi.version = version
local facade = libStub( "ModUiFacade-1.0" )
ModUi.facade = facade
local api = facade.api

local aceConsole = libStub( "AceConsole-3.0" )
local aceEvent = libStub( "AceEvent-3.0" )
local aceTimer = libStub( "AceTimer-3.0" )
local aceComm = libStub( "AceComm-3.0" )

ModUi.callbacks = ModUi.callbacks or {}
local m_callbacks = ModUi.callbacks
ModUi.modules = ModUi.modules or {}
local modules = ModUi.modules
ModUi.extensions = ModUi.extensions or {}
local extensions = ModUi.extensions

---@diagnostic disable-next-line: undefined-global
local mainFrame = ChatFrame1
---@diagnostic disable-next-line: undefined-global
local debugFrame = ChatFrame3
local debug = true
local suspended = false
local m_firstEnterWorld = false
local m_initialized = false

local colors = {
  highlight = function( word )
    return string.format( "|cffff9f69%s|r", word )
  end,
  blue = function( word )
    return string.format( "|cff209ff9%s|r", word )
  end,
  white = function( word )
    return string.format( "|cffffffff%s|r", word )
  end,
  red = function( word )
    return string.format( "|cffff2f2f%s|r", word )
  end,
  orange = function( word )
    return string.format( "|cffff8f2f%s|r", word )
  end,
  grey = function( word )
    return string.format( "|cff9f9f9f%s|r", word )
  end,
  green = function( word )
    return string.format( "|cff2fff5f%s|r", word )
  end
}

colors.hl = colors.highlight
ModUi.colors = colors

local function print_error( message, component_name, callback_name )
  local component = component_name and string.format( "[ %s ]", colors.hl( component_name ) ) or ""
  local callback = callback_name and string.format( "[ %s ]", colors.blue( callback_name ) ) or ""
  mainFrame:AddMessage( string.format( "%s%s%s: %s", colors.red( "ModUi" ), component, callback, message ) )
end

local combatParams = {
  combat = false,
  regenEnabled = true
}

function ModUi:Print( ... )
  return aceConsole:Print( ... )
end

Print = function( ... ) aceConsole:Print( ... ) end

function ModUi:PrettyPrint( message )
  ModUi:Print( string.format( "%s: %s", colors.blue( "ModUi" ), message ) )
end

function ModUi:ScheduleTimer( ... )
  return aceTimer:ScheduleTimer( ... )
end

function ModUi:ScheduleRepeatingTimer( ... )
  return aceTimer:ScheduleRepeatingTimer( ... )
end

function ModUi:CancelTimer( ... )
  return aceTimer:CancelTimer( ... )
end

function ModUi:RegisterComm( ... )
  return aceComm:RegisterComm( ... )
end

function ModUi:SendMessage( ... )
  return aceEvent:SendMessage( ... )
end

function ModUi:SendCommMessage( ... )
  return aceComm:SendCommMessage( ... )
end

local function OnEvent( callback_name, combat_lock_check, component_name, ... )
  local callbacks = m_callbacks[ callback_name ]
  local args = { ... }

  for _, entry in ipairs( callbacks ) do
    if not component_name or entry.component.name == component_name then
      if not suspended and entry.component.enabled and not entry.component.suspended and (not combat_lock_check or not api.InCombatLockdown()) then
        --				entry.component:DebugMsg( format( "event: %s", entry.name ) )
        local status, error = pcall( function() entry.callback( unpack( args ) ) end )
        if not status then print_error( error, entry.component.name, callback_name ) end
      end
    end
  end
end

local function RegisterCallback( callback_name )
  if not m_callbacks[ callback_name ] then m_callbacks[ callback_name ] = {} end

  return function( component, callback, dependencies )
        if not component then
          print_error( string.format( "No self provided in %s event callback. Hint: use : insted of .", callback_name ) )
          return
        end

        if not callback then
          print_error( string.format( "No callback provided for %s event in %s component.", callback_name, component.name ) )
          return
        end

        local callbacks = m_callbacks[ callback_name ]
        local newEntry = { name = callback_name, component = component, callback = callback, dependencies = dependencies }

        for index, entry in ipairs( callbacks ) do
          if ModUi.utils.TableContainsValue( entry.dependencies, component.name ) then
            table.insert( callbacks, index, newEntry )
            return
          end
        end

        table.insert( callbacks, newEntry )
      end,
      function( ... )
        OnEvent( callback_name, true, nil, ... )
      end
end

local function AddBlizzEventCallbackFunctionsToComponent( component )
  component.OnLogin = RegisterCallback( "login" )
  component.OnFirstEnterWorld = RegisterCallback( "firstEnterWorld" )
  component.OnEnterWorld = RegisterCallback( "enterWorld" )
  component.OnEnterCombat = RegisterCallback( "enterCombat" )
  component.OnLeaveCombat = RegisterCallback( "leaveCombat" )
  component.OnRegenEnabled = RegisterCallback( "regenEnabled" )
  component.OnRegenDisabled = RegisterCallback( "regenDisabled" )
  component.OnGroupChanged = RegisterCallback( "groupChanged" )
  component.OnGroupFormed = RegisterCallback( "groupFormed" )
  component.OnJoinedGroup = RegisterCallback( "joinedGroup" )
  component.OnLeftGroup = RegisterCallback( "leftGroup" )
  component.OnZoneChanged = RegisterCallback( "zoneChanged" )
  component.OnAreaChanged = RegisterCallback( "areaChanged" )
  component.OnWorldStatesUpdated = RegisterCallback( "worldStatesUpdated" )
  component.OnHerbsAvailable = RegisterCallback( "herbsAvailable" )
  component.OnHerbGathered = RegisterCallback( "herbGathered" )
  component.OnGasExtracted = RegisterCallback( "gasExtracted" )
  component.OnPartyInviteRequest = RegisterCallback( "partyInviteRequest" )
  component.OnChatMsgSystem = RegisterCallback( "chatMsgSystem" )
  component.OnReadyCheck = RegisterCallback( "readyCheck" )
  component.OnTargetChanged = RegisterCallback( "targetChanged" )
  component.OnActionBarSlotChanged = RegisterCallback( "actionBarSlotChanged" )
  component.OnCombatLogEventUnfiltered = RegisterCallback( "combatLogEventUnfiltered" )
  component.OnPendingMail = RegisterCallback( "pendingMail" )
  component.OnBagUpdate = RegisterCallback( "bagUpdate" )
  component.OnWhisper = RegisterCallback( "whisper" )
  component.OnPartyMessage = RegisterCallback( "partyMessage" )
  component.OnPartyLeaderMessage = RegisterCallback( "partyLeaderMessage" )
  component.OnSkillIncreased = RegisterCallback( "skillIncreased" )
  component.OnHonorGain = RegisterCallback( "honorGain" )
  component.OnAreaPoisUpdated = RegisterCallback( "areaPoisUpdated" )
  component.OnGossipShow = RegisterCallback( "gossipShow" )
  component.OnMerchantShow = RegisterCallback( "merchantShow" )
  component.OnSpecChanged = RegisterCallback( "specChanged" )
  component.OnLootReady = RegisterCallback( "lootReady" )
  component.OnOpenMasterLootList = RegisterCallback( "openMasterLootList" )
  component.OnLootSlotCleared = RegisterCallback( "lootSlotCleared" )
  component.OnTradeShow = RegisterCallback( "tradeShow" )
  component.OnTradePlayerItemChanged = RegisterCallback( "tradePlayerItemChanged" )
  component.OnTradeTargetItemChanged = RegisterCallback( "tradeTargetItemChanged" )
  component.OnTradeClosed = RegisterCallback( "tradeClosed" )
  component.OnTradeAcceptUpdate = RegisterCallback( "tradeAcceptUpdate" )
  component.OnTradeRequestCancel = RegisterCallback( "tradeRequestCancel" )
  component.OnUpdateBattlefieldStatus = RegisterCallback( "updateBattlefieldStatus" )
  component.OnQuestProgress = RegisterCallback( "questProgress" )
  component.OnQuestComplete = RegisterCallback( "questComplete" )
end

local function DebugMsg( message )
  if not debug then return end
  debugFrame:AddMessage( string.format( "|cff33ff99ModUi|r: %s", message ) )
end

function ModUi.NewExtension( _, extension_name, requiredExtensions )
  if not extension_name then
    print_error( "No extension name provided.", nil, "NewExtension" )
    return
  end

  if extensions[ extension_name ] then
    print_error( "Extension already defined.", extension_name, "NewExtension" )
    return
  end

  local extension = {
    name = extension_name,
    requiredExtensions = requiredExtensions,
    enabled = true,
    suspended = false,
    debug = true,
    debugFrame = debugFrame,
    DebugMsg = function( extension, message, force )
      if extension.debugFrame and (debug or extension.debug or force) then
        extension.debugFrame:AddMessage( string.format( "|cff33ff99ModUi [|r|cff209ff9%s|r|cff33ff99]|r: %s", extension.name, message ) )
      end
    end,
    RegisterCallback = function( extension, callbackName )
      if extension.enabled then
        return RegisterCallback( callbackName )
      else
        return function( ext ) ext:DebugMsg( "Extension disabled." ) end, function( ext ) ext:DebugMsg( "Extension disabled." ) end
      end
    end,
    PrettyPrint = function( mod, message )
      mainFrame:AddMessage( string.format( "%s%s%s: %s", colors.blue( "ModUi [ " ), colors.highlight( mod.name ),
        colors.blue( " ]" ), message ) )
    end
  }

  AddBlizzEventCallbackFunctionsToComponent( extension )
  ModUi.AddUtilityFunctionsToModule( combatParams, extension )
  extensions[ extension_name ] = extension

  return extension
end

function ModUi.GetExtension( _, extensionName )
  return extensions[ extensionName ]
end

function ModUi.GetModule( _, moduleName )
  return modules[ moduleName ]
end

local function ExtendComponent( component )
  for _, extension in pairs( extensions ) do
    if extension.enabled and ModUi.utils.TableContainsValue( component.requiredExtensions, extension.name ) and extension.ExtendComponent then
      component:DebugMsg( string.format( "Extending with |cff209ff9%s|r", extension.name ) )
      extension.ExtendComponent( component )
    end
  end
end

function ModUi.NewModule( _, module_name, requiredExtensions )
  if not module_name then
    print_error( "No module name provided.", nil, "NewModule" )
    return
  end

  if modules[ module_name ] then
    print_error( "Module is already defined.", module_name, "NewModule" )
    return
  end

  local mod = {
    name = module_name,
    requiredExtensions = requiredExtensions,
    enabled = true,
    suspended = false,
    debug = debug,
    debugFrame = debugFrame,
    DebugMsg = function( mod, message, force )
      if mod.debugFrame and (mod.debug or force) then
        mod.debugFrame:AddMessage( string.format( "|cff33ff99ModUi [|r|cffff9f69%s|r|cff33ff99]|r: %s", mod.name,
          message or "nil" ) )
      end
    end,
    Initialize = function( mod ) mod:DebugMsg( "No Initialize() found." ) end, -- Override in a module
    PrettyPrint = function( mod, message )
      mainFrame:AddMessage( string.format( "%s%s%s: %s", colors.blue( "ModUi [ " ), colors.highlight( mod.name ),
        colors.blue( " ]" ), message ) )
    end
  }

  AddBlizzEventCallbackFunctionsToComponent( mod )
  ModUi.AddUtilityFunctionsToModule( combatParams, mod )
  modules[ module_name ] = mod

  if m_initialized then
    ExtendComponent( mod )
  end

  return mod
end

local function ExtendExtensions()
  for _, extension in pairs( extensions ) do
    ExtendComponent( extension )
  end
end

local function ExtendModules()
  for _, mod in pairs( modules ) do
    ExtendComponent( mod )
  end
end

local function DisableModulesWithoutExtensions()
  for _, mod in pairs( modules ) do
    if mod.requiredExtensions then
      for _, extensionName in ipairs( mod.requiredExtensions ) do
        local extension = extensions[ extensionName ]

        if not extension or not extension.enabled then
          mod.enabled = false
          mod:DebugMsg( string.format( "Disabled: Extension |cff209ff9%s|r not available", extensionName ) )
        end
      end
    end
  end
end

local function InitializeComponents( components )
  for _, component in pairs( components ) do
    if component.enabled and component.Initialize then
      component:DebugMsg( "Initializing" )
      local status, error = pcall( function() component:Initialize() end )
      if not status then print_error( error, component.name, "Initialize" ) end
    end
  end
end

local function OnUnitSpellcastChannelStop( unit, _, spellId )
  if unit == "player" and spellId == 30427 then
    OnEvent( "gasExtracted", false )
  end
end

-- OnUnitSpellcastStart( who, id, spellId )
local function OnUnitSpellcastStart( _, _, spellId )
  if spellId == 2366 then
    OnEvent( "herbGathered", true )
    return
  end
end

-- OnUnitSpellcastStart( who, id, spellId )
local function OnUnitSpellcastSucceeded( target, spell_name )
  if (target == "player" and spell_name == "Body Switch") then
    OnEvent( "specChanged", true )
  end
end

local function OnChatMsgSystem( message )
  if message == "You have joined a raid group." then
    OnEvent( "joinedGroup", false )
  else
    OnEvent( "chatMsgSystem", false, nil, message )
  end
end

--eventHandler( frame, event, ... )
local function eventHandler( _, event, ... )
  if event == "PLAYER_LOGIN" then
    m_firstEnterWorld = false
    if m_initialized then return end
    ModUi:OnInitialize()
    ExtendExtensions()
    InitializeComponents( extensions )
    ExtendModules()
    DisableModulesWithoutExtensions()
    InitializeComponents( modules )
    OnEvent( "login", false )
  elseif event == "PLAYER_ENTERING_WORLD" then
    if not m_firstEnterWorld then
      OnEvent( "firstEnterWorld", false )
      m_firstEnterWorld = true
    end
    OnEvent( "enterWorld", false )
  elseif event == "PLAYER_REGEN_ENABLED" then
    combatParams.regenEnabled = true
    OnEvent( "regenEnabled", true )
  elseif event == "PLAYER_REGEN_DISABLED" then
    combatParams.regenEnabled = false
    OnEvent( "regenDisabled", true )
  elseif event == "PLAYER_ENTER_COMBAT" then
    combatParams.combat = true
    OnEvent( "enterCombat", true )
  elseif event == "PLAYER_LEAVE_COMBAT" then
    combatParams.combat = false
    OnEvent( "leaveCombat", true )
  elseif event == "RAID_ROSTER_UPDATE" then
    OnEvent( "groupChanged", true )
  elseif event == "PARTY_MEMBERS_CHANGED" then
    OnEvent( "groupChanged", true )
  elseif event == "GROUP_ROSTER_UPDATE" then
    OnEvent( "groupChanged", true )
  elseif event == "GROUP_FORMED" then
    OnEvent( "groupFormed", true )
  elseif event == "GROUP_JOINED" then
    OnEvent( "joinedGroup", true )
  elseif event == "GROUP_LEFT" then
    OnEvent( "leftGroup", true )
  elseif event == "ZONE_CHANGED" or event == "ZONE_CHANGED_INDOORS" then
    OnEvent( "zoneChanged", true )
  elseif event == "ZONE_CHANGED_NEW_AREA" then
    OnEvent( "areaChanged", true )
  elseif event == "UPDATE_WORLD_STATES" then
    OnEvent( "worldStatesUpdated", true )
  elseif event == "UNIT_SPELLCAST_CHANNEL_STOP" then
    OnUnitSpellcastChannelStop( ... )
  elseif event == "PARTY_INVITE_REQUEST" then
    OnEvent( "partyInviteRequest", false, nil, ... )
  elseif event == "CHAT_MSG_SYSTEM" then
    OnChatMsgSystem( ... )
  elseif event == "READY_CHECK" then
    OnEvent( "readyCheck", false )
  elseif event == "PLAYER_TARGET_CHANGED" then
    OnEvent( "targetChanged", true )
  elseif event == "ACTIONBAR_SLOT_CHANGED" then
    OnEvent( "actionBarSlotChanged", true )
  elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
    OnEvent( "combatLogEventUnfiltered", false, nil, ... )
  elseif event == "UPDATE_PENDING_MAIL" then
    OnEvent( "pendingMail", false, nil, ... )
  elseif event == "BAG_UPDATE" then
    OnEvent( "bagUpdate", false, nil, ... )
  elseif event == "CHAT_MSG_WHISPER" then
    OnEvent( "whisper", false, nil, ... )
  elseif event == "CHAT_MSG_PARTY" then
    OnEvent( "partyMessage", false, nil, ... )
  elseif event == "CHAT_MSG_PARTY_LEADER" then
    OnEvent( "partyLeaderMessage", false, nil, ... )
  elseif event == "CHAT_MSG_SKILL" then
    OnEvent( "skillIncreased", false, nil, ... )
  elseif event == "CHAT_MSG_COMBAT_HONOR_GAIN" then
    OnEvent( "honorGain", false, nil, ... )
  elseif event == "UNIT_SPELLCAST_START" then
    OnUnitSpellcastStart( ... )
  elseif event == "UNIT_SPELLCAST_SUCCEEDED" then
    OnUnitSpellcastSucceeded( ... )
  elseif event == "AREA_POIS_UPDATED" then
    OnEvent( "areaPoisUpdated", false, nil, ... )
  elseif event == "GOSSIP_SHOW" then
    OnEvent( "gossipShow", false, nil, ... )
  elseif event == "MERCHANT_SHOW" then
    OnEvent( "merchantShow", false, nil, ... )
  elseif event == "ACTIVE_TALENT_GROUP_CHANGED" then
    OnEvent( "specChanged", false, nil, ... )
  elseif event == "LOOT_READY" then
    OnEvent( "lootReady", false, nil, ... )
  elseif event == "OPEN_MASTER_LOOT_LIST" then
    OnEvent( "openMasterLootList", false, nil, ... )
  elseif event == "LOOT_SLOT_CLEARED" then
    OnEvent( "lootSlotCleared", false, nil, ... )
  elseif event == "TRADE_SHOW" then
    OnEvent( "tradeShow", false, nil, ... )
  elseif event == "TRADE_PLAYER_ITEM_CHANGED" then
    OnEvent( "tradePlayerItemChanged", false, nil, ... )
  elseif event == "TRADE_TARGET_ITEM_CHANGED" then
    OnEvent( "tradeTargetItemChanged", false, nil, ... )
  elseif event == "TRADE_CLOSED" then
    OnEvent( "tradeClosed", false, nil, ... )
  elseif event == "TRADE_ACCEPT_UPDATE" then
    OnEvent( "tradeAcceptUpdate", false, nil, ... )
  elseif event == "TRADE_REQUEST_CANCEL" then
    OnEvent( "tradeRequestCancel", false, nil, ... )
  elseif event == "UPDATE_BATTLEFIELD_STATUS" then
    OnEvent( "updateBattlefieldStatus", false, nil, ... )
  elseif event == "QUEST_PROGRESS" then
    OnEvent( "questProgress", false, nil, ... )
  elseif event == "QUEST_COMPLETE" then
    OnEvent( "questComplete", false, nil, ... )
  end
end

ModUi.frame = ModUi.frame or api.CreateFrame( "FRAME", "ModUiFrame" )
local frame = ModUi.frame

frame:RegisterEvent( "PLAYER_LOGIN" )
frame:RegisterEvent( "PLAYER_ENTERING_WORLD" )
frame:RegisterEvent( "PLAYER_REGEN_ENABLED" )
frame:RegisterEvent( "PLAYER_REGEN_DISABLED" )
frame:RegisterEvent( "PLAYER_ENTER_COMBAT" )
frame:RegisterEvent( "PLAYER_LEAVE_COMBAT" )
frame:RegisterEvent( "RAID_ROSTER_UPDATE" )
frame:RegisterEvent( "PARTY_MEMBERS_CHANGED" )
frame:RegisterEvent( "GROUP_ROSTER_UPDATE" )
frame:RegisterEvent( "GROUP_JOINED" )
frame:RegisterEvent( "GROUP_LEFT" )
frame:RegisterEvent( "GROUP_FORMED" )
frame:RegisterEvent( "ZONE_CHANGED" )
frame:RegisterEvent( "ZONE_CHANGED_INDOORS" )
frame:RegisterEvent( "ZONE_CHANGED_NEW_AREA" )
frame:RegisterEvent( "UNIT_SPELLCAST_CHANNEL_STOP" )
frame:RegisterEvent( "PARTY_INVITE_REQUEST" )
frame:RegisterEvent( "CHAT_MSG_SYSTEM" )
frame:RegisterEvent( "READY_CHECK" )
frame:RegisterEvent( "PLAYER_TARGET_CHANGED" )
frame:RegisterEvent( "ACTIONBAR_SLOT_CHANGED" )
frame:RegisterEvent( "COMBAT_LOG_EVENT_UNFILTERED" )
frame:RegisterEvent( "UPDATE_PENDING_MAIL" )
frame:RegisterEvent( "BAG_UPDATE" )
frame:RegisterEvent( "CHAT_MSG_WHISPER" )
frame:RegisterEvent( "CHAT_MSG_PARTY" )
frame:RegisterEvent( "CHAT_MSG_PARTY_LEADER" )
frame:RegisterEvent( "CHAT_MSG_SKILL" )
frame:RegisterEvent( "CHAT_MSG_COMBAT_HONOR_GAIN" )
frame:RegisterEvent( "UNIT_SPELLCAST_START" )
frame:RegisterEvent( "UNIT_SPELLCAST_SUCCEEDED" )
frame:RegisterEvent( "AREA_POIS_UPDATED" )
frame:RegisterEvent( "GOSSIP_SHOW" )
frame:RegisterEvent( "MERCHANT_SHOW" )
frame:RegisterEvent( "ACTIVE_TALENT_GROUP_CHANGED" )
frame:RegisterEvent( "LOOT_READY" )
frame:RegisterEvent( "OPEN_MASTER_LOOT_LIST" )
frame:RegisterEvent( "LOOT_SLOT_CLEARED" )
frame:RegisterEvent( "TRADE_SHOW" )
frame:RegisterEvent( "TRADE_PLAYER_ITEM_CHANGED" )
frame:RegisterEvent( "TRADE_TARGET_ITEM_CHANGED" )
frame:RegisterEvent( "TRADE_CLOSED" )
frame:RegisterEvent( "TRADE_ACCEPT_UPDATE" )
frame:RegisterEvent( "TRADE_REQUEST_CANCEL" )
frame:RegisterEvent( "UPDATE_BATTLEFIELD_STATUS" )
frame:RegisterEvent( "QUEST_PROGRESS" )
frame:RegisterEvent( "QUEST_COMPLETE" )
frame:SetScript( "OnEvent", eventHandler )

function ModUi.SimulateEvent( _, eventName, ... )
  if not m_callbacks[ eventName ] then
    DebugMsg( string.format( "Event %s not found.", eventName or "N/A" ) )
    return
  end

  OnEvent( eventName, false, nil, ... )
end

local function ProcessSlashCommand( component )
  if not component or component == "" then
    suspended = not suspended

    if suspended then
      ModUi:PrettyPrint( "suspended" )
    else
      ModUi:PrettyPrint( "resumed" )
    end

    return
  end

  if component == "version" then
    ModUi:PrettyPrint( string.format( "Version: %s", ModUi.utils.highlight( ModUi.version ) ) )
    return
  end

  local extension = ModUi:GetExtension( component )
  local mod = ModUi:GetModule( component )

  if not extension and not mod then
    ModUi:PrettyPrint( string.format( "Unknown component: |cffff9f69%s|r", component ) )
    return
  end

  local comp
  if extension then comp = extension end
  if mod then comp = mod end

  comp.suspended = not comp.suspended

  if comp.suspended then
    comp:PrettyPrint( "suspended" )
  else
    comp:PrettyPrint( "resumed" )
  end
end

function ModUi:OnInitialize()
  SLASH_MODUI1 = "/modui"
  api.SlashCmdList[ "MODUI" ] = ProcessSlashCommand

  self.db = libStub( "AceDB-3.0" ):New( "ModUiDb" )
  aceEvent:RegisterMessage( "HERB_COUNT_HERBS_AVAILABLE", function() OnEvent( "herbsAvailable", true ) end )
  m_initialized = true
  M:PrettyPrint( "Loaded." )
end

local adjust_dimension = function( frm, delta, f )
  if not frm then return end
  local name = frm:GetName()
  local before, new = f( frm, delta )
  M:PrettyPrint( string.format( "Name: %s, before: %s, now: %s", name, before, new ) )
end

AdjustHeight = function( frm, delta )
  adjust_dimension( frm, delta, M.utils.AdjustHeight )
end

AdjustWidth = function( frm, delta )
  adjust_dimension( frm, delta, M.utils.AdjustWidth )
end
