---@diagnostic disable-next-line: undefined-global
local facade = LibStub( "ModUiFacade-1.0" )
local api = facade.api
local lua = facade.lua

local get_index = api.GetMacroIndexByName

local create = function( name, body, global, iconIndex )
  local index = get_index( name )
  if index ~= 0 then return end

  api.CreateMacro( name, not iconIndex and 1 or iconIndex, body or "", nil, not global and true or false )
end

local edit = function( name, body, global, icon )
  local index = get_index( name )
  if index == 0 then return end

  api.EditMacro( index, nil, icon, body, true, not global and 1 or nil )
end

local builder = function( macro )
  local function group_consecutive_entries( commands )
    local result = {}
    local current_command = nil
    local current_list = {}

    local function should_group( command )
      return command == "use" or command == "cast"
    end

    local function flush()
      if #current_list > 0 then
        table.insert( result, { command = current_command, values = current_list } )
        current_command = nil
        current_list = {}
      end
    end

    if #commands == 0 then
      return {}
    end

    for _, v in ipairs( commands ) do
      if should_group( v.command ) then
        if current_command == v.command then
          table.insert( current_list, v )
        else
          flush()

          current_command = v.command
          current_list = {}
          table.insert( current_list, v )
        end
      else
        flush()
        table.insert( result, v )
      end
    end

    flush()
    return result
  end

  local function transform_command( command, values )
    local result = lua.format( "/%s ", command )

    for i, v in ipairs( values ) do
      if i > 1 then result = result .. ";" end

      result = result .. lua.format( "%s%s", v.modifiers and lua.format( "[%s]", v.modifiers ) or "", v.spell )
    end

    return result
  end

  local function parse_modifiers( ... )
    if select( "#", ... ) == 0 then return nil end
    return table.concat( { ... }, "," )
  end

  return {
    _tooltip = "",
    _commands = {},
    _global = false,
    tooltip = function( self, ... )
      local modifiers = parse_modifiers( ... )
      self._tooltip = lua.format( "#showtooltip%s\n", modifiers and lua.format( " %s", modifiers ) or "" )
      return self
    end,
    startattack = function( self, ... )
      table.insert( self._commands, { command = "startattack", modifiers = parse_modifiers( ... ) } )
      return self
    end,
    castsequence = function( self, spells, reset, ... )
      if not spells then return self end

      table.insert( self._commands, { command = "castsequence", spells = spells, reset = reset, modifiers = parse_modifiers( ... ) } )
      return self
    end,
    use = function( self, spell, ... )
      if not spell then return self end

      table.insert( self._commands, { command = "use", spell = spell, modifiers = parse_modifiers( ... ) } )
      return self
    end,
    cast = function( self, spell, ... )
      if not spell then return self end

      table.insert( self._commands, { command = "cast", spell = spell, modifiers = parse_modifiers( ... ) } )
      return self
    end,
    dismount = function( self, ... )
      table.insert( self._commands, { command = "dismount", modifiers = parse_modifiers( ... ) } )
      return self
    end,
    cancelform = function( self, ... )
      table.insert( self._commands, { command = "cancelform", modifiers = parse_modifiers( ... ) } )
      return self
    end,
    cancelaura = function( self, ... )
      table.insert( self._commands, { command = "cancelaura", modifiers = parse_modifiers( ... ) } )
      return self
    end,
    global = function( self )
      self._global = true
      return self
    end,
    equip = function( self, item, ... )
      if not item then return self end

      table.insert( self._commands, { command = "equip", item = item, modifiers = parse_modifiers( ... ) } )
      return self
    end,
    build = function( self, macro_name )
      self._grouped_commands = group_consecutive_entries( self._commands )

      local body = self._tooltip or ""

      for i, v in ipairs( self._grouped_commands ) do
        if i > 1 then body = body .. "\n" end

        if v.command == "startattack" then
          body = body .. lua.format( "/startattack%s", v.modifiers and lua.format( " [%s]", v.modifiers ) or "" )
        elseif v.command == "castsequence" then
          body = body ..
              lua.format( "/castsequence %s%s%s", v.modifiers and lua.format( "[%s]", v.modifiers ) or "", v.reset and lua.format( " reset=%s ", v.reset ) or "",
                v.spells )
        elseif v.command == "dismount" then
          body = body .. lua.format( "/dismount%s", v.modifiers and lua.format( " [%s]", v.modifiers ) or "" )
        elseif v.command == "cancelform" then
          body = body .. lua.format( "/cancelform%s", v.modifiers and lua.format( " [%s]", v.modifiers ) or "" )
        elseif v.command == "cancelaura" then
          body = body .. lua.format( "/cancelaura%s", v.modifiers and lua.format( " [%s]", v.modifiers ) or "" )
        elseif v.command == "use" or v.command == "cast" then
          body = body .. transform_command( v.command, v.values )
        elseif v.command == "equip" then
          body = body .. lua.format( "/equip %s%s", v.modifiers and lua.format( "[%s]", v.modifiers ) or "", v.item )
        else
          ModUi:PrettyPrint( lua.format( "Cannot create macro. Command \"%s\" is not implemented.", v.command ) )
        end
      end

      local length = string.len( body )

      if length > 255 then
        ModUi:PrettyPrint( lua.format( "Macro \"%s\" is too long (%s)!", macro_name, length ) )
      elseif macro_name then
        macro.edit( macro_name, body, self._global )
      end

      return self
    end
  }
end

facade.macro = {
  create = create,
  edit = function( name, body, global, icon )
    create( name, nil, global, icon )
    edit( name, body, global, icon )
  end,
  delete = function( name )
    local index = get_index( name )
    if index == 0 then return end

    api.DeleteMacro( index )
  end,
  tooltip = function( self, modifiers ) return builder( self ):tooltip( modifiers ) end,
  modifiers = {
    ALT = "mod:alt",
    CTRL = "mod:ctrl",
    SHIFT = "mod:shift",
    ALT_CTRL = "mod:altctrl",
    ALT_SHIFT = "mod:altshift",
    CTRL_SHIFT = "mod:ctrlshift",
    ALT_CTRL_SHIFT = "mod:altctrlshift",
    NOMOD = "nomod",
    TARGET_PLAYER = "target=player",
    TARGET_FOCUS = "target=focus",
    TARGET_MOUSEOVER = "target=mouseover",
    TARGET_TARGETTARGET = "target=targettarget",
    HARM = "harm",
    NOHARM = "noharm",
    DEAD = "dead",
    NODEAD = "nodead",
    FLYABLE = "flyable",
    NOFLYABLE = "noflyable",
    COMBAT = "combat",
    NOCOMBAT = "nocombat",
    EQUIPPED_THROWN = "equipped:Thrown",
    reset = function( reset_args ) return string.format( "reset=%s", reset_args ) end
  }
}

return facade.macro
