local lib_stub = LibStub
local modules = lib_stub( "EssentialCooldowns-Modules" )
local version = modules.get_addon_version()

local M = lib_stub:NewLibrary( string.format( "EssentialCooldowns-%s", version.major ), version.minor )
if not M then return end

local pretty_print = modules.pretty_print
local hl = modules.colors.hl

local function create_components()
  local m = modules

  M.ace_timer = lib_stub( "AceTimer-3.0" )
  M.spell_registry = m.SpellRegistry.new()
  M.db = m.Db.new( version.str )
  M.api = function() return m.api end
  M.cooldown_printer = m.CooldownPrinter.new()
  M.update_notificator = m.UpdateNotificator.new()
  M.group_tracker = m.GroupTracker.new( M.update_notificator.notify )
  M.gui = m.Gui.new( M.db.set_position )
  M.version_broadcast = m.VersionBroadcast.new( M.db, version.str )
  M.cooldown_tracker = m.CooldownTracker.new( M.db, M.spell_registry, M.cooldown_printer, M.gui )
  M.warlock_tracker = m.WarlockTracker.new( M.db )
  M.spell_tracker = m.SpellTracker.new( M.group_tracker, M.cooldown_tracker, M.warlock_tracker )
end

local function on_cmd()
  M.gui.toggle()
end

local function setup_slash_commands()
  SLASH_ECD1 = "/ecd"
  M.api().SlashCmdList[ "ECD" ] = on_cmd
end

local function subscribe_for_group_tracker_events()
  local tracker = M.group_tracker
  local EventType = tracker.EventType

  tracker.subscribe( EventType.YouJoinedGroup, function( my_unit_info )
    M.cooldown_tracker.on_someone_joined_group( my_unit_info )
    M.version_broadcast.broadcast()
  end )

  tracker.subscribe( EventType.YouLeftGroup, function( former_group_members )
    M.cooldown_tracker.on_you_left_group( former_group_members )
  end )

  tracker.subscribe( EventType.SomeoneJoinedGroup, function( group_member )
    M.cooldown_tracker.on_someone_joined_group( group_member )
  end )

  tracker.subscribe( EventType.SomeoneLeftGroup, function( group_member )
    M.cooldown_tracker.on_someone_left_group( group_member )
  end )
end

local function show_gui()
  local position = M.db.get_position()
  M.gui.create( 135 )
  M.gui.position( position.anchor, UIParent, position.other_anchor, position.x, position.y )
  M.gui.show()
end

function M.initialize()
  create_components()
  setup_slash_commands()
  subscribe_for_group_tracker_events()
  show_gui()

  pretty_print( string.format( "Loaded (%s).", hl( string.format( "v%s", version.str ) ) ) )
end

modules.EventHandler.handle_events( M )

function M.get_visible_cooldown_count()
  return M.gui.get_visible_cooldown_count()
end

function M.register_for_updates( callback )
  M.update_notificator.register_for_updates( callback )
end

EssentialCooldowns = M

return M
