local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.GroupTracker then return end

local M = {}
local api = modules.api

local print_error = modules.print_error
local count_elements = modules.count_elements

local EventType = {
  SomeoneJoinedGroup = "SomeoneJoinedGroup",
  SomeoneLeftGroup = "SomeoneLeftGroup",
  YouJoinedGroup = "YouJoinedGroup",
  YouLeftGroup = "YouLeftGroup"
}

local function get_full_unit_info( name, unit )
  return {
    name = name,
    level = api.UnitLevel( unit ),
    class = api.UnitClass( unit )
  }
end

function M.new( notify_cooldown_updates )
  local m_callbacks = {}
  local m_my_unit_info = get_full_unit_info( api.UnitName( "player" ), "player" )

  local function init_with_player_details()
    return { [ m_my_unit_info.name ] = m_my_unit_info }
  end

  local m_group_members = init_with_player_details()
  local m_first_enter_world = true

  local function valid( event_type )
    for k, _ in pairs( EventType ) do
      if k == event_type then return true end
    end

    return false
  end

  local function subscribe( event_type, callback )
    if not valid( event_type ) then
      print_error( string.format( "Unsupported event type: %s", event_type or "nil" ), "GroupTracker" )
      return
    end

    m_callbacks[ event_type ] = m_callbacks[ event_type ] or {}
    table.insert( m_callbacks[ event_type ], callback )
  end

  local function get_players_in_my_group()
    local result = {}

    local is_raid = api.IsInRaid()
    local group_type = is_raid and "raid" or "party"
    local group_size = is_raid and api.GetNumGroupMembers() or api.GetNumPartyMembers()

    if not is_raid then
      result[ m_my_unit_info.name ] = m_my_unit_info
    end

    for i = 1, group_size do
      local unit = group_type .. i
      local name = api.UnitName( unit )
      if name and name ~= "" and name ~= "Unknown" then result[ name ] = get_full_unit_info( name, unit ) end
    end

    return result
  end

  local function notify( event_type, group_member )
    local callbacks = m_callbacks[ event_type ] or {}
    for _, callback in ipairs( callbacks ) do callback( group_member ) end
  end

  local function add_group_member( group_member )
    m_group_members[ group_member.name ] = group_member
    notify( EventType.SomeoneJoinedGroup, group_member )
  end

  local function find_players_who_left( fresh_members, former_members )
    local result = {}

    for name, full_info in pairs( former_members ) do
      if not fresh_members[ name ] then
        result[ name ] = full_info
      end
    end

    return result
  end

  local function on_party_members_changed()
    local fresh_members = get_players_in_my_group()
    local players_who_left = find_players_who_left( fresh_members, m_group_members )
    local former_count = count_elements( m_group_members )
    local new_count = count_elements( fresh_members )

    if former_count > 1 and new_count == 1 then
      notify( EventType.YouLeftGroup, m_group_members )
      m_group_members = init_with_player_details()
      notify_cooldown_updates()
      return
    elseif former_count == 1 and new_count > 1 then
      notify( EventType.YouJoinedGroup, m_my_unit_info )
    end

    for name, full_info in pairs( players_who_left ) do
      notify( EventType.SomeoneLeftGroup, full_info )
      m_group_members[ name ] = nil
    end

    for name, full_info in pairs( fresh_members ) do
      if not m_group_members[ name ] then
        add_group_member( full_info )
      end
    end

    notify_cooldown_updates()
  end

  local function get_group_member_info( player_name )
    return m_group_members[ player_name ]
  end

  local function on_event( _, event )
    if event == "PLAYER_ENTERING_WORLD" and m_first_enter_world then
      m_first_enter_world = false
      on_party_members_changed()
    elseif event == "PARTY_MEMBERS_CHANGED" then
      on_party_members_changed()
    end
  end

  local frame = modules.api.CreateFrame( "Frame" )
  frame:RegisterEvent( "PLAYER_ENTERING_WORLD" )
  frame:RegisterEvent( "PARTY_MEMBERS_CHANGED" )
  frame:SetScript( "OnEvent", on_event )

  return {
    EventType = EventType,
    subscribe = subscribe,
    get_group_member_info = get_group_member_info
  }
end

modules.GroupTracker = M
return M
