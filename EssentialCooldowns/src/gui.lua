local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.Gui then return end

local M = {}

local api = modules.api

local function enable_drag( frame, on_drag_stop )
  if not frame then return end
  frame:SetMovable( true )
  frame:EnableMouse( true )
  frame:RegisterForDrag( "LeftButton" )
  frame:SetScript( "OnDragStart", frame.StartMoving )
  frame:SetScript( "OnDragStop", function( self, ... )
    self:StopMovingOrSizing( ... )
    local anchor, _, other_anchor, x, y = frame:GetPoint()
    on_drag_stop( anchor, other_anchor, x, y )
  end )
end

local function create_main_frame( width, on_drag_stop )
  local frame = api.CreateFrame( "Frame", "EssentialCooldownsFrame", UIParent )
  frame:SetWidth( width )
  enable_drag( frame, on_drag_stop )

  return frame
end

local function count_cooldowns( players_with_cooldowns )
  local result = 0

  for _, cooldowns in pairs( players_with_cooldowns ) do
    for _ in pairs( cooldowns ) do
      result = result + 1
    end
  end

  return result
end

function M.new( on_drag_stop )
  local frame
  local players_with_cooldowns = {}

  local function sort_cooldowns( visible_cooldowns )
    local function count_players_per_spell()
      local counts = {}

      for _, cooldown in ipairs( visible_cooldowns ) do
        counts[ cooldown.spell_name ] = (counts[ cooldown.spell_name ] or 0) + 1
      end

      return counts
    end

    local function count_spells_per_player()
      local counts = {}

      for _, cooldown in ipairs( visible_cooldowns ) do
        counts[ cooldown.player_name ] = (counts[ cooldown.player_name ] or 0) + 1
      end

      return counts
    end

    local sorted_cooldowns = {}
    local spell_counts = count_players_per_spell()
    local player_counts = count_spells_per_player()
    local multi_player_spells, multi_spell_players, remaining_players = {}, {}, {}

    for _, cooldown in ipairs( visible_cooldowns ) do
      if spell_counts[ cooldown.spell_name ] > 1 then
        table.insert( multi_player_spells, cooldown )
      elseif player_counts[ cooldown.player_name ] > 1 then
        table.insert( multi_spell_players, cooldown )
      else
        table.insert( remaining_players, cooldown )
      end
    end

    local sort_fn = function( a, b )
      if a.spell_name == b.spell_name then
        return a.player_name < b.player_name
      else
        return a.spell_name < b.spell_name
      end
    end

    local sort_fn_grouped = function( a, b )
      if a.player_name == b.player_name then
        return a.spell_name < b.spell_name
      else
        return a.player_name < b.player_name
      end
    end

    table.sort( multi_player_spells, sort_fn )
    table.sort( multi_spell_players, sort_fn_grouped )
    table.sort( remaining_players, function( a, b ) return a.player_name < b.player_name end )

    for _, entry in ipairs( multi_player_spells ) do table.insert( sorted_cooldowns, entry ) end
    for _, entry in ipairs( multi_spell_players ) do table.insert( sorted_cooldowns, entry ) end
    for _, entry in ipairs( remaining_players ) do table.insert( sorted_cooldowns, entry ) end

    return sorted_cooldowns
  end

  local function filter_visible_cooldowns()
    local result = {}

    for player_name, cooldowns in pairs( players_with_cooldowns ) do
      for spell_name, cooldown in pairs( cooldowns ) do
        if cooldown.frame:IsVisible() then
          local entry = { spell_name = spell_name, player_name = player_name, frame = cooldown.frame }
          table.insert( result, entry )
        end
      end
    end

    return result
  end

  local function refresh()
    local visible_cooldowns = filter_visible_cooldowns()
    local sorted_cooldowns = sort_cooldowns( visible_cooldowns )

    frame:SetHeight( count_cooldowns( visible_cooldowns ) * 21 )
    local previous_cooldown

    for _, cooldown in ipairs( sorted_cooldowns ) do
      if cooldown.frame:IsVisible() then
        if not previous_cooldown then
          cooldown.frame:SetPoint( "TOP", frame )
        else
          cooldown.frame:SetPoint( "TOP", previous_cooldown.frame, "BOTTOM", 0, -1 )
        end

        previous_cooldown = cooldown
      end
    end
  end

  local function add_cooldown_frame( player_name, class, spell_name, cooldown_value, icon_name, should_hide )
    players_with_cooldowns[ player_name ] = players_with_cooldowns[ player_name ] or {}
    players_with_cooldowns[ player_name ][ spell_name ] = players_with_cooldowns[ player_name ][ spell_name ] or
        modules.CooldownGui.new( frame, player_name, class, cooldown_value, icon_name )

    if should_hide then
      players_with_cooldowns[ player_name ][ spell_name ].frame:Hide()
      return
    end

    players_with_cooldowns[ player_name ][ spell_name ].frame:Show()
    refresh()
  end

  local function start_cooldown( player_name, spell_name, timestamp, on_ready_callback )
    players_with_cooldowns[ player_name ][ spell_name ].set_value( timestamp, on_ready_callback )
  end

  local function remove_player( player_name )
    local cooldowns = players_with_cooldowns[ player_name ]
    if not cooldowns then return end

    for _, cooldown in pairs( cooldowns ) do
      cooldown.frame:Hide()
    end

    refresh()
  end

  local function remove_cooldown( player_name, spell_name )
    local cooldowns = players_with_cooldowns[ player_name ]
    if not cooldowns then return end

    local cooldown = cooldowns[ spell_name ]
    if not cooldown then return end

    cooldown.frame:Hide()
    refresh()
  end

  local function create( width )
    frame = create_main_frame( width, on_drag_stop )
  end

  local function position( point, anchor, anchor_point, x, y )
    frame:SetPoint( point, anchor, anchor_point, x, y )
  end

  local function show()
    for _, cooldowns in pairs( players_with_cooldowns ) do
      for _, cooldown in pairs( cooldowns ) do
        cooldown.resume()
      end
    end

    frame:Show()
  end

  local function hide()
    for _, cooldowns in pairs( players_with_cooldowns ) do
      for _, cooldown in pairs( cooldowns ) do
        cooldown.pause()
      end
    end

    frame:Hide()
  end

  local function toggle()
    if frame:IsVisible() then
      hide()
    else
      show()
    end
  end

  local function show_soulstone( player_name, spell_name )
    local player = players_with_cooldowns[ player_name ]
    if not player then return end

    local cooldown = player[ spell_name ]
    if not cooldown then return end

    cooldown.show_soulstone()
  end

  local function hide_soulstone( player_name, spell_name )
    local player = players_with_cooldowns[ player_name ]
    if not player then return end

    local cooldown = player[ spell_name ]
    if not cooldown then return end

    cooldown.hide_soulstone()
  end

  local function get_visible_cooldown_count()
    local result = 0

    for _, cooldowns in pairs( players_with_cooldowns ) do
      for _, cooldown in pairs( cooldowns ) do
        if cooldown.frame:IsVisible() then result = result + 1 end
      end
    end

    return result
  end

  return {
    create = create,
    position = position,
    show = show,
    hide = hide,
    toggle = toggle,
    add_cooldown = add_cooldown_frame,
    start_cooldown = start_cooldown,
    remove_cooldown = remove_cooldown,
    remove_player = remove_player,
    show_soulstone = show_soulstone,
    hide_soulstone = hide_soulstone,
    get_visible_cooldown_count = get_visible_cooldown_count
  }
end

modules.Gui = M
return M
