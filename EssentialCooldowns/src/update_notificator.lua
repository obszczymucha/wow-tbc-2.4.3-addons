local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.UpdateNotificator then return end

local M = {}

function M.new()
  local m_callbacks = {}

  local function register_for_updates( callback )
    if not callback then return end

    table.insert( m_callbacks, callback )
  end

  local function notify()
    for _, callback in ipairs( m_callbacks ) do
      callback()
    end
  end

  return {
    register_for_updates = register_for_updates,
    notify = notify
  }
end

modules.UpdateNotificator = M
return M
