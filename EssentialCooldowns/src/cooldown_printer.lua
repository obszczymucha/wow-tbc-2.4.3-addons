local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.CooldownPrinter then return end

local M = {}
local class = modules.colors.class
local print = modules.print

function M.new()
  local function print_start( group_member, spell_name, spell_link, target_name )
    local on = target_name and group_member.name ~= target_name and string.format( " on %s", target_name ) or ""
    print( string.format( "%s used %s%s.", class( group_member.name, group_member.class ), spell_link or spell_name, on ) )
  end

  local function print_ready( group_member, spell_name, spell_link )
    print( string.format( "%s's %s is ready.", class( group_member.name, group_member.class ), spell_link or spell_name ) )
  end

  return {
    print_start = print_start,
    print_ready = print_ready
  }
end

modules.CooldownPrinter = M
return M
