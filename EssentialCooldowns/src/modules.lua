---@diagnostic disable: undefined-global, undefined-field
local M = LibStub:NewLibrary( "EssentialCooldowns-Modules", 1 )
if not M then return end

M.api = _G
M.lua = {
  time = time,
  floor = math.floor,
  GetTime = GetTime,
  strmatch = strmatch
}

local hex_colors = {
  white = "ffffff"
}

M.colors = {
  highlight = function( text )
    return string.format( "|cffff9f69%s|r", text )
  end,
  blue = function( text )
    return string.format( "|cff209ff9%s|r", text )
  end,
  white = function( text )
    return string.format( "|cff%s%s|r", hex_colors.white, text )
  end,
  red = function( text )
    return string.format( "|cffff2f2f%s|r", text )
  end,
  orange = function( text )
    return string.format( "|cffff8f2f%s|r", text )
  end,
  grey = function( text )
    return string.format( "|cff9f9f9f%s|r", text )
  end,
  green = function( text )
    return string.format( "|cff10df30%s|r", text )
  end
}

local class_colors = {
  [ "DRUID" ] = "ff7d0a",
  [ "HUNTER" ] = "abd473",
  [ "MAGE" ] = "3fc7eb",
  [ "PALADIN" ] = "f58cba",
  [ "PRIEST" ] = "ffffff",
  [ "ROGUE" ] = "fff569",
  [ "SHAMAN" ] = "0070de",
  [ "WARLOCK" ] = "8788ee",
  [ "WARRIOR" ] = "c79c6e"
}

local function color( r, g, b )
  return { r = r, g = g, b = b }
end

function M.hex_to_normalized_rgb( hex )
  local r = tonumber( hex:sub( 1, 2 ), 16 ) / 255
  local g = tonumber( hex:sub( 3, 4 ), 16 ) / 255
  local b = tonumber( hex:sub( 5, 6 ), 16 ) / 255
  return color( r, g, b )
end

M.colors.get_class_color_hex = function( class )
  return class_colors[ string.upper( class ) ]
end

M.colors.get_class_color_normalized = function( class )
  return M.hex_to_normalized_rgb( M.colors.get_class_color_hex( class ) )
end

M.colors.normalized_white = function() return M.hex_to_normalized_rgb( hex_colors.white ) end

M.colors.cooldown = {}
M.colors.cooldown.player_name_normalized = M.colors.normalized_white

M.colors.hl = M.colors.highlight
M.colors.class = function( text, class )
  local class_color = class and class_colors[ string.upper( class ) ]
  return string.format( "|cff%s%s|r", class_color or "ffffff", text )
end

function M.pretty_print( message, color_fn, module_name )
  if not message then return end

  local c = color_fn and type( color_fn ) == "function" and color_fn or color_fn and type( color_fn ) == "string" and M.colors[ color_fn ] or M.colors.blue
  local module_str = module_name and string.format( "%s%s%s", c( "[ " ), M.colors.white( module_name ), c( " ]" ) ) or ""
  M.api.DEFAULT_CHAT_FRAME:AddMessage( string.format( "%s%s: %s", c( "EssentialCooldowns" ), module_str, message ) )
end

function M.print( message )
  M.api.DEFAULT_CHAT_FRAME:AddMessage( message )
end

function M.print_error( message, module_name )
  M.pretty_print( message, M.colors.red, module_name )
end

function M.count_elements( t, f )
  local result = 0

  for _, v in pairs( t ) do
    if f and f( v ) or not f then
      result = result + 1
    end
  end

  return result
end

local function colour( colors, opacity )
  table.sort( colors, function( a, b ) return a[ 4 ] < b[ 4 ] end )

  return function( value, max_value )
    local v = value > max_value and max_value or value
    local percentage = v / max_value
    local r = colors[ 1 ][ 1 ]
    local g = colors[ 1 ][ 2 ]
    local b = colors[ 1 ][ 3 ]

    for i = 1, #colors - 1 do
      local color_a = colors[ i ]
      local color_b = colors[ i + 1 ]

      if percentage >= color_a[ 4 ] and percentage <= color_b[ 4 ] then
        local pos_a = color_a[ 4 ]
        local pos_b = color_b[ 4 ]
        local ratio = (percentage - pos_a) / (pos_b - pos_a)

        r = color_a[ 1 ] * (1 - ratio) + color_b[ 1 ] * ratio
        g = color_a[ 2 ] * (1 - ratio) + color_b[ 2 ] * ratio
        b = color_a[ 3 ] * (1 - ratio) + color_b[ 3 ] * ratio

        break
      end
    end

    return r, g, b, opacity
  end
end

M.bar_color_fn = function( value, max_value, class )
  local class_color = class and M.hex_to_normalized_rgb( class_colors[ string.upper( class ) ] ) or nil
  local c = class_color or color( 0.32, 0.537, 1 )
  --local f = colour( { { c.r, c.g, c.b, 1 }, { c.r, c.g, c.b, 0.5 }, { 0, 1, 0, 0 } }, 1 )
  local f = colour( { { c.r, c.g, c.b, 1 } }, 1 )
  return f( value, max_value )
end

function M.format_time( time )
  local minutes = math.floor( time / 60 )
  local seconds = math.floor( time % 60 )
  if minutes >= 1 then
    return string.format( "%d:%02d", minutes, seconds )
  elseif seconds >= 10 then
    return tostring( seconds )
  else
    return tostring( seconds )
  end
end

function M.get_addon_version()
  local version = M.api.GetAddOnMetadata( "EssentialCooldowns", "Version" )
  local major, minor = string.match( version, "(%d+)%.(%d+)" )

  local result = {
    str = version,
    major = tonumber( major ),
    minor = tonumber( minor )
  }

  if not version or not result.major or not result.minor then
    error( "Invalid EssentialCooldowns addon version!" )
    return
  end

  return result
end

return M
