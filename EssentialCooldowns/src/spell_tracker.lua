local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.SpellTracker then return end

local M = {}
local api = modules.api
local pretty_print = modules.pretty_print

function M.new( group_tracker, cooldown_tracker, warlock_tracker )
  local function on_event( _, _, ... )
    local timestamp, sub_event, _, source_name, _, _, dest_name, _, spell_id, spell_name = ...

    -- Yep, SPELL_AURA_APPLIED doesn't have source_name, so we need to hack things.
    if not source_name and spell_name == "Soulstone Resurrection" and sub_event == "SPELL_AURA_APPLIED" and api.IsInGroup() then
      local warlock = warlock_tracker.find( timestamp )

      if not warlock then
        local spell = spell_id and api.GetSpellLink( spell_id ) or spell_name
        -- TODO: If this method deems reliable, remove this message.
        pretty_print( string.format( "A warlock cast %s on %s, but I couldn't determine which one.", spell, dest_name or "N/A" ) )
        return
      end

      cooldown_tracker.on_spell_cast( warlock, spell_name, spell_id, dest_name, timestamp )
      return
    end

    local group_member = group_tracker.get_group_member_info( source_name )
    if not group_member then return end

    if spell_name == "Devastate" and sub_event == "SPELL_CAST_SUCCESS" then
      cooldown_tracker.set_spec( group_member, "Protection" )
      return
    end

    if sub_event == "SPELL_CAST_SUCCESS" or (sub_event == "SPELL_CAST_START" and (spell_name == "Rebirth" or spell_name == "Create Soulstone")) then
      cooldown_tracker.on_spell_cast( group_member, spell_name, spell_id, dest_name, timestamp )
      return
    end

    if sub_event == "SPELL_CAST_START" and spell_name == "Soulstone Resurrection" then
      warlock_tracker.track( group_member, timestamp )
      return
    end

    -- There is no confirmation that a druid cast Rebirth successfully.
    -- Nor that a warlock created a soulstone.
    -- So we start tracking on SPELL_CAST_START and reset it here if needed.
    if (spell_name == "Rebirth" or spell_name == "Create Soulstone") and sub_event == "SPELL_CAST_FAILED" then
      cooldown_tracker.on_spell_cast_failed( group_member, spell_name )
    end
  end

  local frame = modules.api.CreateFrame( "Frame" )
  frame:RegisterEvent( "COMBAT_LOG_EVENT_UNFILTERED" )
  frame:SetScript( "OnEvent", on_event )
end

modules.SpellTracker = M
return M
