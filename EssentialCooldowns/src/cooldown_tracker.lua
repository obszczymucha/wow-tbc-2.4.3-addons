local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.CooldownTracker then return end

local M = {}
local api = modules.api
local lua = modules.lua

local SS = "Soulstone Resurrection"

function M.new( db, spell_registry, cooldown_printer, gui )
  local function on_spell_cast( group_member, spell_name, spell_id, target_name, timestamp )
    if spell_name == "Create Soulstone" then
      gui.show_soulstone( group_member.name, SS )
      db.store_spell_info( group_member.name, spell_name, spell_id, target_name, timestamp )
      return
    end

    local spell_info = spell_registry.get_spell_info( group_member.class, spell_name )
    if not spell_info then return end

    local spell_link = spell_id and api.GetSpellLink( spell_id )
    local id = spell_info.name ~= spell_name and spell_info.spell_id or spell_id

    db.store_spell_info( group_member.name, spell_info.name, id, target_name, timestamp )
    cooldown_printer.print_start( group_member, spell_name, spell_link, target_name )
    gui.start_cooldown( group_member.name, spell_info.name, timestamp, function()
      local spec = db.get_spec( group_member.name )
      if spell_info.spec and spell_info.spec ~= spec then return end
      local link = id and api.GetSpellLink( id )
      cooldown_printer.print_ready( group_member, spell_name, link )
    end )

    if spell_name == SS then
      gui.hide_soulstone( group_member.name, SS )
      db.remove_spell_info( group_member.name, "Create Soulstone" )
    end
  end

  local function on_spell_cast_failed( group_member, spell_name )
    if spell_name == "Create Soulstone" then
      gui.hide_soulstone( group_member.name, SS )
      db.remove_spell_info( group_member.name, spell_name )
      return
    end

    local spell_info = db.get_spell_info( group_member.name, spell_name )
    local spell = spell_info and api.GetSpellLink( spell_info.spell_id ) or spell_name

    db.remove_spell_info( group_member.name, spell_name )
    gui.start_cooldown( group_member.name, spell_name, 0, function()
      cooldown_printer.print_ready( group_member, spell_name, spell )
    end )
  end

  local function add_cooldown( group_member, details, should_hide )
    gui.add_cooldown( group_member.name, group_member.class, details.name, details.cooldown, details.icon, should_hide )

    local spell_info = db.get_spell_info( group_member.name, details.name )
    local cooldown = spell_info and details.cooldown - lua.floor( lua.time() - spell_info.timestamp ) or 0

    gui.start_cooldown( group_member.name, details.name, spell_info and spell_info.timestamp or 0, cooldown > 0 and function()
      local spec = db.get_spec( group_member.name )
      if spell_info.spec and spell_info.spec ~= spec then return end
      local spell = api.GetSpellLink( spell_info.spell_id ) or details.name
      cooldown_printer.print_ready( group_member, details.name, spell )
    end or nil )
  end

  local function on_someone_joined_group( group_member )
    local cooldowns = spell_registry.get_spells_for_class( group_member.class )
    local spec = db.get_spec( group_member.name )

    for _, details in ipairs( cooldowns ) do
      add_cooldown( group_member, details, details.spec and details.spec ~= spec )
    end

    local has_soulstone = db.get_spell_info( group_member.name, "Create Soulstone" )

    if has_soulstone then
      local duration = lua.time() - has_soulstone.timestamp

      if duration < 1800 then
        gui.show_soulstone( group_member.name, SS )
        return
      end

      db.remove_spell_info( group_member.name, "Create Soulstone" )
    end

    gui.hide_soulstone( group_member.name, SS )
  end

  local function on_someone_left_group( group_member )
    gui.remove_player( group_member.name )
  end

  local function on_you_left_group( former_group_members )
    for _, group_member in pairs( former_group_members ) do
      gui.remove_player( group_member.name )
    end
  end

  local function set_spec( group_member, spec )
    local current_spec = db.get_spec( group_member.name )
    if current_spec == spec then return end

    db.set_spec( group_member.name, spec )

    local cooldowns = spell_registry.get_spells_for_class( group_member.class )

    for _, details in ipairs( cooldowns ) do
      if details.spec and spec ~= details.spec then
        gui.remove_cooldown( group_member.name, details.name )
      elseif spec == details.spec then
        add_cooldown( group_member, details )
      end
    end
  end

  return {
    on_spell_cast = on_spell_cast,
    on_spell_cast_failed = on_spell_cast_failed,
    on_someone_joined_group = on_someone_joined_group,
    on_someone_left_group = on_someone_left_group,
    on_you_left_group = on_you_left_group,
    set_spec = set_spec
  }
end

modules.CooldownTracker = M
return M
