local lib_stub = LibStub
local modules = lib_stub( "EssentialCooldowns-Modules" )
if modules.VersionBroadcast then return end

local M = {}

local api = modules.api
local ace_comm = lib_stub( "AceComm-3.0" )
local comm_prefix = "EssentialCooldowns"

local function strip_dots( v )
  local result, _ = v:gsub( "%.", "" )
  return result
end

function M.new( db, version )
  local function version_recently_reminded()
    local reminder_timestamp = db.last_new_version_reminder_timestamp()
    if not reminder_timestamp then return false end

    local time = modules.lua.time()

    -- Only remind once a day
    if time - reminder_timestamp > 3600 * 24 then
      return false
    else
      return true
    end
  end

  local function broadcast_version( target )
    ace_comm:SendCommMessage( comm_prefix, "VERSION::" .. version, target )
  end

  local function broadcast_version_to_the_guild()
    if not api.IsInGuild() then return end
    broadcast_version( "GUILD" )
  end

  local function is_new_version( v )
    local myVersion = tonumber( strip_dots( version ) )
    local theirVersion = tonumber( strip_dots( v ) )

    return theirVersion > myVersion
  end

  local function broadcast_version_to_the_group()
    if not api.IsInGroup() and not api.IsInRaid() then return end
    broadcast_version( api.IsInRaid() and "RAID" or "PARTY" )
  end

  -- OnComm(prefix, message, distribution, sender)
  local function on_comm( prefix, message, _, _ )
    if prefix ~= comm_prefix then return end

    local cmd, value = modules.lua.strmatch( message, "^(.*)::(.*)$" )

    if cmd == "VERSION" and is_new_version( value ) and not version_recently_reminded() then
      db.set_last_new_version_reminder_timestamp( modules.lua.time() )
      modules.pretty_print( string.format( "New version (%s) is available!", modules.colors.highlight( string.format( "v%s", value ) ) ) )
    end
  end

  local function broadcast()
    broadcast_version_to_the_guild()
    broadcast_version_to_the_group()
  end

  ace_comm:RegisterComm( comm_prefix, on_comm )

  return {
    broadcast = broadcast
  }
end

modules.VersionBroadcast = M
return M
