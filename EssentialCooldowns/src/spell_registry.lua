local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.SpellRegistry then return end

local M = {}

local m_spells = {
  [ "Druid" ] = {
    {
      name = "Innervate",
      icon = "Interface\\Icons\\Spell_nature_lightning",
      cooldown = 360,
      --spell_id = 29166,
      --shares_cooldown_with = { "Lifebloom" }
    },
    {
      name = "Rebirth",
      icon = "Interface\\Icons\\Spell_nature_reincarnation",
      cooldown = 1202 -- Extra 2 seconds because we start tracking from the spell cast start.
    }
  },
  [ "Warrior" ] = {
    {
      name = "Last Stand",
      icon = "Interface\\Icons\\spell_holy_ashestoashes",
      cooldown = 480,
      spec = "Protection"
    },
    {
      name = "Shield Wall",
      icon = "Interface\\Icons\\ability_warrior_shieldwall",
      cooldown = 1800,
      spell_id = 871,
      shares_cooldown_with = { "Recklessness", "Retaliation" },
      spec = "Protection"
    }
  },
  [ "Warlock" ] = {
    {
      name = "Last Stand",
      icon = "Interface\\Icons\\spell_holy_ashestoashes",
      cooldown = 480,
      spec = "Protection"
    },
    {
      name = "Soulstone Resurrection",
      class = "Warlock",
      icon = "Interface\\Icons\\Spell_shadow_soulgem",
      cooldown = 1800
    }
  },
  [ "Paladin" ] = {
    {
      name = "Blessing of Protection",
      class = "Paladin",
      icon = "Interface\\Icons\\Spell_holy_sealofprotection",
      cooldown = 300
    }
  },
  [ "Priest" ] = {
    {
      name = "Fear Ward",
      class = "Priest",
      icon = "Interface\\Icons\\Spell_holy_excorcism",
      cooldown = 180
    }
  },
  [ "Shaman" ] = {
    {
      name = "Bloodlust",
      class = "Shaman",
      icon = "Interface\\Icons\\Spell_nature_bloodlust",
      cooldown = 600
    }
  },
  [ "Hunter" ] = {
    {
      name = "Misdirection",
      class = "Hunter",
      icon = "Interface\\Icons\\Ability_hunter_misdirection",
      cooldown = 120
    }
  }
}

function M.new()
  local function get_spell_info( class, spell_name )
    local spells = m_spells[ class ]
    if not spells then return nil end

    for _, spell_info in ipairs( spells ) do
      if spell_info.name == spell_name then return spell_info end

      if spell_info.shares_cooldown_with then
        for _, other_spell in ipairs( spell_info.shares_cooldown_with ) do
          if other_spell == spell_name then
            return spell_info
          end
        end
      end
    end

    return nil
  end

  local function get_spells_for_class( class )
    return m_spells[ class ] or {}
  end

  return {
    get_spell_info = get_spell_info,
    get_spells_for_class = get_spells_for_class
  }
end

modules.SpellRegistry = M
return M
