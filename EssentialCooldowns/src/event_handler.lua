local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.EventHandler then return end

local M = {}
local m_frame

function M.handle_events( main )
  local function on_event( _, event )
    if event == "PLAYER_LOGIN" then
      main.initialize()
    end
  end

  if not m_frame then
    m_frame = modules.api.CreateFrame( "FRAME", nil )

    m_frame:RegisterEvent( "PLAYER_LOGIN" )
    m_frame:SetScript( "OnEvent", on_event )
  end
end

modules.EventHandler = M
return M
