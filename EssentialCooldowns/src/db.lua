local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.Db then return end

local M = {}

function M.new( version )
  local function init()
    local db = LibStub( "AceDB-3.0" ):New( "EssentialCooldownsDb" )

    if not db.global.version then
      db.global.version = version
    end

    if not db.char.settings then
      db.char.settings = {}
    end

    if not db.realm.spells then
      db.realm.spells = {}
    end

    if not db.realm.specs then
      db.realm.specs = {}
    end

    return db
  end

  local db = init()

  local function last_new_version_reminder_timestamp()
    return db.char.last_new_version_reminder_timestamp
  end

  local function set_last_new_version_reminder_timestamp( timestamp )
    db.char.last_new_version_reminder_timestamp = timestamp
  end

  local function class_colors()
    return db.char.settings.class_colors
  end

  local function store_spell_info( player_name, spell_name, spell_id, target_name, timestamp )
    db.realm.spells[ player_name ] = db.realm.spells[ player_name ] or {}
    db.realm.spells[ player_name ][ spell_name ] = db.realm.spells[ player_name ][ spell_name ] or {}
    db.realm.spells[ player_name ][ spell_name ].spell_id = spell_id
    db.realm.spells[ player_name ][ spell_name ].target_name = target_name
    db.realm.spells[ player_name ][ spell_name ].timestamp = timestamp
  end

  local function remove_spell_info( player_name, spell_name )
    if not db.realm.spells[ player_name ] then return end
    db.realm.spells[ player_name ][ spell_name ] = nil
  end

  local function get_spell_info( player_name, spell_name )
    local player = db.realm.spells[ player_name ]
    if not player then return nil end

    return player[ spell_name ]
  end

  local function set_spec( player_name, spec )
    db.realm.specs[ player_name ] = spec
  end

  local function get_spec( player_name )
    return db.realm.specs[ player_name ]
  end

  local function store_debug_info( debug_info )
    local result = db.char.debug_info or {}
    table.insert( result, debug_info )
    db.char.debug_info = result
  end

  local function set_position( anchor, other_anchor, x, y )
    db.char.settings.position = { anchor = anchor, other_anchor = other_anchor, x = x, y = y }
  end

  local function get_position()
    return db.char.settings.position or { anchor = "RIGHT", other_anchor = "RIGHT", x = -18, y = 195 }
  end

  return {
    init = init,
    last_new_version_reminder_timestamp = last_new_version_reminder_timestamp,
    set_last_new_version_reminder_timestamp = set_last_new_version_reminder_timestamp,
    class_colors = class_colors,
    store_spell_info = store_spell_info,
    remove_spell_info = remove_spell_info,
    get_spell_info = get_spell_info,
    set_spec = set_spec,
    get_spec = get_spec,
    store_debug_info = store_debug_info,
    set_position = set_position,
    get_position = get_position
  }
end

modules.Db = M
return M
