local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.WarlockTracker then return end

local M = {}

-- In the shitty 2.4.3 API (or maybe it's the shitty server),
-- there is no SPELL_CAST_SUCCESS or it doesn't have target_name
-- (I don't remember now). Anyways, there's no way for us to
-- easily determine which warlock casted soulstone successfully.
-- So we have to track all warlock who "start" casting and then
-- correlate them with SPELL_AURA_APPLIED.
function M.new( db )
  local m_soulstone_casts = {}

  -- We'll be storing each warlock in a list in reverse order,
  -- so we can iterate the list normally and find the one who
  -- cast first.
  local function track( group_member, soulstone_cast_start_timestamp )
    m_soulstone_casts[ #m_soulstone_casts + 1 ] = { group_member = group_member, timestamp = soulstone_cast_start_timestamp }
  end

  local function store_debug_info( soulstone_applied_timestamp )
    local warlocks = {}

    for i = 1, #m_soulstone_casts do
      local warlock = m_soulstone_casts[ i ]

      if warlock then
        local result = {}
        result.name = warlock.group_member.name
        result.timestamp = warlock.timestamp
        local cast_time = soulstone_applied_timestamp - warlock.timestamp
        result.cast_time = cast_time

        table.insert( warlocks, result )
      end
    end

    local debug_info = {
      type = "warlock_tracking",
      soulstone_applied_timestamp = soulstone_applied_timestamp,
      warlocks = warlocks
    }

    db.store_debug_info( debug_info )
  end

  local function find( soulstone_applied_timestamp )
    for i = 1, #m_soulstone_casts do
      local warlock = m_soulstone_casts[ i ]

      if warlock then
        local cast_time = soulstone_applied_timestamp - warlock.timestamp

        -- The cast time is 3 seconds, but I've seen the the above calculation
        -- resulting in 2.96. Perhaps local/server clock sync issues?
        if cast_time >= 2.8 and cast_time <= 3.3 then -- cast 3 seconds ago (+server processing window)
          m_soulstone_casts[ i ] = nil
          return warlock.group_member
        end
      end
    end

    store_debug_info( soulstone_applied_timestamp )
    return nil
  end

  return {
    track = track,
    find = find
  }
end

modules.WarlockTracker = M
return M
