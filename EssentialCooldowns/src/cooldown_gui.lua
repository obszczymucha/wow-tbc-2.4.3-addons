local modules = LibStub( "EssentialCooldowns-Modules" )
if modules.CooldownGui then return end

local M = {}

local api = modules.api
local lua = modules.lua
local color_fn = modules.bar_color_fn
local format_time = modules.format_time
local REFRESH_INTERVAL = 0.05

local function create_font_frame( parent )
  local frame = parent:CreateFontString( nil, "OVERLAY", "GameFontNormal" )
  frame:SetFont( "Interface\\AddOns\\EssentialCooldowns\\assets\\ABF.ttf", 12 )

  return frame
end

local function create_cooldown_frame( parent_frame, height, opacity )
  local frame = api.CreateFrame( "Frame", nil, parent_frame )
  frame:SetPoint( "LEFT", parent_frame, "LEFT", 0, 0 )
  frame:SetPoint( "RIGHT", parent_frame, "RIGHT", 0, 0 )
  frame:SetHeight( height )
  frame:SetBackdrop( {
    bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
    edgeFile = nil,
    edgeSize = 0,
    insets = { left = 0, right = 0, top = 0, bottom = 0 }
  } )
  frame:SetBackdropColor( 0, 0, 0, opacity or 0.2 )

  return frame
end

local function create_icon( parent_frame, icon_texture, mask_path, icon_size )
  local frame = api.CreateFrame( "Frame", nil, parent_frame )
  local size = icon_size or parent_frame:GetHeight() - 2
  local padding = (parent_frame:GetHeight() - size) / 2

  frame:SetWidth( size )
  frame:SetHeight( size )
  frame:SetPoint( "LEFT", parent_frame, "LEFT", padding, 0 )
  frame:SetPoint( "TOP", parent_frame, "TOP", 0, -padding )

  local texture = frame:CreateTexture( nil, "BACKGROUND" )
  texture:SetAllPoints( frame )
  texture:SetTexture( icon_texture )
  local topleft = 2 / size
  local bottomright = (size - 2) / size
  texture:SetTexCoord( topleft, bottomright, topleft, bottomright )
  if mask_path then frame:SetMask( mask_path ) end

  frame.texture = texture

  return frame
end

local function create_soulstone_icon( parent_frame, icon_texture, icon_size )
  local frame = api.CreateFrame( "Frame", nil, parent_frame )
  local size = icon_size or parent_frame:GetHeight() - 2

  frame:SetWidth( size )
  frame:SetHeight( size )

  local texture = frame:CreateTexture( nil, "BACKGROUND" )
  texture:SetAllPoints( frame )
  texture:SetTexture( icon_texture )
  frame.texture = texture

  return frame
end

local function set_statusbar_color( frame, value, class )
  local _, max = frame:GetMinMaxValues()
  local r, g, b, a = color_fn( value, max, class )
  frame:SetStatusBarColor( r, g, b, a )
end

local function create_statusbar_frame( parent_frame )
  local frame = api.CreateFrame( "StatusBar", nil, parent_frame )
  frame:SetStatusBarTexture( "Interface\\AddOns\\EssentialCooldowns\\assets\\Minimalist" )
  frame:SetFrameStrata( "LOW" )

  return frame
end

function M.new( parent_frame, player_name, class, cooldown, icon_name )
  local missed_callback = false
  local cast_timestamp
  local on_ready_callback
  local frame = create_cooldown_frame( parent_frame, 20 )
  local icon = create_icon( frame, icon_name )
  local statusbar = create_statusbar_frame( frame )

  local padding = 0.5
  statusbar:SetPoint( "TOP", 0, -padding )
  statusbar:SetPoint( "BOTTOM", 0, padding )
  statusbar:SetPoint( "LEFT", icon, "RIGHT", 0, 0 )
  statusbar:SetPoint( "RIGHT", 0, 0 )
  statusbar:SetMinMaxValues( 0, cooldown )
  statusbar:SetValue( 0 )

  local player = create_font_frame( frame )
  player:SetPoint( "LEFT", statusbar, "LEFT", 5, 1 )
  player:SetText( player_name )

  local set_player_name_color = function( color )
    local class_color = color or modules.colors.get_class_color_normalized( class )
    player:SetTextColor( class_color.r, class_color.g, class_color.b, 1 )
  end

  local status = create_font_frame( frame )
  status:SetPoint( "RIGHT", statusbar, "RIGHT", -5, 1 )
  status:SetTextColor( 1, 1, 1, 1 )

  local soulstone_icon = create_soulstone_icon( frame, "Interface\\AddOns\\EssentialCooldowns\\assets\\soulstone.blp", 8 )
  soulstone_icon:SetPoint( "RIGHT", status, "LEFT", -1, 0 )
  soulstone_icon:SetPoint( "TOP", status, "TOP", 0, -3 )
  soulstone_icon:Hide()

  local function disable_updates()
    statusbar:SetScript( "OnUpdate", nil )
  end

  local function set_ready()
    disable_updates()
    status:SetText( "Ready" )
    status:SetTextColor( 0, 0.8, 0.3, 1 )
    set_player_name_color()
  end

  local function on_update( value )
    set_statusbar_color( statusbar, value, class )

    if value > 0 then
      local v = value + 1
      status:SetText( format_time( v > cooldown and cooldown or v ) )
      return
    end

    set_ready()
    if on_ready_callback then on_ready_callback() end
  end

  local function enable_updates()
    local accumulated_time = 0
    local last_time = lua.GetTime()

    statusbar:SetScript( "OnUpdate", function( self )
      local current_time = lua.GetTime()
      local elapsed = current_time - last_time
      last_time = current_time

      accumulated_time = accumulated_time + elapsed
      if accumulated_time < REFRESH_INTERVAL then return end
      accumulated_time = accumulated_time - REFRESH_INTERVAL

      local value = self:GetValue()
      self:SetValue( value - REFRESH_INTERVAL )

      on_update( value )
    end )
  end

  local function pause()
    disable_updates()
  end

  local function run()
    if cast_timestamp == 0 then
      set_ready()
      return
    end

    local value = cooldown - (lua.time() - cast_timestamp)
    disable_updates()
    set_statusbar_color( statusbar, value or cooldown, class )
    statusbar:SetValue( value or cooldown )

    if value <= 0 then
      set_ready()
      if missed_callback == false and on_ready_callback then on_ready_callback() end
    else
      status:SetTextColor( 1, 1, 1, 1 )
      set_player_name_color( modules.colors.normalized_white() )
      enable_updates()
    end
  end

  local function resume()
    local value = cooldown - (lua.time() - cast_timestamp)
    if value <= 0 then missed_callback = true end
    run()
  end

  local function set_value( timestamp, callback )
    cast_timestamp = timestamp
    on_ready_callback = callback
    missed_callback = false

    run()
  end

  local function show_soulstone()
    soulstone_icon:Show()
  end

  local function hide_soulstone()
    soulstone_icon:Hide()
  end

  return {
    frame = frame,
    set_value = set_value,
    pause = pause,
    resume = resume,
    show_soulstone = show_soulstone,
    hide_soulstone = hide_soulstone,
  }
end

modules.CooldownGui = M
return M
