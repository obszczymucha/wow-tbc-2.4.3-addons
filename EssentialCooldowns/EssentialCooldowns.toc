## Interface: 20400
## Title: |cff209ff9Essential Cooldowns|r
## Author: Obszczymucha
## Version: 1.3
## Notes: Tracks essential raid cooldowns.
## SavedVariables: EssentialCooldownsDb

libs\libs.xml
src\wowapi.lua
src\modules.lua
src\cooldown_gui.lua
src\cooldown_printer.lua
src\cooldown_tracker.lua
src\db.lua
src\event_handler.lua
src\group_tracker.lua
src\gui.lua
src\spell_registry.lua
src\spell_tracker.lua
src\update_notificator.lua
src\version_broadcast.lua
src\warlock_tracker.lua
main.lua
