## Interface: 20400
## Title: Development Tools (v1.7)
## Notes: Debgugging, Exploration, and Diagnostic Tools (v1.7)
## Author: Daniel Stephens (daniel@vigilance-committee.org), Iriel (iriel@vigilance-committee.org)
## OptionalDeps: Blizzard_CombatLog
## SavedVariables: DevTools_Options, DevTools_Events
DevTools.lua
DevToolsFormat.lua
DevToolsVisFrame.lua
DevToolsDump.lua
DevToolsChatEvent.lua
DevToolsFrameStack.lua
DevToolsTreeList.lua
DevToolsEventTraceStateList.lua
DevToolsEventTraceClosure.lua
DevToolsEventTrace.lua
DevToolsEventTraceFormats.lua
DevToolsEventTraceVisual.lua
DevToolsEventTraceOptions.lua

