Timer = LibStub( "AceAddon-3.0" ):NewAddon( "Timer", "AceTimer-3.0" )

local timerRegistry = {}

local function SetSize( self, width, height )
	if not self then return end
	
	self:SetWidth( width )
	self:SetHeight( height )
end

local function CreateTimerFrame( name )
	local fullName = format( "%sTimerFrame", name )
	local frame = _G[ fullName ]

	if frame then
		error( format( "Frame %s already exists. Choose different name for your timer.", fullName ) )
		return
	end

	frame = CreateFrame( "FRAME", fullName, UIParent )
	frame.SetSize = SetSize

	local label = frame:CreateFontString( nil, "OVERLAY" )
	label:SetFont( "FONTS\\FRIZQT__.TTF", 14, "OUTLINE" )
	label:SetPoint( "TOPLEFT", 0, 0 )
	label:SetText( "00:00" )
	frame:SetBackdropColor( 0.1, 0.1, 0.1, 0 )
	frame:SetBackdropBorderColor( 0.1, 0.1, 0.1, 0 )
	frame:SetClampedToScreen( false )
	
	frame.label = label
	frame:SetSize( label:GetWidth(), label:GetHeight() )
	frame:SetPoint( "TOPLEFT", UIParent, "TOPLEFT", ( UIParent:GetWidth() / 2 ) - ( frame:GetWidth() / 2 ), -240 )
	frame:SetMovable( true )
	frame:EnableMouse( true )
	frame:RegisterForDrag( "LeftButton" )
	frame:SetScript( "OnDragStart", frame.StartMoving )
	frame:SetScript( "OnDragStop", frame.StopMovingOrSizing )
	frame:SetFrameStrata( "HIGH" )
	frame:Hide()

	return frame
end

local function Update( self )
	local label = self.frame.label
	label:SetText( date( self.format, self.elapsed ) )
	self.frame:SetSize( label:GetWidth(), label:GetHeight() )
end

local function Start( self )
	if self.timerId then return end

	self.startTime = time()
	if self.elapsed then self.startTime = self.startTime - self.elapsed end

	self:Tick()

	self.timerId = Timer:ScheduleRepeatingTimer( function() self:Tick() end, 1 )
end

local function Tick( self )
	if not self.startTime then return end

	self.elapsed = time() - self.startTime
	Update( self )
end

local function Stop( self )
	if not self.startTime then return end

	self.elapsed = time() - self.startTime
	self.startTime = nil
	Timer:CancelTimer( self.timerId )
	self.timerId = nil
end

local function Reset( self )
	self:Stop()
	self.elapsed = 0
	Update( self )
end

local function IsRunning( self )
	if self.timerId then
		return true
	else
		return false
	end
end

function Timer.New( name )
	if timerRegistry[ name ] then return timerRegistry[ name ] end

	local timer = {
		format = "%M:%S",
		frame = CreateTimerFrame( name ),
		Start = Start,
		Tick = Tick,
		Reset = Reset,
		Stop = Stop,
		IsRunning = IsRunning,
		Show = function( self ) self.frame:Show() end,
		Hide = function( self ) self.frame:Hide() end
	}

	timerRegistry[ name ] = timer

	return timer
end

local function PrintUsage()
	ChatFrame1:AddMessage( "Usage: <command> <timer name>" )
end

local function RunCommand( command, timerName )
	if not command or not timerName then
		PrintUsage()
		return
	end

	local timer = timerRegistry[ timerName ]

	if not timer then
		ChatFrame1:AddMessage( format( "Timer %s not found.", timerName ) )
		return
	end

	if command == "start" then
		timer:Start()
	elseif command == "stop" then
		timer:Stop()
	elseif command == "reset" then
		timer:Reset()
	elseif command == "show" then
		timer:Show()
	elseif command == "hide" then
		timer:Hide()
	else
		ChatFrame1:AddMessage( "Unknown command. Try: start, stop, reset, show, hide" )
	end
end

local function ProcessSlashCommand( paramString )
	if not paramString or paramString == "" then
		PrintUsage()
		return
	end

	local params = {}
	
	for str in string.gmatch( paramString, "[^%s]+" ) do
		table.insert( params, str )
	end

	if #params < 2 then
		PrintUsage()
		return
	end

	local command = params[ 1 ]
	local timerName = params[ 2 ]

	RunCommand( command, timerName )
end

SLASH_TIMER1 = "/timer"
SlashCmdList[ "TIMER" ] = ProcessSlashCommand
