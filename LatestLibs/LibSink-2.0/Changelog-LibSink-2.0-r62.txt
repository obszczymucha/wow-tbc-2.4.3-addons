------------------------------------------------------------------------
r62 | nevcairiel | 2009-03-06 10:12:41 +0000 (Fri, 06 Mar 2009) | 1 line
Changed paths:
   M /trunk/LibSink-2.0.toc

Restore LoD to Libraries, as 3.0.8 fixed all bugs that blocked LoD OptionalDeps to load properly.
------------------------------------------------------------------------
r60 | nevcairiel | 2009-03-03 15:06:38 +0000 (Tue, 03 Mar 2009) | 1 line
Changed paths:
   M /trunk/LibSink-2.0/LibSink-2.0.lua

Fix embed upgrading in LibSink-2.0 for any future versions
------------------------------------------------------------------------
