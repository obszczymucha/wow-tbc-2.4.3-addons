------------------------------------------------------------------------
r3 | nevcairiel | 2008-10-09 21:52:44 +0000 (Thu, 09 Oct 2008) | 1 line
Changed paths:
   M /trunk/LibGUIDRegistry-0.1/LibGUIDRegistry-0.1.lua
   M /trunk/LibGUIDRegistry-0.2/LibGUIDRegistry-0.2.lua

WoWAce Post-Processing: Virtually inflate Library Revision numbers for proper upgrade path
------------------------------------------------------------------------
r2 | root | 2008-09-29 21:49:36 +0000 (Mon, 29 Sep 2008) | 1 line
Changed paths:
   A /trunk/.pkgmeta

Facilitate WowAce-on-CurseForge transition
------------------------------------------------------------------------
r1 | root | 2008-09-29 21:19:01 +0000 (Mon, 29 Sep 2008) | 1 line
Changed paths:
   A /branches
   A /tags
   A /trunk
   A /trunk/LibGUIDRegistry-0.1
   A /trunk/LibGUIDRegistry-0.1/LibGUIDRegistry-0.1.lua
   A /trunk/LibGUIDRegistry-0.1/lib.xml
   A /trunk/LibGUIDRegistry-0.1.toc
   A /trunk/LibGUIDRegistry-0.2
   A /trunk/LibGUIDRegistry-0.2/LibGUIDRegistry-0.2.lua
   A /trunk/LibGUIDRegistry-0.2/lib.xml
   A /trunk/LibStub
   A /trunk/LibStub/LibStub.lua
   A /trunk/embeds.xml

Initial import of HEAD
------------------------------------------------------------------------
