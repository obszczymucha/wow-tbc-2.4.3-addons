-- This file is loaded from "HerbCount.toc"

HerbCount = LibStub( "AceAddon-3.0" ):NewAddon( "HerbCount", "AceConsole-3.0", "AceEvent-3.0" )
local frameWidth = 45
local showItemName = false
local verticalLayout = false

if showItemName then
	frameWidth = 120
end

local function SetSize( frame, width, height )
	if not frame then return end
	
	frame:SetWidth( width )
	frame:SetHeight( height )
end

function CountHerbs(itemID)
	local _, itemLink = GetItemInfo(itemID)
	local total = 0

	for bag = 0, NUM_BAG_SLOTS do
		for slot = 1, GetContainerNumSlots(bag) do
			if (GetContainerItemLink(bag, slot) == itemLink) then
				if select(2, GetContainerItemInfo(bag, slot)) then
					total = total + select(2, GetContainerItemInfo(bag, slot))
				end
			end
		end
	end
	return total
end

function initiateHerb( parentFrame, name, id )
	local frame = CreateFrame( "frame", name, parentFrame )
	SetSize( frame, 12, 12 )
	
	local icon = CreateFrame( "frame", "icon", frame )
	SetSize( icon, 12, 12 )
	icon:SetPoint( "TOPLEFT", 0, 0 )

	local texture = icon:CreateTexture(nil, "BACKGROUND")
	texture:SetTexture( GetItemIcon(id) )
	texture:SetAllPoints( icon )
	icon.texture = texture
	
	local label = frame:CreateFontString(nil, "OVERLAY" )
	label:SetFont("FONTS\\FRIZQT__.TTF", 10, "OUTLINE")
	
	if showItemName then
		label:SetPoint( "TOPLEFT", 20, -1 )
	else
		label:SetPoint( "TOPLEFT", 16, -1 )
	end

	frame.label = label
	
	parentFrame[name] = frame
end


local herbs = {
	peacebloom = 2447,
	silverleaf = 765,
	earthroot = 2449,
	mageroyal = 785,
	briarthorn = 2450,
	swiftthistle = 2452,
	bruiseweed = 2453,
	wildsteelbloom = 3355,
	gravemoss = 3369,
	kingsblood = 3356,
	khadgarswhisker = 3358,
	stranglekelp = 3820,
	goldthorn = 3821,
	primalwater = 21885,
	moteofwater = 22578,
	primalair = 22451,
	moteofair = 22572,
	primalmana = 22457,
	moteofmana = 22576,
	primalshadow = 22456,
	moteofshadow = 22577,
	gromsblood = 8846,
	sungrass = 8838,
	dreamingglory = 22786,
	goldensansam = 13464,
	felweed = 22785,
	fellotus = 22794,
	terocone = 22789
}

local displayedHerbs = {
--	"peacebloom",
--	"silverleaf",
--	"earthroot",
--	"mageroyal",
--	"briarthorn",
--	"swiftthistle",
--	"bruiseweed",
--	"wildsteelbloom",
--	"gravemoss",
--	"kingsblood",
--	"khadgarswhisker",
--	"stranglekelp",
--	"goldthorn",
--	"sungrass",
	"primalwater",
	"moteofwater",
	"primalair",
	"moteofair",
	"primalmana",
	"moteofmana",
	"primalshadow",
	"moteofshadow",
	"gromsblood",
	"felweed",
	"terocone",
	"goldensansam",
	"dreamingglory",
	"fellotus"
}

local herbCountFrame = CreateFrame( "frame", "herbCountFrame" )
herbCountFrame.count = 0
SetSize( herbCountFrame, frameWidth, #displayedHerbs * 15 + 5 )

herbCountFrame:SetBackdrop(
	{
		bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
		edgeFile = "Interface\\ChatFrame\\ChatFrameBackground",
		tile = true,
		tileSize = 5,
		edgeSize = 2,
	}
)

herbCountFrame:SetBackdropColor( 0.1, 0.1, 0.1, 0 )
herbCountFrame:SetBackdropBorderColor( 0.1, 0.1, 0.1, 0 )

for i = 1, #displayedHerbs do
	local name = displayedHerbs[ i ]
	local id = herbs[ name ]
	initiateHerb( herbCountFrame, name, id )
end

herbCountFrame:SetFrameStrata( "BACKGROUND" )
herbCountFrame:SetPoint( "TOPLEFT", UIParent, "TOPLEFT" )
herbCountFrame:SetMovable( true )
herbCountFrame:EnableMouse( true )
herbCountFrame:RegisterForDrag( "LeftButton" )
herbCountFrame:SetScript( "OnDragStart", herbCountFrame.StartMoving )
herbCountFrame:SetScript( "OnDragStop", herbCountFrame.StopMovingOrSizing )
herbCountFrame:SetClampedToScreen( false )

local updateCount = CreateFrame( "frame" )
updateCount:RegisterEvent( "BAG_UPDATE" )
updateCount:SetScript( "OnEvent", function()
	local count = {}
	local yposition = -5
	local availableHerbs = 0
	
	for i = 1, #displayedHerbs do
		local name = displayedHerbs[i]
		local id = herbs[name]
		count[name] = CountHerbs(id)

		if count[name] and count[name] > 0 then
			local itemName = GetItemInfo(id) or "unknown"
			
			local text = itemName .. ": "

			if showItemName then
				text = text .. count[name]
			else
				text = count[name]
			end

			herbCountFrame[name].label:SetText(text)
			
			if verticalLayout then
				herbCountFrame[name]:SetPoint( "TOPLEFT", 5, yposition )
			else
				herbCountFrame[name]:SetPoint( "TOPLEFT", 5 + frameWidth * availableHerbs, -5 )
			end
			
			herbCountFrame[name]:Show()
			availableHerbs = availableHerbs + 1
			yposition = yposition - 15
		else
			herbCountFrame[name]:Hide()
		end
	end

	if verticalLayout then
		herbCountFrame:SetWidth(frameWidth)
		herbCountFrame:SetHeight(availableHerbs * 15 + 5)
	else
		herbCountFrame:SetWidth( frameWidth * availableHerbs )
		herbCountFrame:SetHeight( 20 )
	end

	herbCountFrame.count = availableHerbs

	if availableHerbs > 0 then
		HerbCount:SendMessage( "HERB_COUNT_HERBS_AVAILABLE" )
	else
		herbCountFrame:Hide()
	end
end)

function HerbCount.ShowHerbCountFrame()
	if herbCountFrame.count > 0 then
		herbCountFrame:Show()
	end
end

function HerbCount.HideHerbCountFrame()
	herbCountFrame:Hide()
end

local function HerbCountCommands(msg, editbox)
	if msg == 'hide' then
		herbCountFrame:Hide()
	elseif msg == 'show' then
		herbCountFrame:Show()
	elseif herbCountFrame:IsVisible() then
		herbCountFrame:Hide()
	else
		herbCountFrame:Show()
  end
end

SLASH_HERBCOUNT1 = "/herbcount"
SlashCmdList["HERBCOUNT"] = HerbCountCommands
