local E = ModUi:NewExtension( "ShadowUFTarget" )

function E.Initialize()
  ---@diagnostic disable-next-line: undefined-global
  if not ShadowUF then
    E:DebugMsg( "ShadowUF not available. Disabling." )
    E.enabled = false
    return
  end
end

local function OnTargetAcquired( component, callback )
  ---@diagnostic disable-next-line: undefined-global
  if not SUFUnittarget then error( "SUFUnittarget not available." ) end
  ---@diagnostic disable-next-line: undefined-global
  SUFUnittarget:HookScript( "OnShow", function() if E.enabled and not E.suspended and component.enabled then callback() end end )
end

local function OnTargetLost( component, callback )
  ---@diagnostic disable-next-line: undefined-global
  if not SUFUnittarget then error( "SUFUnittarget not available." ) end
  ---@diagnostic disable-next-line: undefined-global
  SUFUnittarget:HookScript( "OnHide", function() if E.enabled and not E.suspended and component.enabled then callback() end end )
end

local function OnTargetOfTargetAcquired( component, callback )
  ---@diagnostic disable-next-line: undefined-global
  if not SUFUnittargettarget then error( "SUFUnittargettarget not available." ) end
  ---@diagnostic disable-next-line: undefined-global
  SUFUnittargettarget:HookScript( "OnShow", function() if E.enabled and not E.suspended and component.enabled then callback() end end )
end

local function OnTargetOfTargetLost( component, callback )
  ---@diagnostic disable-next-line: undefined-global
  if not SUFUnittargettarget then error( "SUFUnittargettarget not available." ) end
  ---@diagnostic disable-next-line: undefined-global
  SUFUnittargettarget:HookScript( "OnHide", function() if E.enabled and not E.suspended and component.enabled then callback() end end )
end

function E.ExtendComponent( component )
  component.OnTargetAcquired = OnTargetAcquired
  component.OnTargetLost = OnTargetLost
  component.OnTargetOfTargetAcquired = OnTargetOfTargetAcquired
  component.OnTargetOfTargetLost = OnTargetOfTargetLost
end
