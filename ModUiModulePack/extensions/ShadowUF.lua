local E = ModUi:NewExtension( "ShadowUF" )

function E.Initialize()
  ---@diagnostic disable-next-line: undefined-global
  if not ShadowUF then
    E:DebugMsg( "ShadowUF not available. Disabling." )
    E.enabled = false
    return
  end
end

