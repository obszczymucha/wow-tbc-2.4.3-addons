local E = ModUi:NewExtension( "ForceRaid" )
local OnForceRaid, EmitForceRaidEvent = E:RegisterCallback( "forceRaid" )
local forceRaid = false

local function Toggle()
	forceRaid = not forceRaid
	EmitForceRaidEvent()
end

function E.Initialize()
	SLASH_TOGGLERAID1 = "/toggleraid"
	SlashCmdList[ "TOGGLERAID" ] = Toggle

	SLASH_TR1 = "/tr"
	SlashCmdList[ "TR" ] = Toggle
end

function E.ExtendComponent( component )
	component.IsForceRaid = function() return forceRaid end
	component.OnForceRaid = OnForceRaid
end
