local ModUi = LibStub:GetLibrary( "ModUi-1.0", true )
local E = ModUi:NewExtension( "Class" )

local function IsClass( className )
	return
	function()
		_, class = UnitClass( "player" )
		return class == className
	end
end

function E.ExtendComponent( component )
	component.IsWarrior = IsClass( "WARRIOR" )
	component.IsPriest = IsClass( "PRIEST" )
	component.IsDruid = IsClass( "DRUID" )
	component.IsShaman = IsClass( "SHAMAN" )
	component.IsHunter = IsClass( "HUNTER" )
	component.IsRogue = IsClass( "ROGUE" )
	component.IsPaladin = IsClass( "PALADIN" )
	component.IsWarlock = IsClass( "WARLOCK" )
	component.IsMage = IsClass( "MAGE" )
end
