local E = ModUi:NewExtension( "FarmingAnchor", { "FarmingMode" } )
local frame
local isVisible = false
local position = { "TOP", "TOP", 0, -230 }

local function Update()
	if isVisible and E:IsFarmingMode() then
		E:Show( frame )
		E:DebugMsg( "Visible" )
	else
		E:Hide( frame )
		E:DebugMsg( "Hidden" )
	end
end

local function OnDragStop()
	frame:StopMovingOrSizing()

	local p, _, parentPoint, x, y = frame:GetPoint()
	E:DebugMsg( format( "%s %s |cffff9f69%d|r |cffff9f69%d|r", p, parentPoint, x, y ) )
	ModUiDb.farmingAnchor.position = { p, "UIParent", parentPoint, x, y }
end

local function CreateAnchor()
	frame = CreateFrame( "FRAME", "FarmingAnchorFrame", UIParent, BackdropTemplateMixin and "BackdropTemplate")
	frame:SetBackdrop({
		bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
		edgeFile = "Interface\\ChatFrame\\ChatFrameBackground",
		tile = true,
		tileSize = 1,
		edgeSize = 1,
	})

	frame:SetBackdropColor( 0.1, 0.1, 0.1, 0.6 )
	frame:SetBackdropBorderColor( 0.1, 0.1, 0.1, 0.8 )
	frame:SetFrameStrata( "HIGH" )
	frame:SetMovable( true )
	frame:EnableMouse( true )
	frame:RegisterForDrag( "LeftButton" )
	frame:SetScript( "OnDragStart", frame.StartMoving )
	frame:SetScript( "OnDragStop", OnDragStop )
	frame:SetClampedToScreen( false )
	frame:SetAlpha( 0.7 )

	Update()
	
	local label = frame:CreateFontString( nil, "OVERLAY" )
	label:SetFont( "FONTS\\FRIZQT__.TTF", 9, "OUTLINE" )
	label:SetPoint( "CENTER", 1, 0 )
	label:SetText( "FA" )

	E:SetSize( frame, 23, 20 )
	local point, parentPoint, x, y = unpack( position )
	E:MoveFrameByPoint( FarmingAnchorFrame, { point, UIParent, parentPoint, x, y } )
end

local function Toggle()
	isVisible = not isVisible
	Update()
end

local function ProcessSlashCommand( command )
	if not command or command == "" or command == " " then
		Toggle()
	elseif command == "reset" then
		local minimapScale = Minimap:GetScale()
		E:MoveFrameByPoint( frame, { "BOTTOMRIGHT", UIParent, "CENTER", Minimap:GetWidth() * minimapScale / 2, -(Minimap:GetHeight() * minimapScale / 2 ) } )
	else
		E:Print( format( "Unknown command: |cffff9f69%s|r", command ) )
	end
end

local function LoadPosition()
	if not ModUiDb.farmingAnchor then
        ModUiDb.farmingAnchor = {}
    end

    if ModUiDb.farmingAnchor.position then
		E:MoveFrameByPoint( frame, ModUiDb.farmingAnchor.position )
	end
end

local function OnFirstEnterWorld()
	SLASH_FA1 = "/fa"
	SlashCmdList[ "FA" ] = ProcessSlashCommand
end

function E.Initialize()
	CreateAnchor()
	E:OnFirstEnterWorld( OnFirstEnterWorld )
	E:OnEnterWorld( LoadPosition )
	E:OnFarmingMode( Update )
end

local function GetFarmingAnchor()
	return frame
end

function E.ExtendComponent( component )
	component.GetFarmingAnchor = GetFarmingAnchor
end
