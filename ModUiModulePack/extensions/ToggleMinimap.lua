local E = ModUi:NewExtension( "ToggleMinimap" )
local OnToggleMinimap, EmitToggleMinimapEvent = E:RegisterCallback( "toggleMinimap" )

local api = ModUi.facade.api

local MAP_MODE_COUNT = 4; -- So we don't have to calculate each time :P

E.MinimapMode = {
  On = { value = 1, displayName = "On" },
  CombatOnly = { value = 2, displayName = "Combat only" },
  NonCombatOnly = { value = 3, displayName = "Non-combat only" },
  Off = { value = 4, displayName = "Off" }
}

local m_mode = E.MinimapMode.On

function E.ShouldBeOnInCombat()
  local mode = m_mode.value

  return
      mode == E.MinimapMode.On.value or
      mode == E.MinimapMode.CombatOnly.value or
      false
end

function E.ShouldBeOnNotInCombat()
  local mode = m_mode.value

  return
      mode == E.MinimapMode.On.value or
      mode == E.MinimapMode.NonCombatOnly.value or
      false
end

local function GetNextMode( mode )
  local index = mode.value == MAP_MODE_COUNT and 1 or mode.value + 1
  local key = E:GetKeyByValue( E.MinimapMode, function( v ) return v.value == index end )
  return E.MinimapMode[ key ]
end

function E.Toggle()
  if api.InCombatLockdown() then
    E:Print( "Cannot toggle map in combat." )
    return
  end

  m_mode = GetNextMode( m_mode )
  ModUiDb.ToggleMinimap.mode = m_mode

  -- Have to print before the event, otherwise chat doesn't print (bug?).
  E:PrettyPrint( m_mode.displayName )
  EmitToggleMinimapEvent( m_mode )
end

local function SetupStorage()
  ModUiDb.ToggleMinimap = ModUiDb.ToggleMinimap or {}

  if not ModUiDb.ToggleMinimap.mode then
    ModUiDb.ToggleMinimap.mode = m_mode
  end

  m_mode = ModUiDb.ToggleMinimap.mode
end

function E.Initialize()
  SetupStorage()
end

function E.ExtendComponent( component )
  component.ToggleMinimap = E
  component.OnToggleMinimap = OnToggleMinimap
end
