local E = ModUi:NewExtension( "Debug", { "FarmingMode" } )
local OnDebugOn, EmitDebugOnEvent = E:RegisterCallback( "debugOn" )
local OnDebugOff, EmitDebugOffEvent = E:RegisterCallback( "debugOff" )
local frame = ChatFrame3

local api = ModUi.facade.api

local function Resize( y )
	E:MoveFrameByPoint( ChatFrame3, { "TOPLEFT", UIParent, "TOPLEFT", 12, y } )
	E:SetHeight( ChatFrame3, 840 + y )
end

local function GroupChanged()
	if not api.IsInGroup() or E:IsFarmingMode() then
		Resize( -221 )
		return
	end

	local groupSize = GetNumGroupMembers()
	if groupSize > 13 then groupSize = 13 end
	local height = 30 * 0.85
	local y = -5 - ( height * groupSize ) - 5

	Resize( y - 142 )
end

local function OnEnterWorld()
	frame:Hide()
	GroupChanged()
end

local function Toggle( toggleFunc )
	E.debug = true
	E.enabled = true

	toggleFunc()
end

local function SayHello()
	E:DebugMsg( "Hello." )
end

local function ToggleDebugForModule( moduleName )
	local m = ModUi:GetModule( moduleName )
	local e = ModUi:GetExtension( moduleName )

	if not m and not e then
		E:PrettyPrint( format( "Module/Extension %s not found.", moduleName ) )
		return
	end

	local component = m or e
	component.debug = not component.debug

	local state = "debug off"
	if component.debug then state = "debug on" end

	component:PrettyPrint( state )
	component:DebugMsg( state, true )

	if E.debug and not frame:IsVisible() then
		Toggle( function()
			frame:Show()
			SayHello()
		end )
	end
end

local function ToggleAndPrint()
	if frame:IsVisible() then
		frame:Hide()
	else
		frame:Show()
		E:Hide( ChatFrame3ResizeButton )
	end

	if frame:IsVisible() then
		E:PrettyPrint( "on" )
		SayHello()
	else
		E:PrettyPrint( "off" )
		E:DebugMsg( "Bye!" )
	end
end

local function ProcessSlashCommand( moduleName )
	if not moduleName or moduleName == "" or moduleName == " " then
		Toggle( ToggleAndPrint )
	else
		ToggleDebugForModule( moduleName )
	end

	if frame:IsVisible() then
		EmitDebugOnEvent()
	else
		EmitDebugOffEvent()
	end
end

function E.Initialize()
	E:OnEnterWorld( OnEnterWorld )
	E:OnGroupChanged( GroupChanged )

	SLASH_DEBUG1 = "/debug"
	SlashCmdList[ "DEBUG" ] = ProcessSlashCommand
end

function E.ExtendComponent( component )
	component.OnDebugOn = OnDebugOn
	component.OnDebugOff = OnDebugOff
end
