local E = ModUi:NewExtension( "OutfitterEvents" )
local OnOutfitChanged, EmitOutfitChangedEvent = E:RegisterCallback( "outfitChanged" )
local equippingOutfits = {}
local addon

local function PurgeTable( tab )
  for key, _ in pairs( tab ) do
    tab[ key ] = nil
  end
end

local function SendEvent( outfits )
  local copyOfOutfits = { unpack( outfits ) }

  E:ScheduleTimer( function() EmitOutfitChangedEvent( copyOfOutfits ) end, 1 )
end

local function DecorateExecuteEquipmentChangeList()
  if addon.PreDecoratedExecuteEquipmentChangeList then return end

  addon.PreDecoratedExecuteEquipmentChangeList = addon.ExecuteEquipmentChangeList
  addon.ExecuteEquipmentChangeList = function( self, pEquipmentChangeList, pEmptyBagSlots, pExpectedEquippableItems )
    addon.PreDecoratedExecuteEquipmentChangeList( self, pEquipmentChangeList, pEmptyBagSlots, pExpectedEquippableItems )
    SendEvent( equippingOutfits )
    PurgeTable( equippingOutfits )
  end
end

local function DecorateWearOutfit()
  if addon.PreDecoratedWearOutfit then return end

  addon.PreDecoratedWearOutfit = addon.WearOutfit
  addon.WearOutfit = function( self, pOutfit, playerID, pCallerIsScript )
    addon.PreDecoratedWearOutfit( self, pOutfit, playerID, pCallerIsScript )
    table.insert( equippingOutfits, pOutfit.Name )
  end
end

local function Decorate()
  --DecorateUpdateEquippedItems()
  DecorateExecuteEquipmentChangeList()
  DecorateWearOutfit()
end

function E.Initialize()
  addon = Outfitter

  if not addon then
    E.enabled = false
    E.DebugMsg( "Disabling: Outfitter not found." )
    return
  end

  Decorate()
end

function E.ExtendComponent( component )
  component.OnOutfitChanged = OnOutfitChanged
end
