local E = ModUi:NewExtension( "WarriorMode" )
local api = ModUi.facade.api
local lua = ModUi.facade.lua

local OnWarriorMode, EmitWarriorModeEvent = E:RegisterCallback( "warriorMode" )

local SINGLE_TARGET = {
  display_name = "Single targetting",
  value = "SingleTarget"
}

local MULTI_TARGET = {
  display_name = "Multi targetting",
  value = "MultiTarget"
}

local AUTO = {
  display_name = "Auto targetting",
  value = "Auto"
}

local modes = { SINGLE_TARGET, MULTI_TARGET, AUTO }

local g_mode
local g_single_targetting
local g_current_area_mob_list
local g_last_zone

local function update_storage()
  ---@diagnostic disable-next-line: undefined-global
  ModUiDb.WarriorMode.mode = g_mode.value
end

local function get_mode_by_value( value )
  for _, v in pairs( modes ) do
    if v.value == value then
      return v
    end
  end

  return nil
end

local function is_mode( mode )
  return g_mode == mode
end

local function GetTankableTarget()
  return not api.UnitIsDead( "target" ) and not api.UnitIsPlayer( "target" ) and api.UnitName( "target" )
end

local function DetermineSingleTargetting( target )
  if g_mode == SINGLE_TARGET then return true end
  if g_mode == MULTI_TARGET then return false end
  if not target then return false end

  local zone = api.GetRealZoneText()
  local mobs = ModUiDb.WarriorMode.mobs[ zone ] or {}
  return mobs[ target ] ~= nil
end

local function set_mode( mode, force )
  if not E.enabled or (is_mode( mode ) and not force) or api.InCombatLockdown() then return false end

  g_mode = mode
  g_single_targetting = DetermineSingleTargetting( GetTankableTarget() )

  update_storage()
  EmitWarriorModeEvent( true )
  E:EmitExternalEvent( "WARRIOR_MODE" )

  return true
end

local function cycle( mode )
  if mode == AUTO and g_single_targetting then
    return MULTI_TARGET
  elseif mode == AUTO and not g_single_targetting then
    return SINGLE_TARGET
  else
    return AUTO
  end
end

local function set_targetting( mode, force )
  if not E.enabled then return end

  if api.InCombatLockdown() then
    E:Print( "Cannot set tanking mode in combat." )
    return false
  end

  set_mode( mode, force )
end

function E.SetSingleTargetting( force )
  set_targetting( SINGLE_TARGET, force )
end

function E.SetMultiTargetting( force )
  set_targetting( MULTI_TARGET, force )
end

function E.SetAutoTargetting( force )
  set_targetting( AUTO, force )
end

function E.Toggle()
  if not E.enabled then return end
  if api.InCombatLockdown() then
    E:PrettyPrint( "Cannot switch mode in combat." )
    return false
  end

  local mode = cycle( g_mode )
  set_mode( mode )
end

local function OnPlayerTargetChanged()
  if g_mode ~= AUTO or api.InCombatLockdown() then return end

  local target = GetTankableTarget()
  if not target then return end

  local single_targetting = DetermineSingleTargetting( target )

  if single_targetting ~= g_single_targetting then
    g_single_targetting = single_targetting
    EmitWarriorModeEvent()
    E:EmitExternalEvent( "WARRIOR_MODE" )
  end
end

local colors = {
  RED = "df2020",
  WHITE = "ffffff",
  LIGHT_GREEN = "33ff99",
  LIGHT_BROWN = "ff9f69"
}

local function color( text, c )
  return lua.format( "|cff%s%s|r", c, text )
end

local function red( text )
  return color( text, colors.RED )
end

local function lightGreen( text )
  return color( text, colors.LIGHT_GREEN )
end

local function lightBrown( text )
  return color( text, colors.LIGHT_BROWN )
end

local function LoadBosses()
  local zone = api.GetRealZoneText()
  if g_last_zone == zone then return end

  g_last_zone = zone
  g_current_area_mob_list = ModUiDb.WarriorMode.mobs[ zone ] or {}

  local count = E:CountElements( g_current_area_mob_list )
  if not g_current_area_mob_list or count == 0 then return end

  local boss = "boss"
  if count > 1 then boss = boss .. "es" end

  E:Print( lua.format( "Found |cffff9f69%d|r %s in |cff33ff99%s|r.", count, boss, zone ) )
end

local function SetupStorage()
  if not ModUiDb.WarriorMode then ModUiDb.WarriorMode = {} end
  if not ModUiDb.WarriorMode.mobs then ModUiDb.WarriorMode.mobs = {} end

  g_mode = get_mode_by_value( ModUiDb.WarriorMode.mode ) or AUTO
  g_single_targetting = g_mode == SINGLE_TARGET
end

function E.MarkMob()
  if not E:IsTargetting() then return end

  local zone = api.GetRealZoneText()
  local target = api.UnitName( "target" )

  local mobs = ModUiDb.WarriorMode.mobs[ zone ] or {}
  mobs[ target ] = not mobs[ target ] and SINGLE_TARGET.value or nil
  ModUiDb.WarriorMode.mobs[ zone ] = mobs

  local mode = mobs[ target ]

  if mode then
    E:Print( lua.format( "%s %s.", get_mode_by_value( mode ).display_name, lightGreen( target ) ) )
  else
    E:Print( lua.format( "Unmarked %s.", lightGreen( target ) ) )
  end

  OnPlayerTargetChanged()
end

function E.Initialize()
  SetupStorage()
  LoadBosses()

  E:OnLeaveCombat( OnPlayerTargetChanged )
  E:OnRegenEnabled( OnPlayerTargetChanged )
  E:OnRegenDisabled( OnPlayerTargetChanged )
  E:OnTargetChanged( OnPlayerTargetChanged )
  E:OnAreaChanged( LoadBosses )
end

local function print_mode()
  E:PrettyPrint( lua.format( "%s.", g_mode.display_name ) )
end

function E.IsSingleTargetting()
  return g_single_targetting
end

function E.ExtendComponent( component )
  component.OnWarriorMode = OnWarriorMode
  component.SetSingleTargetting = E.SetSingleTargetting
  component.SetMultiTargetting = E.SetMultiTargetting
  component.SetAutoTargetting = E.SetAutoTargetting
  component.IsSingleTargetting = E.IsSingleTargetting
  component.PrintWarriorModeName = print_mode
end
