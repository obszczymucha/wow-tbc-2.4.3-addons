local E = ModUi:NewExtension( "TankTarget" )
local OnTankTarget, EmitTankTargetEvent = E:RegisterCallback( "tankTarget" )

local function color( text )
	return format( "|cff33ff99%s|r", text )
end

local function ReportTankTarget( tankTarget )
	E:PrettyPrint( format( "%s", color( tankTarget ) ) )
end

local function ReportTankTargetCleared( reason )
	E:PrettyPrint( format( "Tank target cleared%s.", reason and format( " (%s)", reason ) or "" ) )
end

local function ToggleTankTarget()
	if InCombatLockdown() then
		E:PrettyPrint( "Can't change tank target in combat." )
		return
	end

	local tankTarget = UnitName( "target" )
	ModUiDb.TankTarget = tankTarget

	if tankTarget then
		ReportTankTarget( tankTarget )
	else
		ReportTankTargetCleared()
	end

	EmitTankTargetEvent()
end

function E.Initialize()
	SLASH_TANK1 = "/tank"
	SlashCmdList[ "TANK" ] = ToggleTankTarget
end

local function OnFirstEnterWorld()
	local tankTarget = ModUiDb.TankTarget
	if not tankTarget then return end

	if not E:IsPlayerInMyGroup( tankTarget ) then
		ReportTankTargetCleared( format( "%s not in the group", color( tankTarget ) ) )
		ModUiDb.TankTarget = nil
	else
		ReportTankTarget( tankTarget )
	end

	EmitTankTargetEvent()
end

function E.ExtendComponent( component )
	E:OnFirstEnterWorld( OnFirstEnterWorld )
	component.GetTankTarget = function() return ModUiDb.TankTarget end
	component.OnTankTarget = OnTankTarget
end
