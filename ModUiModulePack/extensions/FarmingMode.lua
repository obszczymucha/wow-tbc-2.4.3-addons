local E = ModUi:NewExtension( "FarmingMode" )
local OnFarmingMode, EmitFarmingModeEvent = E:RegisterCallback( "farmingMode" )
local farmingMode = false

function E.IsFarmingMode()
	return farmingMode
end

local function Update( updateFunc )
	if InCombatLockdown() then
		E:Print( "Cannot change farming mode in combat." )
		return
	end

	updateFunc()

	EmitFarmingModeEvent()
	E:EmitExternalEvent( "FARMING_MODE_CHANGED" )
end

function E.On()
	Update( function() farmingMode = true end )
end

function E.Off()
	Update( function() farmingMode = false end )
end

function E.Toggle()
	Update( function() farmingMode = not farmingMode end )
end

function E.ExtendComponent( component )
	component.OnFarmingMode = OnFarmingMode
	component.IsFarmingMode = E.IsFarmingMode
	component.FarmingModeOn = E.On
	component.FarmingModeOff = E.Off
	component.FarmingModeToggle = E.Toggle
end
