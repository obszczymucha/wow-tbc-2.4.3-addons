local E = ModUi:NewExtension( "ChatLeftSide" )
local OnChatLeftSide, EmitChatLeftSideEvent = E:RegisterCallback( "chatLeftSide" )
local chatLeftSide = false

function E.IsChatLeftSide()
	return chatLeftSide
end

local function DebugState()
	E:DebugMsg( chatLeftSide and "On" or "Off" )
end

local function Update( updateFunc )
	if InCombatLockdown() then
		E:Print( "Cannot move chat to the left in combat." )
		return
	end

	updateFunc()
	DebugState()

	EmitChatLeftSideEvent()
end

function E.On()
	Update( function() chatLeftSide = true end )
end

function E.Off()
	Update( function() chatLeftSide = false end )
end

function E.Toggle()
	Update( function() chatLeftSide = not chatLeftSide end )
end

function E.ExtendComponent( component )
	component.OnChatLeftSide = OnChatLeftSide
	component.IsChatLeftSide = E.IsChatLeftSide
	component.ChatLeftSideOn = E.On
	component.ChatLeftSideOff = E.Off
	component.ChatLeftSideToggle = E.Toggle
end
