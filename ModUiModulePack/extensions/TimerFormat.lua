local E = ModUi:NewExtension( "TimerFormat" )

local function PrettyFormat( _, elapsed )
	if not elapsed then return "0 seconds" end

	local minutes, seconds = tonumber( date( "%M", elapsed) ), tonumber( date( "%S", elapsed ) )
	
	local minuteStr = "minute"
	local secondStr = "second"		

	local result = ""

	if minutes > 0 then
		if minutes > 1 then
			minuteStr = minuteStr .. "s"
		end

		result = result .. format( "|cffff9f69%d|r %s", minutes, minuteStr )
	end

	if seconds > 0 then
		if minutes > 0 then
			result = result .. " "
		end

		if seconds > 1 then
			secondStr = secondStr .. "s"
		end

		result = result .. format( "|cffff9f69%d|r %s", seconds, secondStr )
	end

	return result
end

function E.ExtendComponent( component )
	component.PrettyFormat = PrettyFormat
end
