local E = ModUi:NewExtension( "WarriorShouts", { "Class" } )
local OnShoutChanged, EmitShoutChangedEvent = E:RegisterCallback( "shoutChanged" )

local api = ModUi.facade.api
local g_battle_shouting = false

function E.IsBattleShouting()
  return g_battle_shouting
end

local function set_shouting( battle_shouting )
  if not E.enabled then return end
  if api.InCombatLockdown() then
    E:Print( "Cannot set shouting in combat." )
    return
  end

  g_battle_shouting = battle_shouting
  ModUiDb.WarriorShouts.battleShouting = g_battle_shouting

  EmitShoutChangedEvent()
  E:EmitExternalEvent( "SHOUT_CHANGED" )
end

function E.Toggle()
  set_shouting( not g_battle_shouting )
end

function E.SetBattleShouting()
  set_shouting( true )
end

function E.SetCommandShouting()
  set_shouting( false )
end

local function SetupStorage()
  if not ModUiDb.WarriorShouts then
    ModUiDb.WarriorShouts = {}
  end

  if not ModUiDb.WarriorShouts.battleShouting then
    ModUiDb.WarriorShouts.battleShouting = g_battle_shouting
  end

  g_battle_shouting = ModUiDb.WarriorShouts.battleShouting
end

function E.Initialize()
  if not E:IsWarrior() then
    E.enabled = false
    return
  end

  SetupStorage()
end

local function print_shout()
  if g_battle_shouting then
    E:PrettyPrint( "Battle shouting." )
  else
    E:PrettyPrint( "Command shouting." )
  end
end

function E.ExtendComponent( component )
  component.OnShoutChanged = OnShoutChanged
  component.SetBattleShouting = E.SetBattleShouting
  component.SetCommandShouting = E.SetCommandShouting
  component.IsBattleShouting = E.IsBattleShouting
  component.PrintShoutName = print_shout
end
