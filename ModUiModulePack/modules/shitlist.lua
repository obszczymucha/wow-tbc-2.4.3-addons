local M = ModUi:NewModule( "shitlist" )
local api = ModUi.facade.api
local lua = ModUi.facade.lua
local hl = ModUi.colors.hl
local red = ModUi.colors.red
local warning_frame

--- COMMS
---@diagnostic disable-next-line: undefined-global
local lib_stub = LibStub

local comm_prefix = "ModUi-shitlist"
local HELLO = "HELLO"       -- Initiates sync. Command: HELLO <my nickname> <my data checksum>
local PRINCESS = "PRINCESS" -- Sends the data. Command: PRINCESS <my nickname> <my data>
local KENNY = "KENNY"       -- Sends the data. Command: KENNY <my nickname> <my data>

local function capitalize( nick )
  local nickname = string.lower( nick )
  local result = nickname:gsub( "^%l", string.upper )
  return result
end

local function sort_keys( data )
  local result = {}

  for key, _ in pairs( data ) do
    table.insert( result, key )
  end

  table.sort( result, function( a, b ) return a > b end )

  return result
end

local function calculate_checksum( data )
  local libc = lib_stub:GetLibrary( "LibCompress" )
  local json = ModUi.json.encode( sort_keys( data ) )
  local checksum = libc:fcs16init()
  checksum = libc:fcs16update( checksum, json )
  checksum = libc:fcs16final( checksum )

  return tostring( checksum )
end

local function send_hello( player )
  ModUi:SendCommMessage( comm_prefix, string.format( "%s %s %s", HELLO, M:MyName(), ModUiDb.shitlist.checksum ), "WHISPER", player )
end

local function send_allgood( player )
  ModUi:SendCommMessage( comm_prefix, string.format( "ALLGOOD %s", M:MyName() ), "WHISPER", player )
end

local function send_shindeita( player )
  ModUi:SendCommMessage( comm_prefix, string.format( "SHINDEITA %s", M:MyName() ), "WHISPER", player )
end

local function on_allgood( player )
  M:DebugMsg( string.format( "Syncing shitlists with %s was successful.", player ) )
end

local function on_shindeita( player )
  M:Print( M.systemColor( string.format( "Syncing shitlists with %s was unsuccessful.", player ) ) )
end

local function send_data( player, command )
  local deflate = lib_stub:GetLibrary( "LibDeflate" )

  local json = ModUi.json.encode( ModUiDb.shitlist.players )
  local compressed = deflate:CompressDeflate( json )
  local data = deflate:EncodeForWoWAddonChannel( compressed )

  ModUi:SendCommMessage( comm_prefix, string.format( "%s %s %s", command, M:MyName(), data ), "WHISPER", player )
end

local function on_hello( player, checksum )
  local myChecksum = calculate_checksum( ModUiDb.shitlist.players )

  if myChecksum == checksum then
    send_allgood( player )
    on_allgood( player )
  else
    send_data( player, PRINCESS )
  end
end

local function update_shitlist( their_shitlist )
  local new_entries = 0

  for nick, their_entry in pairs( their_shitlist ) do
    local my_entry = ModUiDb.shitlist.players[ nick ]

    if not my_entry or their_entry.timestamp > my_entry.timestamp then
      ModUiDb.shitlist.players[ nick ] = their_entry
      new_entries = new_entries + 1
    end
  end

  if new_entries > 0 then
    ModUiDb.shitlist.checksum = calculate_checksum( ModUiDb.shitlist.players )
  end

  return new_entries
end

local function on_data( player, data, next_step )
  local deflate = lib_stub:GetLibrary( "LibDeflate" )

  local compressed = deflate:DecodeForWoWAddonChannel( data )
  local json = deflate:DecompressDeflate( compressed )
  local their_shitlist = ModUi.json.decode( json )
  local their_checksum = calculate_checksum( their_shitlist )

  local new_entries = update_shitlist( their_shitlist )

  if new_entries > 0 then
    M:Print( M.systemColor( string.format( "Synced %s new shitlist entr%s with %s.", new_entries, new_entries > 1 and "ies" or "y", player ) ) )
  end

  if ModUiDb.shitlist.checksum == their_checksum then
    send_allgood( player )
    on_allgood( player )
  else
    next_step( player )
  end
end

local function on_comm( _, message )
  for player, checksum in (message):gmatch "HELLO (%w+) (.+)" do
    on_hello( player, checksum )
    return
  end

  for player in (message):gmatch "ALLGOOD (%w+)" do
    on_allgood( player )
    return
  end

  for player in (message):gmatch "SHINDEITA (%w+)" do
    on_shindeita( player )
    return
  end

  for player, data in (message):gmatch "PRINCESS (%w+) (.+)" do
    on_data( player, data, function( player_name ) send_data( player_name, KENNY ) end )
    return
  end

  for player, data in (message):gmatch "KENNY (%w+) (.+)" do
    on_data( player, data, send_shindeita )
    return
  end
end

local function OnChatMsgSystem( message )
  for player in (message):gmatch "|Hplayer:(.+)|h%[.+%]|h has come online." do
    M:ScheduleTimer( function() send_hello( player ) end, 4 )
    return
  end

  for player in (message):gmatch "(.+) has come online." do
    M:ScheduleTimer( function() send_hello( player ) end, 4 )
    return
  end
end

--- COMMS

local function create_warning_frame()
  local frame = api.CreateFrame( "FRAME", "DiedFromCrushWarningFrame", UIParent )
  frame:Hide()

  local label = frame:CreateFontString( nil, "OVERLAY" )
  label:SetFont( "FONTS\\FRIZQT__.TTF", 17, "OUTLINE" )
  label:SetPoint( "TOPLEFT", 0, 0 )
  frame.label = label

  warning_frame = frame
end

local function was_recently_alerted( nick )
  local timestamp = ModUiDb.shitlist.alerts[ nick ]
  if not timestamp then return false end

  local time = lua.time()

  -- Only remind every 1 minute
  if time - timestamp > 60 then
    return false
  else
    return true
  end
end

local function alert( nick )
  local label = warning_frame.label
  local text = string.format( "%s found nearby!", red( capitalize( nick ) ) )
  label:SetText( text )
  M:SetSize( warning_frame, label:GetWidth(), label:GetHeight() )
  warning_frame:SetPoint( "TOPLEFT", UIParent, "TOPLEFT", (UIParent:GetWidth() / 2) - (warning_frame:GetWidth() / 2), -270 )
  M:DisplayAndFadeOut( warning_frame, 2 )
  M:PrettyPrint( text )

  api.PlaySoundFile( "Interface\\AddOns\\Music\\AuctionSold.mp3", "Master" )

  ModUiDb.shitlist.alerts[ nick ] = lua.time()
end

---@diagnostic disable-next-line: unused-local
local function OnEvent( _, event, _, sourceName, _, _, destName, _, _, spellName, _, extraSpellId, extraSpellName )
  local function should_alert( nick )
    if not ModUiDb.shitlist or not ModUiDb.shitlist.players then return false end

    local entry = ModUiDb.shitlist.players[ nick ]
    return entry and entry.status == 1 and not was_recently_alerted( nick )
  end

  if should_alert( sourceName ) then
    alert( sourceName )
  end

  if should_alert( destName ) then
    alert( destName )
  end
end

local function print_usage()
  M:PrettyPrint( string.format( "Usage: /%s <%s> [%s||%s]", hl( "sl" ), hl( "nick" ), hl( "add" ), hl( "remove" ) ) )
end

local function check( nick )
  if ModUiDb.shitlist.players[ nick ] then
    M:PrettyPrint( string.format( "%s is shitlisted.", hl( capitalize( nick ) ) ) )
    return
  end

  M:PrettyPrint( string.format( "%s is not shitlisted.", hl( capitalize( nick ) ) ) )
end

local function add( nick )
  ModUiDb.shitlist.players[ nick ] = { status = 1, timestamp = lua.time() }
  ModUiDb.shitlist.checksum = calculate_checksum( ModUiDb.shitlist.players )
  M:PrettyPrint( string.format( "%s is now shitlisted.", hl( capitalize( nick ) ) ) )
end

local function remove( nick )
  if not ModUiDb.shitlist.players[ nick ] then
    M:PrettyPrint( string.format( "%s is not shitlisted.", hl( capitalize( nick ) ) ) )
    return
  end

  ModUiDb.shitlist.players[ nick ] = { status = 0, timestamp = lua.time() }
  ModUiDb.shitlist.checksum = calculate_checksum( ModUiDb.shitlist.players )

  M:PrettyPrint( string.format( "Removed %s from the shitlist.", hl( capitalize( nick ) ) ) )
end

local function OnCmd( args )
  if args == nil or args == "" then
    print_usage()
    return
  end

  local nickname, command = args:match( "(%w+)%s*(%w*)" )
  local nick = string.lower( nickname )

  if command == "" then
    check( nick )
    return
  end

  if command == "add" then
    add( nick )
    return
  end

  if command == "remove" then
    remove( nick )
    return
  end

  M:PrettyPrint( string.format( "Unknown command: %s", red( command ) ) )
  print_usage()
end

local function OnFirstEnterWorld()
  if not ModUiDb.shitlist then ModUiDb.shitlist = {} end
  if not ModUiDb.shitlist.players then ModUiDb.shitlist.players = {} end
  if not ModUiDb.shitlist.alerts then ModUiDb.shitlist.alerts = {} end
  if not ModUiDb.shitlist.checksum then ModUiDb.shitlist.checksum = calculate_checksum( ModUiDb.shitlist.players ) end

  create_warning_frame()
  ModUi:RegisterComm( comm_prefix, on_comm )
end

function M.Initialize()
  SLASH_SL1 = "/sl"
  api.SlashCmdList[ "SL" ] = OnCmd

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnCombatLogEventUnfiltered( OnEvent )
  M:OnChatMsgSystem( OnChatMsgSystem )
end
