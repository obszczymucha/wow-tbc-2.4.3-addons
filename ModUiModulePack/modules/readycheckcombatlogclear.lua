local M = ModUi:NewModule( "readycheckcombatlogclear" )

local function ClearCombatLog()
	CombatLogClearEntries()
	M:Print( "|cff33ff99Combat Log|r cleared." )
end

function M.Initialize()
	M:OnReadyCheck( ClearCombatLog )
end
