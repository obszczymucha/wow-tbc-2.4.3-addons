local M = ModUi:NewModule( "instancereset" )
local api = ModUi.facade.api

local function OnFirstEnterWorld()
  M:DebugMsg( "ok" )
end

local function hasBeenReset( str )
  return string.match( str, "has been reset%.$" ) ~= nil
end

local function playersInside( str )
  return string.match( str, "There are players still inside the instance%.$" ) ~= nil
end

local function OnChatMsgSystem( message )
  if not api.IsInParty() then return end

  if hasBeenReset( message ) then
    api.SendChatMessage( message, "PARTY" )
    return
  end

  if playersInside( message ) then
    api.SendChatMessage( "Trying to reset the dungeon, but there are players still inside.", "PARTY" )
    return
  end
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnChatMsgSystem( OnChatMsgSystem )
end
