local M = ModUi:NewModule( "damagemeter", { "FarmingMode" } )
local api = ModUi.facade.api

-- TODOs:
-- persist toggle auto-hide

local m_frame
local m_addon
local m_auto_hide = false
---@diagnostic disable-next-line: undefined-global
local m_was_visible
local m_min_rows = 4
local m_last_row_count

local function Position()
  --[[if api.IsInGroup() then]]
  --[[local groupSize = api.GetNumGroupMembers()]]
  --[[if groupSize > 13 then groupSize = 13 end]]
  --[[local height = 30 * 0.85]]

  --[[M:MoveFrameByPoint( m_frame, { "TOPLEFT", UIParent, "TOPLEFT", 21, -9 - (height * groupSize) - 5 } )]]
  --[[M:SetWidth( m_frame, 190 )]]
  --[[else]]
  --[[M:MoveFrameByPoint( m_frame, { "TOPLEFT", UIParent, "TOPLEFT", 21, -12 } )]]
  --[[end]]

  --[[M:SetWidth( m_frame, 190 )]]
end

local function Show()
  if m_frame:IsVisible() then return end
  M:Show( m_frame )
  m_addon:RefreshMainWindow()
  m_was_visible = true
  --m_addon:ResetData()
end

local function Hide()
  if not m_frame:IsVisible() then return end
  M:Hide( m_frame )
end

local function Toggle()
  if api.IsInGroup() or api.IsInRaid() then
    Show()
  else
    if m_auto_hide then Hide() end
  end

  Position()
end

local function set_row_count( number, manual )
  local rows = number < m_min_rows and m_min_rows or number > 10 and not manual and 10 or number
  if not manual and m_last_row_count then rows = m_last_row_count end

  m_frame:SetHeight( 48 + (rows - 1) * 15 )
  m_addon:ResizeMainWindow()

  if manual then m_last_row_count = number end
end

local function OnCmd( args )
  if not args or args == "" then
    Position()

    if m_frame:IsVisible() then
      Hide()
      m_was_visible = false
    else
      Show()
    end

    return
  end

  local number = tonumber( args )

  if number then
    set_row_count( number, true )
    return
  end

  m_auto_hide = not m_auto_hide

  M:PrettyPrint( string.format( "Auto-hide: %s", m_auto_hide and "on" or "off" ) )
end

local function Combat()
  Show()
end

local function NoCombat()
  if m_auto_hide then Hide() end
end

local function hide_recount_buttons()
  ---@diagnostic disable-next-line: undefined-global
  local frame = Recount_MainWindow
  M:SetAlpha( frame.ConfigButton, 0 )
  M:SetAlpha( frame.ReportButton, 0 )
  M:SetAlpha( frame.FileButton, 0 )
  M:SetAlpha( frame.ResetButton, 0 )
  M:SetAlpha( frame.LeftButton, 0 )
  M:SetAlpha( frame.RightButton, 0 )
end

local function show_recount_buttons()
  ---@diagnostic disable-next-line: undefined-global
  local frame = Recount_MainWindow
  M:SetAlpha( frame.ConfigButton, 1 )
  M:SetAlpha( frame.ReportButton, 1 )
  M:SetAlpha( frame.FileButton, 1 )
  M:SetAlpha( frame.ResetButton, 1 )
  M:SetAlpha( frame.LeftButton, 1 )
  M:SetAlpha( frame.RightButton, 1 )
end

local function on_enter( frame, callback )
  frame:SetScript( "OnEnter",
    function( self )
      local mouse_x, mouse_y = api.GetCursorPosition()
      local scale = UIParent:GetScale()

      local left = self:GetLeft() * scale
      local right = self:GetRight() * scale
      local top = self:GetTop() * scale
      local bottom = self:GetBottom() * scale

      local is_over = mouse_x >= left and mouse_x <= right and mouse_y >= bottom and mouse_y <= top

      if is_over then callback() end
    end )
end

local function on_leave( frame, callback )
  local is_over = function( self )
    local mouse_x, mouse_y = api.GetCursorPosition()
    local scale = UIParent:GetScale()

    local left = self:GetLeft() * scale
    local right = self:GetRight() * scale
    local top = self:GetTop() * scale
    local bottom = self:GetBottom() * scale

    return mouse_x >= left and mouse_x <= right and mouse_y >= bottom and mouse_y <= top
  end

  frame:SetScript( "OnLeave", function( self )
    if not is_over( self ) then
      callback()
    else
      frame:SetScript( "OnUpdate", function()
        if not is_over( self ) then
          callback()
          frame:SetScript( "OnUpdate", nil )
        end
      end )
    end
  end )
end

local function create_recount_header_frame( parent )
  local frame = api.CreateFrame( "Frame", "RecountHeaderFrame", parent )
  frame:SetHeight( 18 )
  frame:SetPoint( "TOPLEFT", parent, "TOPLEFT", 0, -8 )
  frame:SetPoint( "TOPRIGHT", parent, "TOPRIGHT", 0, -8 )
  frame:EnableMouse( true )
  frame:SetScript( "OnEnter", function() print( "enter" ) end )
  frame:SetScript( "OnLeave", function() print( "leave " ) end )
  --local texture = frame:CreateTexture( nil, "BACKGROUND" )
  --texture:SetAllPoints( frame )
  --texture:SetTexture( 0.4, 0.8, 1, 0.5 )
  on_enter( frame, show_recount_buttons )
  on_leave( frame, hide_recount_buttons )
end

local function resize_to_group()
  local player_count = #M:GetAllPlayersInMyGroup()
  local group_count = api.ceil( player_count / 5 )
  local healer_count = group_count == 2 and 3 or group_count

  set_row_count( player_count - healer_count )
  if player_count == 1 then Hide() end
end

local function prettify_recount()
  local borderRed, borderBlue, borderGreen, borderAlpha = m_frame:GetBackdropBorderColor()
  local red, green, blue, alpha = m_frame:GetBackdropColor()
  local backdrop = m_frame:GetBackdrop()
  backdrop.edgeSize = 26
  backdrop.insets = { left = 0, right = 0, top = 16, bottom = 0 }
  backdrop.edgeFile = "Interface\\AddOns\\Recount\\textures\\otravi-semi-full-border-square"
  m_frame:SetBackdrop( backdrop )
  m_frame:SetBackdropBorderColor( borderRed, borderBlue, borderGreen, borderAlpha )
  m_frame:SetBackdropColor( red, blue, green, alpha )

  M:SetScale( m_frame.Title, 0.9 )
  M:MoveFrameVertically( m_frame.Title, -11 )
  M:SetScale( m_frame.FileButton, 0.75 )
  M:SetScale( m_frame.ResetButton, 0.75 )
  M:SetScale( m_frame.LeftButton, 0.9 )
  M:SetScale( m_frame.RightButton, 0.9 )

  M:Hide( m_frame.CloseButton )
  M:MoveFrameByPoint( m_frame.RightButton, { "TOPRIGHT", m_frame, "TOPRIGHT", -7, -10 } )

  M:MoveFrameVertically( m_frame.CloseButton, -7 )
  --M:MoveFrameByPoint( m_frame.RightButton, { "RIGHT", m_frame.CloseButton, "LEFT", 0, 0 } )
  create_recount_header_frame( m_frame )
  hide_recount_buttons()

  resize_to_group()
end

local function OnFirstEnterWorld()
  ---@diagnostic disable-next-line: undefined-global
  m_frame = Recount_MainWindow
  ---@diagnostic disable-next-line: undefined-global
  m_addon = Recount

  if not m_frame or not m_addon then
    M:DebugMsg( "Disabling: No Recount found." )
    M.enabled = false
    return
  end

  prettify_recount()

  SLASH_DM1 = "/dm"
  api.SlashCmdList[ "DM" ] = OnCmd
end

local function OnFarmingMode()
  if M:IsFarmingMode() then
    Hide()
  else
    if m_was_visible then
      Show()
      Position()
    else
      Toggle()
    end
  end
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnGroupChanged( resize_to_group )
  M:OnRegenEnabled( NoCombat )
  M:OnRegenDisabled( Combat )
  M:OnEnterCombat( Combat )
  M:OnLeaveCombat( NoCombat )
  M:OnFarmingMode( OnFarmingMode )
end
