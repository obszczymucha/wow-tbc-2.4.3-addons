local M = ModUi:NewModule( "discord", {} )

local function OnWhisper( message, sender )
    if not ModUiDb.discord.enabled or not ModUiDb.discord.url then return end

    -- link please
    -- discord
    -- Discord?
    -- link dis
    -- link plz
    -- got a disc?
    local m = string.lower( message )

    if string.match( m, "disc" ) or string.match( m, "link dis" ) or string.match( m, "link" )then
        SendChatMessage( format( "%s Join and change your nick to your in-game nick please.", ModUiDb.discord.url ), "WHISPER", nil, sender )
    end
end

local highlight = function( word )
    return format( "|cffff9f69%s|r", word )
end

local function PrintStatus()
    local url = ModUiDb.discord.url
    M:PrettyPrint( ModUiDb.discord.enabled and url and format( "On (%s)", highlight( url ) ) or "Off" )
end

local function Toggle( args )
    if not ModUiDb.discord.url and ( not args or args == "" ) then
        M:PrettyPrint( "Discord url not set." )
        return
    end

    for url in ( args ):gmatch "(https://discord.gg/.+)" do
        ModUiDb.discord.url = url
        ModUiDb.discord.enabled = true
        PrintStatus()
        return
    end

    ModUiDb.discord.enabled = not ModUiDb.discord.enabled
    PrintStatus()
end

local function Init()
    if not ModUiDb.discord then ModUiDb.discord = {} end
    if not ModUiDb.discord.enabled then ModUiDb.discord.enabled = false end

    SLASH_DISCORD1 = "/discord"
    SlashCmdList[ "DISCORD" ] = Toggle
end

function M.Initialize()
    M:OnFirstEnterWorld( Init )
	M:OnWhisper( OnWhisper )
end
