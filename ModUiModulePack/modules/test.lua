local M = ModUi:NewModule( "test", {} )

local function MakeNickname( text )
--     text = "Äskeladd"

    local function titleCase( first, rest )
        return first:upper()..rest:lower()
    end
 
    if not text then return nil end

    return titleCase( strsub( text, 1, 1 ), strsub( text, 2 ) )
end

local function Test( args )
    M:Print( "|cffdf2020chuj w Dupe|r" )
end

local function OnFirstEnterWorld()
    SLASH_TEST1 = "/test"
    SlashCmdList[ "TEST" ] = Test
end

function M.Initialize()
	M:OnFirstEnterWorld( OnFirstEnterWorld )
end
