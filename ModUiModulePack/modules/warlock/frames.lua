---@diagnostic disable: undefined-global
local M = ModUi:NewModule( "warlock/frames", { "Class", "ShadowUF", "ChatLeftSide" } )
local api = ModUi.facade.api

local m_topleft_anchor
local m_recount_top_anchor
local m_recount_bottom_anchor
local m_recount_frame
local m_cooldown_tracker_anchor

local Move = ModUi.utils.MoveFrameByPoint

local function create_anchor_frame( name )
  local frame = api.CreateFrame( "Frame", name )
  frame:SetWidth( 1 )
  frame:SetHeight( 1 )
  frame:Show()
  return frame
end

local function create_anchors()
  m_topleft_anchor = create_anchor_frame( "TopLeftAnchor" )
  m_recount_top_anchor = create_anchor_frame( "RecountTopAnchor" )
  m_recount_bottom_anchor = create_anchor_frame( "RecountBottomAnchor" )
  m_cooldown_tracker_anchor = create_anchor_frame( "CooldownTrackerAnchor" )
end

local function reset_topleft_anchor()
  Move( m_topleft_anchor, { "TOPLEFT", UIParent, "TOPLEFT", 17, -18 } )
end

local function on_chat_left_side()
  if M:IsChatLeftSide() then
    Move( m_topleft_anchor, { "TOPLEFT", ChatFrame1, "BOTTOMLEFT", 1, -36 } )
  else
    reset_topleft_anchor()
  end
end

local function position_recount_top_anchor( size, offset )
  local group_size = size or #M:GetAllPlayersInMyGroup()

  if group_size > 5 then
    Move( SUFHeaderraid, { "TOPLEFT", m_topleft_anchor, "TOPLEFT", -3, 4 } )
  else
    Move( SUFHeaderraid, { "TOPLEFT", m_topleft_anchor, "TOPLEFT", -11, 4 } )
  end

  if group_size == 1 then
    Move( m_recount_top_anchor, { "TOPLEFT", m_topleft_anchor, "TOPLEFT", 0, 10 } )
    return
  end

  local multiplier = M:IsInRaid() and group_size or (group_size - 1)
  if multiplier > 5 then multiplier = 5 end

  local _, _, _, _, top = SUFHeaderraid:GetPoint()

  local y = top - (offset or 21.5) * multiplier
  Move( m_recount_top_anchor, { "TOPLEFT", m_topleft_anchor, "TOPLEFT", 0, y } )

  if group_size <= 10 then
    ShadowUF.db.profile.units.raid.width = 110
  else
    ShadowUF.db.profile.units.raid.width = 90
  end

  ShadowUF.Layout:Reload( "raid" )
end

Pos = position_recount_top_anchor

local function position_recount_bottom_anchor()
  position_recount_top_anchor()

  if m_recount_frame:IsVisible() then
    Move( m_recount_bottom_anchor, { "TOPLEFT", Recount_MainWindow, "BOTTOMLEFT", 0, 0 } )
    Move( m_cooldown_tracker_anchor, { "TOPLEFT", m_recount_bottom_anchor, "BOTTOMLEFT", 0, 0 } )
  else
    Move( m_recount_bottom_anchor, { "TOPLEFT", m_recount_top_anchor, "BOTTOMLEFT", 0, 0 } )
    Move( m_cooldown_tracker_anchor, { "TOPLEFT", m_recount_top_anchor, "BOTTOMLEFT", 0, 0 } )
  end
end

local function OnFirstEnterWorld()
  reset_topleft_anchor()
  position_recount_top_anchor()

  m_recount_frame = Recount_MainWindow
  m_recount_frame:Hide()
  Move( m_recount_frame, { "TOPLEFT", RecountTopAnchor, "TOPLEFT", 0, -3.2 } )

  Move( OmenAnchor, { "TOPLEFT", SUFUnitplayer, "BOTTOMLEFT", -1, 26 } )
  Move( DBM_StatusBarTimerAnchor, { "TOPLEFT", m_topleft_anchor, "TOPLEFT", 265, -37 } )

  position_recount_bottom_anchor()

  UIErrorsFrame:SetScale( 0.59413599967957 )
  M:SetWidth( UIErrorsFrame, 300 )
  Move( UIErrorsFrame, { "TOPRIGHT", SUFUnitplayer, "BOTTOMLEFT", -20, 75 } )
  M:Hide( UIErrorsFrame )

  m_recount_frame:HookScript( "OnShow", position_recount_bottom_anchor )
  m_recount_frame:HookScript( "OnHide", position_recount_bottom_anchor )
  m_recount_frame:SetWidth( 175 )
  Recount:ResizeMainWindow()

  Move( EssentialCooldownsFrame, { "TOPLEFT", CooldownTrackerAnchor, "BOTTOMLEFT", 0, -10 } )
end

function M.Initialize()
  if not M:IsWarlock() then
    M.enabled = false
    return
  end

  create_anchors()

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnGroupChanged( position_recount_top_anchor )
  M:OnChatLeftSide( on_chat_left_side )
end
