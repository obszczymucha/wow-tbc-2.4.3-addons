local M = ModUi:NewModule( "encountertimer", { "TankingMode", "TimerFormat" } )

local function OnEnterWorld()
	M:MoveFrameByPoint( M.timer.frame, { "TOPLEFT", UIParent, "TOPLEFT", 590, -127 } )
	M:Hide( M.timer.frame )
end

local function RegenDisabled()
	if M:IsFightingBoss() then
		M.timer:Reset()
		M.timer:Start()
		M:Show( M.timer.frame )
	else
		M:Hide( M.timer.frame )
	end
end

local function RegenEnabled()
	if not M:IsFightingBoss() then return end

	M.timer:Stop()
	M:Hide( M.timer.frame )

	if not M.timer.elapsed then return end

	M:Print( format( "Combat time: %s", M:PrettyFormat( M.timer.elapsed ) ) )
end

function M.Initialize()
	if not Timer then
		M:DebugMsg( "Disabling: No Timer addon found." )
		M.enabled = false
		return
	end

	M.timer = Timer.New( "Encounter" )
	M:OnEnterWorld( OnEnterWorld )
	M:OnRegenDisabled( RegenDisabled )
	M:OnRegenEnabled( RegenEnabled )
end
