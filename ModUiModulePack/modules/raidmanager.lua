local M = ModUi:NewModule( "raidmanager" )

local function Hide()
	local frame = CompactRaidFrameManager
	if not frame then
		M:DebugMsg( "No CompactRaidFrameManager frame found." )
		return
	end

	frame:UnregisterAllEvents()
    frame:HookScript("OnShow", function(s) s:Hide() end)
    frame:Hide()

	M:Hide( frame )
end

function M.Initialize()
	M:OnFirstEnterWorld( Hide )
end
