local M = ModUi:NewModule( "actionbars", { "ShadowUFTarget", "FarmingMode" } )
local actionBarIdToBongosId = { [1] = 20, [2] = 2, [3] = 9, [4] = 8, [5] = 31, [11] = 35, [18] = 42, [19] = 43 }

local addon

local function Enable()
	addon:ShowBars( 1 )
	addon:ShowBars( 2 )
end

local function HookButtons()
    --local frame

    --for i = 6, 30 do
        --frame = getglobal( "Bongos3ActionButton" .. i )
        --if frame then frame:HookScript( "OnClick", function() if not InCombatLockdown() then Bongos3:HideBars( 3 ) end end ) end
    --end
end

local function ShowButton( button )
	if not button then return end

	button:SetAlpha( 1 )

	if not InCombatLockdown() then
		button:EnableMouse( true )
	end
end

local function HideButton( button )
	if not button then return end

	button:SetAlpha( 0 )

	if not InCombatLockdown() then
		button:EnableMouse( false )
	end
end

local function Show()
	if M:IsFarmingMode() and not M:IsInCombat() and M:IsRegenEnabled() then return end
	addon:GetModule( "ActionBar-Config" ).db.profile.showTooltips = true

	for i = 1, 16 do
		ShowButton( _G[ "Bongos3ActionButton" .. i ] )
	end
end

local function Hide()
	addon:GetModule( "ActionBar-Config" ).db.profile.showTooltips = false

	for i = 1, 16 do
		HideButton( _G[ "Bongos3ActionButton" .. i ] )
	end
end

local function OnFarmingMode()
	if M:IsFarmingMode() or not M:IsTargetting() then
		Hide()
	else
		Show()
	end
end

local function OnEnterWorld()
	Enable()

	if M:IsTargetting() then
		Show()
	else
		Hide()
	end

	M:OnTargetAcquired( OnFarmingMode )
	M:OnTargetLost( Hide )
end

local function OnFirstEnterWorld()
	HookButtons()
end

local function Toggle()
	if M:IsTargetting() and not M:IsFarmingMode() then
		Show()
	else
		Hide()
	end
end

local function OnActionBarSlotChanged( slotId )
	local bongosId = actionBarIdToBongosId[ slotId ]
	if not bongosId then return end

	local button = _G[ "Bongos3ActionButton" .. bongosId ]
	if button and not button:IsVisible() then button:Update() end
end

function M.Initialize()
---@diagnostic disable-next-line: undefined-global
  addon = Bongos3
	if not addon then
		M:DebugMsg( "Bongos3 addon not found. Disabling." )
		M.enabled = false
		return
	end

	M:OnFirstEnterWorld( OnFirstEnterWorld )
	M:OnEnterWorld( OnEnterWorld )
	M:OnEnterCombat( Show )
	M:OnRegenDisabled( Show )
	M:OnLeaveCombat( Toggle )
	M:OnRegenEnabled( Toggle )
	M:OnFarmingMode( OnFarmingMode )
	M:OnActionBarSlotChanged( OnActionBarSlotChanged )
end
