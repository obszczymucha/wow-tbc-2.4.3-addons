local M = ModUi:NewModule( "leavevehiclebutton" )

local function Init()
    local button = MainMenuBarVehicleLeaveButton

    M:HideFunction( button, "SetSize" )
    M:HideFunction( button, "SetScale" )

    M:MoveFrameByPoint( button, { "BOTTOMRIGHT", Minimap, "BOTTOMRIGHT", 0, 0 } )
    -- MainMenuBarVehicleLeaveButton
end

function M.Initialize()
    M:OnFirstEnterWorld( Init )
end
