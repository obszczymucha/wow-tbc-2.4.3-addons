local M = ModUi:NewModule( "raidcooldowns" )
local api = ModUi.facade.api
local Move = ModUi.utils.MoveFrameByPoint

local m_frame

local function create_font_frame( parent )
  local frame = parent:CreateFontString( nil, "OVERLAY", "GameFontNormal" )
  frame:SetFont( "Interface\\AddOns\\ModUiModulePack\\fonts\\ABF.ttf", 12 )

  return frame
end

local function create_cooldown_frame( height, opacity )
  local frame = api.CreateFrame( "Frame", nil, m_frame )
  frame:SetPoint( "LEFT", m_frame, "LEFT", 0, 0 )
  frame:SetPoint( "RIGHT", m_frame, "RIGHT", 0, 0 )
  frame:SetHeight( height )
  frame:SetBackdrop( {
    bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
    edgeFile = nil,
    edgeSize = 0,
    insets = { left = 0, right = 0, top = 0, bottom = 0 }
  } )
  frame:SetBackdropColor( 0, 0, 0, opacity or 0.2 )

  return frame
end

local function colour( color_max, color_mid, color_min, mid_pos )
  return function( value, max_value )
    local percentage = value / max_value
    local r, g, b

    if percentage >= mid_pos then
      local ratio = (percentage - mid_pos) / (1 - mid_pos)
      r = color_mid[ 1 ] * (1 - ratio) + color_max[ 1 ] * ratio
      g = color_mid[ 2 ] * (1 - ratio) + color_max[ 2 ] * ratio
      b = color_mid[ 3 ] * (1 - ratio) + color_max[ 3 ] * ratio
    else
      local ratio = percentage / mid_pos
      r = color_min[ 1 ] * (1 - ratio) + color_mid[ 1 ] * ratio
      g = color_min[ 2 ] * (1 - ratio) + color_mid[ 2 ] * ratio
      b = color_min[ 3 ] * (1 - ratio) + color_mid[ 3 ] * ratio
    end

    return r, g, b, 0.9
  end
end

local function create_icon( parent_frame, icon_texture, icon_size )
  local frame = api.CreateFrame( "Frame", nil, parent_frame )
  local size = icon_size or parent_frame:GetHeight() - 2
  local padding = (parent_frame:GetHeight() - size) / 2

  frame:SetWidth( size )
  frame:SetHeight( size )
  frame:SetPoint( "LEFT", parent_frame, "LEFT", padding, 0 )
  frame:SetPoint( "TOP", parent_frame, "TOP", 0, -padding )

  local texture = frame:CreateTexture( nil, "BACKGROUND" )
  texture:SetAllPoints( frame )
  texture:SetTexture( icon_texture )
  local topleft = 2 / size
  local bottomright = (size - 2) / size
  texture:SetTexCoord( topleft, bottomright, topleft, bottomright )
  frame.texture = texture

  return frame
end

local function default_color()
  return 1, 0, 0, 0.7
end

local function set_statusbar_color( frame, value, color_fn )
  local _, max = frame:GetMinMaxValues()
  local r, g, b, a = color_fn( value, max )
  frame:SetStatusBarColor( r, g, b, a )
end

local function format_time( time )
  local minutes = math.floor( time / 60 )
  local seconds = math.floor( time % 60 )
  if minutes >= 1 then
    return string.format( "%d:%02d", minutes, seconds )
  elseif seconds >= 10 then
    return tostring( seconds )
  else
    return tostring( seconds )
  end
end

local function create_statusbar_frame( parent_frame )
  local frame = api.CreateFrame( "StatusBar", nil, parent_frame )
  --frame:Hide() -- This breaks everything... Wonder why.
  frame:SetStatusBarTexture( "Interface\\AddOns\\ModUiModulePack\\textures\\statusbar\\Minimalist" )
  frame:SetFrameStrata( "LOW" )

  return frame
end

local function create_cooldown( player_name, spell_name, current_value, cooldown, icon_name )
  local frame = create_cooldown_frame( 20 )
  frame.refresh_interval = 0.05
  frame.player = player_name
  frame.spell_name = spell_name
  frame.color_fn = colour( { 0.780, 0.537, 0.403 }, { 1, 0.7, 0.3 }, { 0, 1, 0 }, 0.3 )

  local icon = create_icon( frame, icon_name )
  frame.icon_frame = icon

  local statusbar = create_statusbar_frame( frame )
  local padding = 0.5
  statusbar:SetPoint( "TOP", 0, -padding )
  statusbar:SetPoint( "BOTTOM", 0, padding )
  statusbar:SetPoint( "LEFT", icon, "RIGHT", 0, 0 )
  statusbar:SetPoint( "RIGHT", 0, 0 )
  statusbar:SetMinMaxValues( 0, cooldown )
  statusbar:SetValue( current_value )
  frame.statusbar_frame = statusbar

  local player = create_font_frame( frame )
  player:SetPoint( "LEFT", statusbar, "LEFT", 8, 1 )
  player:SetText( player_name )
  player:SetTextColor( 1, 1, 1, 1 )
  frame.player_frame = player

  local status = create_font_frame( frame )
  status:SetPoint( "RIGHT", statusbar, "RIGHT", -7, 1 )
  status:SetTextColor( 1, 1, 1, 1 )
  frame.status_frame = status

  local function on_update( value )
    if frame.color_fn then
      set_statusbar_color( statusbar, value, frame.color_fn )
    end

    if value > 0 then
      status:SetText( format_time( value ) )
      return
    end

    statusbar:SetScript( "OnUpdate", nil )
    status:SetText( "Ready" )
    status:SetTextColor( 0, 0.8, 0.3, 1 )
  end

  local function enable_updates()
    statusbar.timer = statusbar.timer or current_value

    statusbar:SetScript( "OnUpdate", function( self, elapsed )
      self.timer = self.timer + elapsed
      if self.timer < frame.refresh_interval then return end

      self.timer = self.timer - frame.refresh_interval
      local value = self:GetValue()
      self:SetValue( value - frame.refresh_interval )

      on_update( value )
    end )
  end

  frame.start = function( self )
    statusbar.timer = 0

    local color_fn = self.color_fn or default_color
    set_statusbar_color( statusbar, cooldown, color_fn )
    statusbar:SetValue( cooldown )
    status:SetText( format_time( cooldown ) )
    status:SetTextColor( 1, 1, 1, 1 )

    enable_updates()
    self:Show()
  end

  on_update( current_value )
  if current_value > 0 then enable_updates() end

  function frame:ready()
    statusbar.timer = 0
    statusbar:SetValue( 0 )
    on_update( 0 )
  end

  return frame
end

local function create_main_frame( width )
  local frame = api.CreateFrame( "Frame", "CooldownsFrame", UIParent )
  frame:SetWidth( width )
  M.EnableDrag( frame )

  frame.cooldowns = {}

  local function refresh()
    frame:SetHeight( #frame.cooldowns * 21 )
    local previous_cooldown

    for i = 1, #frame.cooldowns do
      local cooldown = frame.cooldowns[ i ]

      if cooldown then
        if not previous_cooldown then
          cooldown:SetPoint( "TOP", frame )
        else
          cooldown:SetPoint( "TOP", previous_cooldown, "BOTTOM", 0, -1 )
        end

        previous_cooldown = cooldown
      end
    end
  end

  function frame:add_cooldown( player_name, spell_name, current_value, cooldown, icon_name )
    table.insert( self.cooldowns, create_cooldown( player_name, spell_name, current_value, cooldown, icon_name ) )
    refresh()
  end

  function frame:remove_player( player_name )
    local player_found = false

    for i = 1, #self.cooldowns do
      local cooldown = self.cooldowns[ i ]

      if cooldown and cooldown.player == player_name then
        cooldown:Hide()
        self.cooldowns[ i ] = nil
        player_found = true
      end
    end

    if player_found then refresh() end
  end

  return frame
end

local function toggle()
  if not m_frame then
    m_frame = create_main_frame( 145 )
    m_frame:add_cooldown( "Obszczymucha", "Innervate", 0, 20, "Interface\\Icons\\Spell_nature_lightning" )
    m_frame:add_cooldown( "Obszczymucha", "Rebirth", 0, 20, "Interface\\Icons\\Spell_nature_reincarnation" )
    Move( m_frame, { "TOPLEFT", UIParent, "TOPLEFT", 19, -65 } )

    return
  end

  M:Toggle( m_frame )
end

local function get_players_in_my_group()
  local result = {}

  if not api.IsInGroup() then
    local my_name = M.MyName()
    table.insert( result, { name = my_name, unit = "player" } )
    return result
  end

  if api.IsInParty() then
    local myName = M.MyName()
    table.insert( result, myName )

    for i = 1, 4 do
      local unit = "party" .. i
      local name = api.UnitName( unit )
      if name then table.insert( result, { name = name, unit = unit } ) end
    end

    return result
  end

  for i = 1, 40 do
    local name = api.GetRaidRosterInfo( i )
    if name then table.insert( result, { name = name, unit = "raid" .. i } ) end
  end

  return result
end

local function group_changed()
  --for _, v in ipairs( get_players_in_my_group() ) do
    --print( string.format( "%s: %s", v.unit, v.name ) )
  --end
end

function M.Initialize()
  --M:OnUpdateBattlefieldStatus( OnUpdateBattlefieldStatus )
  SLASH_RCD1 = "/rcd"
  api.SlashCmdList[ "RCD" ] = toggle
  M:OnGroupChanged( group_changed )
end
