local M = ModUi:NewModule( "itemid" )

local function ProcessItemId( args )
  if not args or args == "" then
    M:PrettyPrint( string.format( "Usage: |cffff9f69%s|r %s", "/itemid", "<item>" ) )
    return
  end

  local result = M:GetItemId( args )
  M:PrettyPrint( string.format( "%s: %s", args, result ) )
end

local function ProcessItemName( args )
  if not args or args == "" then
    M:PrettyPrint( string.format( "Usage: |cffff9f69%s|r %s", "/itemname", "<item>" ) )
    return
  end

  local result = M:GetItemName( args )
  M:PrettyPrint( string.format( "%s: %s", args, result or "N/A" ) )
end

local function OnFirstEnterWorld()
  SLASH_ITEMID1 = "/itemid"
  SlashCmdList[ "ITEMID" ] = ProcessItemId
  SLASH_ITEMNAME1 = "/itemname"
  SlashCmdList[ "ITEMNAME" ] = ProcessItemName
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
end
