local M = ModUi:NewModule( "hidebuffframe" )

local function HideBuffFrame()
    M:Hide( BuffFrame )
end

function M.Initialize()
    M:OnFirstEnterWorld( HideBuffFrame )
end
