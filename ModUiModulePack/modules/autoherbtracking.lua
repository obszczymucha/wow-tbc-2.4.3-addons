local M = ModUi:NewModule( "autoherbtracking" )

local function Toggle()
	local trackingHerbs = ( GetTrackingTexture() == "Interface\\Icons\\INV_Misc_Flower_02" )
	local inBlastedLands = ( GetRealZoneText() == "Blasted Lands" )
	local inZangarmarsh = ( GetRealZoneText() == "Zangarmarsh" )

	if inZangarmarsh and trackingHerbs then
		SetTracking()
		M:Print( "Auto-unset |cffff9f69herb|r tracking." )
	elseif not trackingHerbs then
		SetTracking( 1 )
		M:Print( "Auto-set |cffff9f69herb|r tracking." )
	end
end

function M.Initialize()
	M:OnEnterWorld( Toggle )
	M:OnAreaChanged( Toggle )
end
