local M = ModUi:NewModule( "honortokenhandin" )
if not M then return end

local NPC_NAME = "Horde Warbringer"

-- WoW API facade
local api = {
  GetCurrency = function( id )
    local currency = C_CurrencyInfo.GetCurrencyInfo( id )
    return currency and currency.quantity or 0
  end
}

-- Lua facade
local lua = {
  format = format
}

local function OnFirstEnterWorld()
  M:PrettyPrint( "Hello!" )
end

local function OnGossipShow()
  if UnitName( "target" ) ~= NPC_NAME then
    M:PrettyPrint( "Nope" )
    return
  end

-- GetGossipOptions()
  SelectGossipOption( 1 )
end

local function OnMerchantShow()
  if UnitName( "target" ) ~= NPC_NAME then
    return
  end

  local alteracValleyMarks = api.GetCurrency( 121 )

  if alteracValleyMarks > 0 then
    BuyOption( 1 )
  end
end

function M.Initialize()
	M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnGossipShow( OnGossipShow )
  M:OnMerchantShow( OnMerchantShow )
end

