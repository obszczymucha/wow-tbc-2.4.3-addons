local M = ModUi:NewModule( "honortracker" )

local api = {
  CreateFrame = CreateFrame,
  UIParent = UIParent,
  GetHonor = function() return C_CurrencyInfo.GetCurrencyInfo( 1901 ).quantity end
}

local lua = {
  format = format
}

local frame

local function update_honor()
  local label = frame.label
	label:SetText( lua.format( "Honor: %s", api.GetHonor() ) )
	M:SetSize( frame, label:GetWidth(), label:GetHeight() )
  M:Show( frame )
end

local function create_frame()
	frame = api.CreateFrame( "FRAME", "HonorTrackingFrame", api.UIParent )
	frame:Hide()

	local label = frame:CreateFontString( nil, "OVERLAY" )
  frame.label = label
	label:SetFont( "FONTS\\ARIALN.TTF", 11, "OUTLINE" )
	label:SetPoint( "TOPLEFT", 2.5, 3 )
	frame:SetPoint( "BOTTOMLEFT", "SUFUnitplayer", "TOPLEFT", 0, -2 )
end

function M.Initialize()
  if not M:IsPlayerName( "Psikutas" ) then return end

  M:OnFirstEnterWorld( create_frame )
  M:OnEnterWorld( update_honor )
  M:OnHonorGain( update_honor )
  M:OnBagUpdate( update_honor )
end

