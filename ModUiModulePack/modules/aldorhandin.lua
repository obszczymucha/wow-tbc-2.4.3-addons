local M = ModUi:NewModule( "aldorhandin" )
if not M then return end
local api = ModUi.facade.api

local NPC_NAME = "Ishanah"

local function OnQuestProgress()
  if api.UnitName( "target" ) ~= NPC_NAME then
    return
  end

  api.CompleteQuest()
end

local function OnQuestComplete()
  if api.UnitName( "target" ) ~= NPC_NAME then
    return
  end

  api.GetQuestReward( api.QuestFrameRewardPanel.itemChoice )
end

local function OnGossipShow()
  if api.UnitName( "target" ) ~= NPC_NAME then return end
  api.SelectGossipActiveQuest( 2 )
end

function M.Initialize()
  M:OnGossipShow( OnGossipShow )
  M:OnQuestProgress( OnQuestProgress )
  M:OnQuestComplete( OnQuestComplete )
end
