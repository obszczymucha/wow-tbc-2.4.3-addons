local M = ModUi:NewModule( "buyerfound" )

local function ChatMsgSystem( _, message )
	if UnitName( "player" ) ~= "Germichan" then return end

	if string.match( message, "A buyer has been found" ) then
		SendChatMessage( message, "WHISPER", nil, "Guildhamster" )
	end
end

local function Toggle()
	M.enabled = not M.enabled

	local status = "off"
	if M.enabled then status = "on" end

	M:PrettyPrint( status )
end

function M.Initialize()
	if UnitName( "player" ) ~= "Germichan" then
		M.enabled = false
		return
	end

	M:OnChatMsgSystem( ChatMsgSystem )

	SLASH_BUYERFOUND1 = "/buyerfound"
	SlashCmdList[ "BUYERFOUND" ] = Toggle
end
