local M = ModUi:NewModule( "auctionsold" )

local lastSoldDingTime = nil
local lastOutbidDingTime = nil

local function PlaySoldDing()
	PlaySoundFile( "Interface\\AddOns\\Music\\AuctionSold.mp3", "Master" )
end

local function PlayOutbidDing()
	PlaySoundFile( "Interface\\AddOns\\Music\\AuctionOutbid.mp3", "Master" )
end

local function OnAuctionSold( itemName )
	local time = time()

	if not lastSoldDingTime or ( time - lastSoldDingTime ) > 5 then
		PlaySoldDing()
		lastSoldDingTime = time
	end
end

local function ParseItemThatSold( message )
	for itemName in ( message ):gmatch "A buyer has been found for your auction of (.*)%." do
		return function() OnAuctionSold( itemName ) end
	end

	return nil
end

local function OnAuctionOutbid( itemName )
	local time = time()

	if not lastOutbidDingTime or ( time - lastOutbidDingTime ) > 5 then
		PlayOutbidDing()
		lastOutbidDingTime = time
	end
end

local function ParseItemThatHasBeenOutbid( message )
	for itemName in ( message ):gmatch "You have been outbid on (.+)%." do
		return function() OnAuctionOutbid( itemName ) end
	end

	return nil
end

local function OnChatMsgSystem( message )
	local f = ParseItemThatSold( message ) or ParseItemThatHasBeenOutbid( message )

	if f then f() end
end

function M.Initialize()
	M:OnChatMsgSystem( OnChatMsgSystem )
end
