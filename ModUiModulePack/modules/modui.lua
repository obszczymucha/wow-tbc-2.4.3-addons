local M = ModUi:NewModule( "modui" )
local api = ModUi.facade.api

local m_enabled = false
local m_frame

local function create_frame()
  m_frame = api.CreateFrame( "FRAME", "ModUiAnchor", UIParent )
  local bg = m_frame:CreateTexture( nil, "BACKGROUND" )
  bg:SetAllPoints( true )

  local function show_background()
    bg:SetTexture( 0, 0, 1, 0.4 )
    bg:SetVertexColor( 0, 0, 1, 0.4 )
  end

  local function show_border_line( line )
    line:SetTexture( 0.125, 0.623, 0.976, 0.4 )
  end

  local function hide_element( line )
    line:SetTexture( 0, 0, 0, 0 )
  end

  local function create_border_line( parent )
    local line = parent:CreateTexture( nil, "BORDER" )
    show_border_line( line )
    return line
  end

  local top = create_border_line( m_frame )
  top:SetPoint( "TOPLEFT", m_frame, "TOPLEFT" )
  top:SetPoint( "TOPRIGHT", m_frame, "TOPRIGHT" )
  top:SetHeight( 1 )

  local bottom = create_border_line( m_frame )
  bottom:SetPoint( "BOTTOMLEFT", m_frame, "BOTTOMLEFT" )
  bottom:SetPoint( "BOTTOMRIGHT", m_frame, "BOTTOMRIGHT" )
  bottom:SetHeight( 1 )

  local left = create_border_line( m_frame )
  left:SetPoint( "TOPLEFT", m_frame, "TOPLEFT" )
  left:SetPoint( "BOTTOMLEFT", m_frame, "BOTTOMLEFT" )
  left:SetWidth( 1 )

  local right = create_border_line( m_frame )
  right:SetPoint( "TOPRIGHT", m_frame, "TOPRIGHT" )
  right:SetPoint( "BOTTOMRIGHT", m_frame, "BOTTOMRIGHT" )
  right:SetWidth( 1 )

  m_frame.custom_hide = function()
    hide_element( top )
    hide_element( bottom )
    hide_element( left )
    hide_element( right )
    hide_element( bg )
  end

  m_frame.custom_show = function()
    show_background()
    show_border_line( top )
    show_border_line( bottom )
    show_border_line( left )
    show_border_line( right )
  end

  m_frame:SetFrameStrata( "DIALOG" )
end

local function show_frame_under_cursor()
  local f = api.EnumerateFrames()

  while f do
    if f:IsVisible() and api.MouseIsOver( f ) then
      local name = f:GetName()

      if name ~= nil and name ~= "ModUiAnchor" and name ~= "WorldFrame" and name ~= "UIParent" and name ~= "WeakAurasFrame" then
        if m_frame.current_frame_name == name then return end
        m_frame.current_frame_name = name
        m_frame:SetPoint( "TOPLEFT", f, "TOPLEFT", 0, 0 )
        m_frame:SetWidth( f:GetWidth() )
        m_frame:SetHeight( f:GetHeight() )
        m_frame:SetScale( f:GetScale() )
        m_frame:custom_show()
        print( name )
        return
      end
    end

    f = api.EnumerateFrames( f )
  end

  m_frame:custom_hide()
  m_frame.current_frame_name = nil
end

local function enable_frame()
  if not m_frame then
    create_frame()
    m_frame:SetScript( "OnUpdate", show_frame_under_cursor )
  end

  m_frame:Show()
end

local function disable_frame()
  if not m_frame then return end
  m_frame:Hide()
end

function Toggle()
  m_enabled = not m_enabled
  M:PrettyPrint( m_enabled == true and "enabled" or "disabled" )

  if m_enabled == true then
    enable_frame()
  else
    disable_frame()
  end
end

local function on_create_frame( type, name )
  --print( string.format( "CreateFrame: %s %s", type or "N/A", name or "N/A" ) )
end

local function decorate_create_frame()
  if not api.CreateFrameModUi then
    api.CreateFrameModUi = api.CreateFrame
    api.CreateFrame = function( type, name, ... )
      local result = api.CreateFrameModUi( type, name, ... )
      if name then on_create_frame( type, name ) end
      return result
    end
  end
end

function M.Initialize()
  SLASH_MU1 = "/mu"
  api.SlashCmdList[ "MU" ] = Toggle
  --M:OnFirstEnterWorld( decorate_create_frame )
end
