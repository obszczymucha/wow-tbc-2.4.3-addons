local M = ModUi:NewModule( "dbm", { "FarmingMode" } )

local function Toggle()
	if M:IsFarmingMode() or not M:IsInCombat() then
		M:Hide( DBM_StatusBarTimer1 )
		M:Hide( DBM_StatusBarTimer2 )
		M:Hide( DBM_StatusBarTimer3 )
	else
		M:MoveFrameByPoint( DBM_StatusBarTimer1, { "TOPLEFT", UIParent, "TOPLEFT", 385, -65 } )
		M:Show( DBM_StatusBarTimer1 )
		M:Show( DBM_StatusBarTimer2 )
		M:Show( DBM_StatusBarTimer3 )
	end
end

function M.Initialize()
	M:OnEnterWorld( Toggle )
	M:OnFarmingMode( Toggle )
	M:OnLeaveCombat( Toggle )
	M:OnRegenEnabled( Toggle )
end
