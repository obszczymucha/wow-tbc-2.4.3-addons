local M = ModUi:NewModule( "raidwarning", { "FarmingMode" } )

local function Toggle()
	if M:IsFarmingMode() then
		M:MoveFrameByPoint( RaidWarningFrame, { "TOP", UIParent, "TOP", 0, -326 } )
	else
		M:MoveFrameByPoint( RaidWarningFrame, { "TOP", UIParent, "TOP", 0, -276 } )
	end
end

function M.Initialize()
	M:OnEnterWorld( Toggle )
	M:OnFarmingMode( Toggle )
end
