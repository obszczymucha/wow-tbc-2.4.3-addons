local M = ModUi:NewModule( "xpbarnone", { "FarmingMode" } )

local function Toggle()
	if M:IsFarmingMode() then
		M:Hide( XPBarNoneFrame )
	else
		M:Show( XPBarNoneFrame )
	end
end

function M.Initialize()
	if not XPBarNoneFrame then
		M:DebugMsg( "Disabling. XPBarNone not found." )
		M.enabled = false
		return
	end

	M:OnEnterWorld( Toggle )
	M:OnFarmingMode( Toggle )
end
