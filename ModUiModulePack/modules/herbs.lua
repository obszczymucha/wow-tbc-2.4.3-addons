local M = ModUi:NewModule( "herbs", { "FarmingMode" } )

local function Hide()
	M:Hide( herbCountFrame )
end

local function Toggle()
	if M:IsFarmingMode() then
		M:Show( herbCountFrame )
	else
		Hide()
	end
end

local function OnFirstEnterWorld()
	M:MoveFrameByPoint( herbCountFrame, { "TOPLEFT", FarmingTimerFrame, "TOPRIGHT", 16, 4 } )
end

local function OnEnterWorld()
	Toggle()
end

function M.Initialize()
	if not herbCountFrame then
		M.enabled = false
		M:DebugMsg( "Disabling. No herbCountFrame found." )
		return
	end

	M:OnFirstEnterWorld( OnFirstEnterWorld )
	M:OnEnterWorld( Toggle )
	M:OnFarmingMode( Toggle )
	M:OnEnterCombat( Hide )
	M:OnRegenDisabled( Hide )
	M:OnLeaveCombat( Toggle )
	M:OnRegenEnabled( Toggle )
	M:OnHerbsAvailable( Toggle )
end
