local M = ModUi:NewModule( "autobossmodeinfo", { "TankingMode", "FarmingMode" } )
local bossModeInfoFrame

local function CreateBossModeInfoFrame()
	local frame = CreateFrame( "FRAME", "BpssModeInfoFrame", UIParent )
	frame:Hide()
	
	local label = frame:CreateFontString( nil, "OVERLAY" )
	label:SetFont( "FONTS\\FRIZQT__.TTF", 17, "OUTLINE" )
	label:SetPoint( "TOPLEFT", 0, 0 )
	label:SetText( "Auto-set |cffff4040Boss Mode|r" )
	M:SetSize( frame, label:GetWidth(), label:GetHeight() )
	frame:SetPoint( "TOPLEFT", UIParent, "TOPLEFT", ( UIParent:GetWidth() / 2 ) - ( frame:GetWidth() / 2 ), -240 )
	
	return frame
end

local function OnTankingMode( manuallySet )
	if not manuallySet and M:IsBossMode() and ( UnitAffectingCombat( "player" ) or not M:IsFarmingMode() ) then
		M:DisplayAndFadeOut( bossModeInfoFrame )
	end
end

function M.Initialize()
	bossModeInfoFrame = CreateBossModeInfoFrame()
	M:OnTankingMode( OnTankingMode )
end
