local M = ModUi:NewModule( "warriorshoutmacro", { "WarriorShouts" } )

local BATTLE_SHOUT = "Battle Shout"
local COMMANDING_SHOUT = "Commanding Shout"

local function GetShoutParams()
	if M:IsBattleShouting() then
		return BATTLE_SHOUT, COMMANDING_SHOUT
	else
		return COMMANDING_SHOUT, BATTLE_SHOUT
	end
end

local function OnShoutChanged()
	local index = GetMacroIndexByName( "      Shout" )
	local shoutName, altShoutName = GetShoutParams()

	EditMacro( index, nil, nil, format( "#showtooltip\n/cast [mod:alt] %s; %s", altShoutName, shoutName ), true, true )
end

function M.Initialize()
	M:OnFirstEnterWorld( OnShoutChanged )
	M:OnShoutChanged( OnShoutChanged )
end
