local M = ModUi:NewModule( "playernotes" )
local lua = ModUi.facade.lua
local api = ModUi.facade.api
---@diagnostic disable-next-line: undefined-global
local libStub = LibStub

-- patchwerk fat american 胖胖美国人angered hits on armored men对装甲兵的怒吼intentional pain river keeps others safe故意痛苦的河流使他人安全medics focus those who eat fists医务人员将重点放在那些吃拳头的人身上
-- Senpai Razuvious 吓人的老师 priests discipline the students 他们一直很调皮 hide from scary voice 治愈学生 Melee pray for good luck 倒霉的 When plan fails retreat to the Great Cart 怪坦克
-- heigan dirty man 黑衣脏人 disease spread far 疾病传播得很远 make the fast dance 跳快舞 become friend with small worm 和小虫子成为朋友 do the disco party 参加迪斯科舞会

-- ♥
-- Flow scenarios:
-- HELLO -> ALLGOOD
-- HELLO -> PRINCESS -> ALLGOOD
-- HELLO -> PRINCESS -> KENNY -> ALLGOOD
-- HELLO -> PRINCESS -> KENNY -> SHINDEITA
local HELLO = "HELLO"       -- Initiates sync. Command: HELLO <my nickname> <my data checksum>
--local ALLGOOD = "ALLGOOD"     -- Confirms checksums are the same. Ends the sync. Command: ALLGOOD <my nickname>
local PRINCESS = "PRINCESS" -- Sends the data. Command: PRINCESS <my nickname> <my data>
local KENNY = "KENNY"       -- Sends the data. Command: KENNY <my nickname> <my data>
--local SHINDEITA = "SHINDEITA" -- Checksums don't match. Possibly a bug or an error. Ends the sync. Command: SHINDEITA <my nickname>

local commPrefix = "ModUi-pnotes"
local db
local hl = ModUi.colors.highlight

local function capitalize( str )
  local lowerStr = str:lower()
  local head = lowerStr:sub( 1, 1 ):upper()
  local tail = lowerStr:sub( 2 )

  return head .. tail
end

local function note_recently_displayed( player_name )
  local last_timestamp = db.timestamp_cache[ player_name ]
  if not last_timestamp then return false end

  local time = lua.time()

  -- Only remind once a day
  if time - last_timestamp > 3600 * 1 then -- 1 hour
    return false
  else
    return true
  end
end

local function ParseArgs( args )
  for player, note in (args):gmatch "(%w+)%s?(.*)" do
    return player, note ~= "" and note or nil
  end

  return nil, nil
end

local function SortedKeys( data )
  local result = {}

  for key, _ in pairs( data ) do
    table.insert( result, key )
  end

  table.sort( result, function( a, b ) return a > b end )

  return result
end

local function CalculateChecksum( data )
  local libc = libStub:GetLibrary( "LibCompress" )
  local json = ModUi.json.encode( SortedKeys( data ) )
  local checksum = libc:fcs16init()
  checksum = libc:fcs16update( checksum, json )
  checksum = libc:fcs16final( checksum )

  return tostring( checksum )
end

local function SetNote( player, note )
  local notes = db.notes
  local name = string.lower( player )

  if notes[ name ] then
    M:PrettyPrint( string.format( "Previous note for %s: %s", hl( player ), notes[ name ] ) )
  end

  db.notes[ name ] = note
  db.checksum = CalculateChecksum( db.notes )

  M:PrettyPrint( string.format( "Set note for %s: %s", hl( player ), note ) )
end

local function ShowNote( player )
  local notes = db.notes
  local note = notes[ string.lower( player ) ]
  local cap_name = capitalize( player )

  if not note then
    M:PrettyPrint( string.format( "No notes found for %s.", hl( cap_name ) ) )
  else
    M:PrettyPrint( string.format( "%s %s", hl( cap_name ), note ) )
  end
end

local function ProcessNoteSlashCommand( args )
  local player, note = ParseArgs( args )

  if not player and not note then
    M:PrettyPrint( string.format( "Usage: %s <%s> [%s]", hl( "/note" ), hl( "player" ), hl( "note" ) ) )
    return
  end

  if not note then
    ShowNote( player )
  else
    SetNote( player, note )
  end
end

local function SetupStorage()
  if not ModUi.db.realm.playernotes then ModUi.db.realm.playernotes = {} end
  db = ModUi.db.realm.playernotes
  if not db.notes then db.notes = {} end
  if not db.checksum then db.checksum = 0 end
  if not db.timestamp_cache then db.timestamp_cache = {} end
end

local function SendHello( player )
  ModUi:SendCommMessage( commPrefix, string.format( "%s %s %s", HELLO, M:MyName(), db.checksum ), "WHISPER", player )
end

local function ProcessNoteSyncSlashCommand( player )
  if not player or player == "" then
    M:Print( string.format( "Usage: %s <%s>", hl( "/notesync" ), hl( "player" ) ) )
    return
  end

  SendHello( player )
end

local function SendAllGood( player )
  ModUi:SendCommMessage( commPrefix, string.format( "ALLGOOD %s", M:MyName() ), "WHISPER", player )
end

local function SendShindeita( player )
  ModUi:SendCommMessage( commPrefix, string.format( "SHINDEITA %s", M:MyName() ), "WHISPER", player )
end

local function OnAllGood( player )
  M:DebugMsg( string.format( "Syncing notes with %s was successful.", player ) )
end

local function OnShindeita( player )
  M:Print( M.systemColor( string.format( "Syncing notes with %s was unsuccessful.", player ) ) )
end

local function SendData( player, command )
  local deflate = libStub:GetLibrary( "LibDeflate" )

  local notes = db.notes
  local json = ModUi.json.encode( notes )
  local compressed = deflate:CompressDeflate( json )
  local data = deflate:EncodeForWoWAddonChannel( compressed )

  if not data then
    M:PrettyPrint( string.format( "Not sending data to %s (nil).", hl( player ) ) )
    return
  end

  ModUi:SendCommMessage( commPrefix, string.format( "%s %s %s", command, M:MyName(), data ), "WHISPER", player )
end

local function OnHello( player, checksum )
  local notes = db.notes
  local myChecksum = CalculateChecksum( notes )

  if myChecksum == checksum then
    SendAllGood( player )
    OnAllGood( player )
  else
    SendData( player, PRINCESS )
  end
end

local function UpdateNotes( theirNotes )
  local newNotes = 0

  for player, note in pairs( theirNotes ) do
    if not db.notes[ player ] then
      db.notes[ player ] = note
      newNotes = newNotes + 1
    end
  end

  if newNotes > 0 then
    db.checksum = CalculateChecksum( db.notes )
  end

  return newNotes
end

local function OnData( player, data, nextStep )
  local deflate = libStub:GetLibrary( "LibDeflate" )

  local compressed = deflate:DecodeForWoWAddonChannel( data )
  local json = deflate:DecompressDeflate( compressed )

  if not data then
    M:PrettyPrint( string.format( "Received nil data from %s.", hl( player ) ) )
    return
  end

  local theirNotes = ModUi.json.decode( json )
  local theirChecksum = CalculateChecksum( theirNotes )

  local newNotes = UpdateNotes( theirNotes )

  if newNotes > 0 then
    M:Print( M.systemColor( string.format( "Synced %s new note%s with %s.", newNotes, newNotes > 1 and "s" or "", player ) ) )
  end

  local notes = db.notes
  local myChecksum = CalculateChecksum( notes )

  if myChecksum == theirChecksum then
    SendAllGood( player )
    OnAllGood( player )
  else
    nextStep( player )
  end
end

local function OnComm( _, message )
  for player, checksum in (message):gmatch "HELLO (%w+) (.+)" do
    OnHello( player, checksum )
    return
  end

  for player in (message):gmatch "ALLGOOD (%w+)" do
    OnAllGood( player )
    return
  end

  for player in (message):gmatch "SHINDEITA (%w+)" do
    OnShindeita( player )
    return
  end

  for player, data in (message):gmatch "PRINCESS (%w+) (.+)" do
    OnData( player, data, function( player_name ) SendData( player_name, KENNY ) end )
    return
  end

  for player, data in (message):gmatch "KENNY (%w+) (.+)" do
    OnData( player, data, SendShindeita )
    return
  end
end

local function ProcessNotesSlashCommand()
  local notes = db.notes

  if M:CountElements( notes ) == 0 then
    M:PrettyPrint( "No notes found." )
    return
  end

  for player, note in pairs( db.notes ) do
    M:Print( string.format( "%s: %s", hl( player ), note ) )
  end
end

local function ProcessNoteClearSlashCommand( player_name )
  if not player_name or player_name == "" then
    M:PrettyPrint( string.format( "Usage: %s <player_name>", hl( "/noteclear" ) ) )
    return
  end

  local name = player_name:lower()
  local cap_name = capitalize( name )

  if not db.notes[ name ] then
    M:PrettyPrint( string.format( "No note found for %s.", hl( cap_name ) ) )
    return
  end

  local previous_note = db.notes[ name ]
  db.notes[ name ] = nil
  if db.timestamp_cache[ name ] then db.timestamp_cache[ name ] = nil end
  M:PrettyPrint( string.format( "Note cleared for %s.", hl( cap_name ) ) )
  M:PrettyPrint( string.format( "Previous note for %s: %s", hl( cap_name ), previous_note ) )
end

local function OnFirstEnterWorld()
  SetupStorage()
  ModUi:RegisterComm( commPrefix, OnComm )

  SLASH_NOTE1 = "/note"
  api.SlashCmdList[ "NOTE" ] = ProcessNoteSlashCommand
  SLASH_NOTECLEAR1 = "/noteclear"
  api.SlashCmdList[ "NOTECLEAR" ] = ProcessNoteClearSlashCommand
  SLASH_NOTESYNC1 = "/notesync"
  api.SlashCmdList[ "NOTESYNC" ] = ProcessNoteSyncSlashCommand
  SLASH_NOTES1 = "/notes"
  api.SlashCmdList[ "NOTES" ] = ProcessNotesSlashCommand

  M:DebugMsg( "ok" )
end

local function append_dot( str )
  if str and str ~= "" and string.sub( str, -1 ) ~= "." then
    return str .. "."
  else
    return str
  end
end

local function ShowPlayerNote( player, delay )
  local notes = db.notes
  local name = string.lower( player )
  local note = notes[ name ]

  if note then
    local f = function() M:Print( string.format( "|cffffff08%s %s|r", capitalize( player ), append_dot( note ) ) ) end

    if delay and delay > 0 then
      M:ScheduleTimer( f, delay )
    else
      f()
    end
  end
end

local function OnChatMsgSystem( message )
  for player in (message):gmatch "|Hplayer:(.+)|h%[.+%]|h has come online." do
    M:ScheduleTimer( function() SendHello( player ) end, 4 )
    return
  end

  for player in (message):gmatch "(.+) has come online." do
    M:ScheduleTimer( function() SendHello( player ) end, 4 )
    return
  end

  for player in (message):gmatch "|Hplayer:(.+)|h%[.+%]|h joins the party." do
    ShowPlayerNote( player, 0.1 )
    return
  end

  for player in (message):gmatch "|Hplayer:(.+)|h%[.+%]|h has joined the raid group" do
    ShowPlayerNote( player, 0.1 )
    return
  end

  for player in (message):gmatch "|Hplayer:(.+)|h%[.+%]|h has joined the guild." do
    ShowPlayerNote( player, 0.1 )
    return
  end

  for player in (message):gmatch "(.+) joins the party." do
    ShowPlayerNote( player, 0.1 )
    return
  end

  for player in (message):gmatch "(.+) has joined the raid group" do
    ShowPlayerNote( player, 0.1 )
    return
  end

  for player in (message):gmatch "(.+) has joined the guild." do
    ShowPlayerNote( player, 0.1 )
    return
  end
end

local function OnJoinedParty()
  for i = 1, 4 do
    local name = api.UnitName( "party" .. i )

    if name then
      ShowPlayerNote( name )
    end
  end
end

local function OnJoinedRaid()
  local players = M:GetAllPlayersInMyGroup()

  for _, player in pairs( players ) do
    ShowPlayerNote( player )
  end
end

local function OnJoinedGroup()
  if api.IsInRaid() then
    OnJoinedRaid()
  else
    OnJoinedParty()
  end
end

local function OnWhisper( _, player_name )
  local name = player_name:lower()
  if note_recently_displayed( name ) then return end
  if not db.notes[ name ] then return end

  ShowPlayerNote( name, 0.2 )
  db.timestamp_cache[ name ] = lua.time()
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnChatMsgSystem( OnChatMsgSystem )
  M:OnJoinedGroup( OnJoinedGroup )
  M:OnWhisper( OnWhisper )
end
