local M = ModUi:NewModule( "trashmodewarning", { "TankingMode", "Class" } )
local trashModeWarningFrame

local function CreateTrashModeWarningFrame()
	local frame = CreateFrame( "FRAME", "TrashModeWarningFrame", UIParent )
	frame:Hide()
	
	local label = frame:CreateFontString( nil, "OVERLAY" )
	label:SetFont( "FONTS\\FRIZQT__.TTF", 17, "OUTLINE" )
	label:SetPoint( "TOPLEFT", 0, 0 )
	label:SetText( "|cffff4040Warning!|r Trash Mode is on" )
	M:SetSize( frame, label:GetWidth(), label:GetHeight() )
	frame:SetPoint( "TOPLEFT", UIParent, "TOPLEFT", ( UIParent:GetWidth() / 2 ) - ( frame:GetWidth() / 2 ), -240 )
	
	return frame
end

local function ShowTrashModeWarning()
	M:DisplayAndFadeOut( trashModeWarningFrame )
end

local function OnReadyCheck()
	if M:IsTrashMode() then
		ShowTrashModeWarning()
	end
end

function M.Initialize()
	if not M:IsWarrior() then
		M.enabled = false
	end

	trashModeWarningFrame = CreateTrashModeWarningFrame()
	M:OnReadyCheck( OnReadyCheck )
end
