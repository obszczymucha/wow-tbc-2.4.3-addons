local M = ModUi:NewModule( "bgtimer" )
local api = ModUi.facade.api

local m_counting = false
local m_frame

local function create_background_frame()
  local frame = api.CreateFrame( "Frame", "BgTimerFrame", UIParent )
  frame:SetWidth( 200 )
  frame:SetHeight( 20 )
  frame:SetPoint( "CENTER", 0, 0 )
  frame:SetBackdrop( {
    bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
    edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
    edgeSize = 12,
    insets = { left = 3, right = 3, top = 3, bottom = 3 }
  } )
  frame:SetBackdropColor( 0, 0, 0, 0.4 )

  M.EnableDrag( frame )
  return frame
end

local function create_statusbar_frame( parent_frame )
  local frame = api.CreateFrame( "StatusBar", nil, parent_frame )
  frame:SetPoint( "TOPLEFT", 3, -3 )
  frame:SetPoint( "BOTTOMRIGHT", -3, 3 )
  frame:SetMinMaxValues( 0, 10 )
  frame:SetValue( 10 )
  frame:SetStatusBarTexture( "Interface\\TargetingFrame\\UI-StatusBar" )
  frame:SetStatusBarColor( 1, 0, 0 )

  parent_frame.timer = 0
  local interval = frame:GetValue() / parent_frame:GetWidth()

  parent_frame.EnableUpdates = function( self )
    frame:SetScript( "OnUpdate", function( _, elapsed )
      self.timer = self.timer + elapsed
      if self.timer >= interval then
        self.timer = self.timer - interval
        local value = frame:GetValue()
        frame:SetValue( value - interval )

        if value <= 0 then
          frame:SetScript( "OnUpdate", nil )
          self:Hide()
          return
        end
      end
    end )
  end

  return frame
end

local function toggle()
  if not m_frame then
    local frame = create_background_frame()
    local status_bar = create_statusbar_frame( frame )
    frame.status_bar = status_bar
    m_frame = frame
  end

  m_frame.status_bar:SetValue( 10 )
  m_frame:EnableUpdates()
  m_frame:Show()
end

function M.Initialize()
  --M:OnUpdateBattlefieldStatus( OnUpdateBattlefieldStatus )
  SLASH_TIM1 = "/tim"
  api.SlashCmdList[ "TIM" ] = toggle
end
