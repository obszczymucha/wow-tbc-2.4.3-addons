local M = ModUi:NewModule( "unitframes", { "FarmingMode", "ForceRaid" } )

local function Load()
  if ShadowUF.db.profile.units.player.enabled == false or
      ShadowUF.db.profile.units.target.enabled == false or
      ShadowUF.db.profile.units.targettarget.enabled == false or
      ShadowUF.db.profile.units.party.enabled == false or
      ShadowUF.db.profile.units.raid.enabled == false
  then
    ShadowUF.db.profile.units.player.enabled = true
    ShadowUF.db.profile.units.target.enabled = true
    ShadowUF.db.profile.units.targettarget.enabled = true
    ShadowUF.db.profile.units.party.enabled = true
    ShadowUF.db.profile.units.raid.enabled = true

    ShadowUF:LoadUnits()
  end
end

local function Show()
  M:Show( SUFUnitplayer )

  if M:IsTargetting() then
    M:Show( SUFUnittarget )
  end

  if M:IsTargetOfTarget() then
    M:Show( SUFUnittargettarget )
  end

  M:Show( SUFHeaderraid )
end

local function Hide()
  M:Hide( SUFUnitplayer )
  M:Hide( SUFUnittarget )
  M:Hide( SUFUnittargettarget )
  M:Hide( SUFHeaderraid )
end

local function Disable()
  SUFUnitplayer:SetAlpha( 0 )
  SUFUnittarget:SetAlpha( 0 )
  SUFUnittargettarget:SetAlpha( 0 )
  SUFUnitfocus:SetAlpha( 0 )
  SUFHeaderparty:SetAlpha( 0 )

  if M:IsForceRaid() then
    SUFHeaderraid:SetAlpha( 1 )
  else
    SUFHeaderraid:SetAlpha( 0 )
  end
end

local function Enable()
  SUFUnitplayer:SetAlpha( 1 )
  SUFUnittarget:SetAlpha( 1 )
  SUFUnittargettarget:SetAlpha( 1 )
  SUFHeaderraid:SetAlpha( 1 )
  SUFUnitfocus:SetAlpha( 1 )
  SUFHeaderparty:SetAlpha( 1 )
  if SUFUnitfocustarget then SUFUnitfocustarget:SetAlpha( 1 ) end
end

local function GroupChanged()
  if M:MyName() == "Obszczymucha" then
    local count = #M:GetAllPlayersInMyGroup()
    if count < 6 then
      M:MoveFrameByPoint( SUFHeaderraid, { "TOPRIGHT", SUFUnitplayer, "BOTTOMRIGHT", 0, -10 } )
    else
      M:MoveFrameByPoint( SUFHeaderraid, { "TOPRIGHT", SUFUnitplayer, "BOTTOMRIGHT", 0, -10 } )
    end
  else
    M:MoveFrameByPoint( SUFHeaderraid, { "TOPLEFT", UIParent, "TOPLEFT", 10, -16 } )
  end
end

local function Toggle()
  if not ShadowUF then return end

  if M:IsFarmingMode() then
    Load()
    Disable()
  else
    Load()
    Enable()
  end

 --GroupChanged()
end

local function OnFirstEnterWorld()
  M:HideFunction( SUFHeaderraid, "ClearAllPoints" )
  M:HideFunction( SUFHeaderraid, "SetPoint" )
end

local function OnEnterWorld()
  Toggle()
  --GroupChanged()
end

function M.Initialize()
  if not ShadowUF then
    M:DebugMsg( "ShadowUF not available. Disabling." )
    M.enabled = false
    return
  end

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( OnEnterWorld )
  M:OnFarmingMode( Toggle )
  M:OnEnterCombat( Enable )
  M:OnRegenDisabled( Enable )
  M:OnLeaveCombat( Toggle )
  M:OnRegenEnabled( Toggle )
  --M:OnGroupChanged( GroupChanged )
  M:OnForceRaid( Toggle )
end
