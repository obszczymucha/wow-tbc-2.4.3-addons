local M = ModUi:NewModule( "moteextractor", { "Class" } )
local macro = ModUi.facade.macro

local function OnFirstEnterWorld()
  local is_druid = M:IsDruid()
  local mountName = is_druid and "Swift Flight Form" or "Swift Red Windrider"
  local zme = "Zapthrottle Mote Extractor"

  local builder = macro:tooltip( zme )

  if is_druid then builder:cancelform( "nomod,noform:0" ) else builder:dismount( "nomod,mounted" ) end

  builder
      :use( zme, "mod:ctrl" )
      :use( mountName, "mod:shift" )
      :castsequence( string.format( "%s,%s", zme, mountName ), "6", "nomod" )
      :build( "Zap" )
end

function M.Initialize()
  if not M:HasProfession( "engineering" ) then
    M.enabled = false
    return
  end

  M:OnFirstEnterWorld( OnFirstEnterWorld )
end
