local M = ModUi:NewModule( "tankingmode", { "TankingMode", "WarriorShouts" } )

local function GetTankingModeParams()
	if M:IsBossMode() then
		return "Boss Mode", "ff2020"
	else
		return "Trash Mode", "ff9f69"
	end
end

local function GetShoutParams()
	if M:IsBattleShouting() then
		return "Battle Shout", "ff9f69"
	else
		return "Commanding Shout", "5060b0"
	end
end

function M.DisplayTankingMode( _, prefix )
	local modeName, tankingModeColor = GetTankingModeParams()
	local shoutName, shoutColor = GetShoutParams()

	M:Print( format( "%s|cff%s%s|r with |cff%s%s|r.", prefix or "", tankingModeColor, modeName, shoutColor, shoutName ) )
end

local function OnTankingMode( manuallySet )
	if manuallySet then
		M:DisplayTankingMode()
	else
		M:DisplayTankingMode( "Auto-set: " )
	end
	
end

function M.Initialize()
	M:OnTankingMode( OnTankingMode )
	M:OnShoutChanged( M.DisplayTankingMode )
	M:OnReadyCheck( M.DisplayTankingMode )
end
