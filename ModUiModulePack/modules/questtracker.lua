local M = ModUi:NewModule( "questtracker", { "FarmingMode" } )

local function OnFarmingMode()
	if not Questie_BaseFrame then return end

	if M:IsFarmingMode() then
		M:Hide( Questie_BaseFrame )
	else
		M:Show( Questie_BaseFrame )
	end
end

function M.Initialize()
	M:OnFarmingMode( OnFarmingMode )
end
