local M = ModUi:NewModule( "warrior", { "Class", "WarriorMode", "WarriorShouts" } )
local api = ModUi.facade.api
local macro = ModUi.facade.macro

local ALT = macro.modifiers.ALT
local SHIFT = macro.modifiers.SHIFT
local TARGET_PLAYER = macro.modifiers.TARGET_PLAYER
local TARGET_FOCUS = macro.modifiers.TARGET_FOCUS
local TARGET_MOUSEOVER = macro.modifiers.TARGET_MOUSEOVER
local TARGET_TARGETTARGET = macro.modifiers.TARGET_TARGETTARGET
local FLYABLE = macro.modifiers.FLYABLE
local COMBAT = macro.modifiers.COMBAT
local NOCOMBAT = macro.modifiers.NOCOMBAT
local EQUIPPED_THROWN = macro.modifiers.EQUIPPED_THROWN
local MAIN_WEAPON = "King's Defender"

local WarriorSpec = {
  Fury = "Fury",
  Arms = "Arms",
  Prot = "Prot"
}

local function parse_stance_numbers( ... )
  if select( "#", ... ) == 0 then error( "No stance numbers provided." ) end
  return table.concat( { ... }, "/" )
end

local function stance( ... )
  return string.format( "stance:%s", parse_stance_numbers( ... ) )
end

local function nostance( ... )
  if select( "#", ... ) == 0 then return "nostance" end
  return string.format( "nostance:%s", parse_stance_numbers( ... ) )
end

local BATTLE = "1"
local DEFENSIVE = "2"
local BERSERKER = "3"

local m_should_update_all_macros = false
local arms = false
local fury = false
local prot = false

local function show_combat_info()
  M:PrettyPrint( "Macros will update after combat." )
end

local function set_helper_variables( spec )
  if spec == WarriorSpec.Prot then
    arms = false; fury = false; prot = true;
  elseif spec == WarriorSpec.Fury then
    arms = false; fury = true; prot = false;
  elseif spec == WarriorSpec.Arms then
    arms = true; fury = false; prot = false;
  else
    error( string.format( "Unsupported WarriorSpec: %s", spec ) )
    return
  end
end

local function determine_spec( callback )
  local name, _, _, _, points = api.GetTalentInfo( 3, 22 )
  if name == "Devastate" and points == 1 then
    return WarriorSpec.Prot
  end

  name, _, _, _, points = api.GetTalentInfo( 2, 21 )
  if name == "Rampage" and points == 1 then
    return WarriorSpec.Fury
  end

  name, _, _, _, points = api.GetTalentInfo( 1, 23 )
  if name == "Endless Rage" and points == 1 then
    return WarriorSpec.Arms
  end

  -- At this point it means we couldn't determine the spec.
  -- It happens when we log in sometimes.
  -- Let's try again in 0.5 second.
  M:ScheduleTimer( function()
    local spec = determine_spec( function()
      error( "Couldn't determine warrior's spec!" )
    end )

    if spec ~= nil then callback() end
  end, 0.5 )

  return nil
end

local function UpdateButtonF1()
  if prot then
    macro:tooltip()
        :cast( "Battle Stance", ALT, nostance( BATTLE ) )
        :cast( "Mocking Blow", stance( BATTLE ) )
        :cast( "Hamstring", ALT, stance( BERSERKER ) )
        :cast( "Pummel", stance( BERSERKER ) )
        :cast( "Thunder Clap" )
        :use( "14", "nomod" )
        :build( "F1" )
  elseif fury then
    macro:tooltip()
        :startattack()
        :cast( "Defensive Stance", ALT, nostance( BATTLE ) )
        :cast( "Thunder Clap", stance( BATTLE ) )
        :cast( "Hamstring", ALT, stance( BERSERKER ) )
        :cast( "Pummel", stance( BERSERKER ) )
        :cast( "Thunder Clap" )
        :use( "14", "nomod" )
        :build( "F1" )
  elseif arms then
    macro:tooltip()
        :startattack()
        :cast( "Battle Stance", ALT, nostance( BATTLE ) )
        :cast( "Thunder Clap", stance( BATTLE ) )
        :cast( "Hamstring", ALT, stance( BERSERKER ) )
        :cast( "Pummel", stance( BERSERKER ) )
        :cast( "Thunder Clap" )
        :use( "14", "nomod" )
        :build( "F1" )
  end
end

local function UpdateButtonF2()
  macro:tooltip( "[mod:alt]Berserker Rage;[mod:shift]Hamstring;Demoralizing Shout" )
      :cast( "Berserker Stance", ALT, nostance( BERSERKER ) )
      :cast( "Berserker Stance", SHIFT, stance( DEFENSIVE ) )
      :cast( "Berserker Rage", ALT, stance( BERSERKER ) )
      :cast( "Hamstring", SHIFT )
      :cast( "Demoralizing Shout" )
      :build( "F2" )
end

local function UpdateButtonF3()
  macro:tooltip():startattack():cast( "War Stomp" ):build( "F3" )
end

local function UpdateButtonF4()
  macro:tooltip( "Shield Wall" )
      :cast( "Defensive Stance", nostance( DEFENSIVE ) )
      :cast( "Shield Wall" )
      :build( "F4" )
end

local function UpdateButtonF5()
  if prot then
    macro:tooltip()
        :cast( "Last Stand" )
        :build( "F5" )
  else
    macro:tooltip()
        :cast( "Recklessness" )
        :build( "F5" )
  end
end

-- name, id1, id2, ...
local count = function( name, ... )
  local total = 0
  for _, id in ipairs( { ... } ) do total = total + M:CountItemsById( id ) end

  return total > 0 and name or nil
end

local function UpdateButton1()
  local single_targetting = M:IsSingleTargetting()
  local primary_ability = single_targetting and "Heroic Strike" or "Cleave"
  local secondary_ability = single_targetting and "Cleave" or "Heroic Strike"
  local consume =
      count( "Master Healthstone", 22103, 22104, 22105 ) or
      count( "Auchenai Healing Potion", 32947 ) or
      count( "Super Healing Potion", 22829 )

  if arms then
    macro:tooltip()
        :startattack()
        :use( "13", "mod:ctrl" )
        :use( consume, SHIFT )
        :cast( "Heroic Strike", ALT )
        :cast( "Whirlwind" )
        :global()
        :build( "1" )
  else
    macro:tooltip()
        :startattack()
        :equip( "The Decapitator", "mod:ctrl,noequipped:The Decapitator" )
        :equip( "The Decapitator", "nocombat,nomod,noequipped:The Decapitator" )
        :use( "16", "worn:Axe" )
        :use( consume, SHIFT )
        :cast( secondary_ability, ALT )
        :cast( primary_ability )
        :global()
        :build( "1" )
  end
end

local function UpdateButton2()
  local consume = count( "Mighty Rage Potion", 13442 )

  if prot then
    macro:tooltip()
        :startattack()
        :use( "14", "mod:ctrl" )
        :use( consume, SHIFT )
        :cast( "Defensive Stance", ALT, nostance( DEFENSIVE ) )
        :cast( "Disarm", ALT )
        :cast( "Devastate" )
        :global()
        :build( "2" )
  elseif fury then
    macro:tooltip()
        :startattack()
        :use( "14", "mod:ctrl" )
        :use( consume, SHIFT )
        :cast( "Battle Stance", ALT, nostance( BATTLE ) )
        :cast( "Defensive Stance", ALT, stance( BATTLE ) )
        :cast( "Disarm", ALT )
        :cast( "Sunder Armor" )
        :global()
        :build( "2" )
  elseif arms then
    macro:tooltip()
        :startattack()
        :use( "14", "mod:ctrl" )
        :use( consume, SHIFT )
        :cast( "Defensive Stance", ALT, stance( BATTLE, BERSERKER ) )
        :cast( "Disarm", ALT )
        :cast( "Disarm", stance( BATTLE, BERSERKER ) )
        :cast( "Sunder Armor" )
        :global()
        :build( "2" )
  end
end

local function UpdateButton3()
  local primary_ability = prot and "Shield Block" or fury and "Bloodthirst" or arms and "Mortal Strike"
  local secondary_ability = prot and "Concussion Blow" or "Hamstring"
  local berserker_stance_ability = prot and "Intercept" or fury and "Bloodthirst" or arms and "Mortal Strike"
  local consume = count( "Free Action Potion", 5634 )
  local rum = count( "Rumsey Rum Black Label", 21151 )

  if arms then
    macro:tooltip()
        :startattack()
        :use( consume, SHIFT )
        :cast( "Hamstring", stance( BATTLE ), ALT )
        :cast( "Whirlwind", stance( BERSERKER ), ALT )
        :cast( berserker_stance_ability )
        :cast( secondary_ability, ALT )
        :cast( primary_ability )
        :global()
        :build( "3" )
  else
    macro:tooltip()
        :startattack( ALT )
        :use( consume, SHIFT )
        :use( rum, "mod:ctrl" )
        :cast( "Hamstring", stance( BATTLE ), ALT )
        :cast( "Whirlwind", stance( BERSERKER ), ALT )
        :cast( secondary_ability, ALT )
        :cast( primary_ability )
        :global()
        :build( "3" )
  end
end

local function UpdateButton4()
  local consume = count( "Ironshield Potion", 22849 )

  if arms then
    macro:tooltip()
        :startattack()
        :cast( "Death Wish", ALT )
        :cast( "Hamstring" )
        :global()
        :build( "4" )
  else
    macro:tooltip()
        :startattack()
        :use( consume, SHIFT )
        :cast( "Hamstring", ALT, stance( BERSERKER ) )
        :cast( prot and "Shield Bash" or "Rend", ALT )
        :cast( "Whirlwind", stance( BERSERKER ), "noworn:Shield" )
        :cast( fury and "Sweeping Strikes" or nil )
        :cast( "Shield Slam" )
        :equip( MAIN_WEAPON, string.format( "nomod,noequipped:%s", MAIN_WEAPON ) )
        :global()
        :build( "4" )
  end
end

local function UpdateButton5()
  local consume = count( "Nightmare Seed", 22797 )

  if arms then
    macro:tooltip()
        :startattack()
        :cast( "Overpower", stance( BATTLE ) )
        :cast( "Spell Reflection", stance( DEFENSIVE ), ALT )
        :cast( "Execute", ALT, stance( BERSERKER ) )
        :cast( "Piercing Howl", stance( BERSERKER ) )
        :cast( "Revenge" )
        :global()
        :build( "5" )
  else
    macro:tooltip()
        :startattack()
        :use( consume, SHIFT )
        :cast( "Overpower", stance( BATTLE ) )
        :cast( "Spell Reflection", stance( DEFENSIVE ), ALT )
        :cast( "Execute", stance( BERSERKER ) )
        :cast( "Revenge" )
        :global()
        :build( "5" )
  end
end

local function UpdateButton6()
  if prot then
    macro:tooltip()
        :startattack()
        :cast( "Pummel", stance( BERSERKER ) )
        :cast( "Shield Bash" )
        :global()
        :build( "6" )
  elseif arms then
    macro:tooltip()
        :startattack()
        :cast( "Pummel", stance( BERSERKER ) )
        :cast( "Shield Bash" )
        :global()
        :build( "6" )
  else
    macro:tooltip()
        :startattack()
        :cast( "Rampage", stance( BERSERKER ), ALT )
        :cast( "Pummel", stance( BERSERKER ) )
        :cast( "Rampage" )
        :global()
        :build( "6" )
  end
end

local function UpdateButtonF()
  if prot then
    macro:tooltip( "[mod:shift]Intercept;[nocombat]Charge;Intercept" )
        :startattack()
        :cast( "Defensive Stance", COMBAT, stance( BATTLE ) )
        :cast( "Berserker Stance", COMBAT, stance( DEFENSIVE ) )
        :cast( "Intercept", "combat" )
        :cast( "Battle Stance", nostance( BATTLE ) )
        :cast( "Charge" )
        :build( "F" )
  else
    macro:tooltip( "[mod:shift]Intercept;[nocombat]Charge;Intercept" )
    --:dismount( "mounted,harm,nodead" )
        :startattack()
        :cast( "Berserker Stance", COMBAT, nostance( BERSERKER ) )
        :cast( "Berserker Stance", SHIFT, nostance( BERSERKER ) )
        :cast( "Intercept", SHIFT )
        :cast( "Intercept", COMBAT, stance( BERSERKER ) )
        :cast( "Battle Stance", NOCOMBAT, nostance( BATTLE ) )
        :cast( "Charge" )
        :build( "F" )
  end
end

local BATTLE_SHOUT = "Battle Shout"
local COMMANDING_SHOUT = "Commanding Shout"

local function get_shout_params()
  if M:IsBattleShouting() then
    return BATTLE_SHOUT, COMMANDING_SHOUT
  else
    return COMMANDING_SHOUT, BATTLE_SHOUT
  end
end

local function UpdateButtonG()
  local primary, secondary = get_shout_params()

  macro:tooltip()
      :cast( "Bloodrage", ALT )
      :cast( primary, SHIFT )
      :cast( secondary, "mod:ctrl" )
      :castsequence( string.format( "Bloodrage,%s", primary ), nil, "nomod" )
      :build( "G" )
end

local function UpdateButtonR()
  if prot or arms then
    macro:tooltip()
        :cast( "Defensive Stance", nostance( DEFENSIVE ) )
        :cast( "Intervene", ALT, TARGET_FOCUS )
        :cast( "Intervene", SHIFT, TARGET_MOUSEOVER )
        :cast( "Intervene", TARGET_TARGETTARGET )
        :build( "R" )
  else
    macro:tooltip( "Pummel" )
        :cast( "Berserker Stance", nostance( BERSERKER ) )
        :cast( "Pummel" )
        :build( "R" )
  end
end

local function UpdateButtonZ()
  local consume =
      count( "Conjured Manna Biscuit", 34062 ) or
      count( "Bladespire Bagel", 29449 )

  macro:tooltip()
      :use( consume, "mod:ctrl" )
      :use( "Heavy Netherweave Bandage", ALT, TARGET_PLAYER )
      :use( "Heavy Netherweave Bandage", SHIFT )
      :use( "Fel Iron Bomb" )
      :build( "Z" )
end

local function UpdateTaunt()
  macro:tooltip()
      :cast( "Throw", ALT, EQUIPPED_THROWN )
      :cast( "Shoot", ALT )
      :cast( "Taunt", stance( DEFENSIVE ) )
      :cast( "Defensive Stance" )
      :build( "Taunt" )
end

local function UpdateMount()
  local name = "Mount"

  macro:tooltip()
      :dismount( "mounted" )
      :use( "Great Brown Kodo", SHIFT, FLYABLE )
      :use( "Swift Red Windrider", FLYABLE )
      :use( "Great Brown Kodo" )
      :build( name )
end

local function show_spec( spec )
  if spec then
    M:PrettyPrint( string.format( "%s macros updated.", spec ) )
  else
    M:PrettyPrint( "Could not determine the spec." )
  end
end

local function UpdateAllMacros()
  if api.InCombatLockdown() then
    show_combat_info()
    m_should_update_all_macros = true
    return
  end

  local spec = determine_spec( UpdateAllMacros )
  if not spec then return end

  set_helper_variables( spec )

  UpdateButtonF1()
  UpdateButtonF2()
  UpdateButtonF3()
  UpdateButtonF4()
  UpdateButtonF5()
  UpdateButton1()
  UpdateButton2()
  UpdateButton3()
  UpdateButton4()
  UpdateButton5()
  UpdateButton6()
  UpdateButtonF()
  UpdateButtonG()
  UpdateButtonR()
  UpdateButtonZ()
  UpdateTaunt()
  UpdateMount()

  show_spec( spec )
  m_should_update_all_macros = false
end

local function OnWarriorMode( manual )
  if api.InCombatLockdown() then
    show_combat_info()
    m_should_update_all_macros = true
    return
  end

  UpdateButton1()

  if manual then M:PrintWarriorModeName() end
end

local function OnSpecChanged()
  M:ScheduleTimer( function()
    UpdateAllMacros()

    if prot then
      M:SetAutoTargetting( true )
      M:SetCommandShouting()
    else
      M:SetSingleTargetting( true )
      M:SetBattleShouting()
    end
  end, 0.2 )
end

local function OnShoutChanged()
  UpdateButtonG()
  M:PrintShoutName()
end

local function UpdateConsumableMacros()
  if api.InCombatLockdown() then return end

  UpdateButton1()
  UpdateButton2()
  UpdateButton3()
  UpdateButton4()
  UpdateButton5()
  UpdateButtonZ()
end

function OnRegenEnabled()
  if m_should_update_all_macros then
    UpdateAllMacros()
  else
    UpdateConsumableMacros()
  end
end

function M.Initialize()
  if M:MyName() ~= "Ohhaimark" then
    M.enabled = false
    return
  end

  SLASH_MAC1 = "/mac"
  api.SlashCmdList[ "MAC" ] = UpdateAllMacros

  M:OnFirstEnterWorld( UpdateAllMacros )
  M:OnRegenEnabled( OnRegenEnabled )
  M:OnSpecChanged( OnSpecChanged )
  M:OnBagUpdate( function() M:ScheduleTimer( UpdateConsumableMacros, 1 ) end )
  M:OnWarriorMode( OnWarriorMode )
  M:OnShoutChanged( OnShoutChanged )
end
