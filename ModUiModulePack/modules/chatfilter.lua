local M = ModUi:NewModule( "chatfilter" )

local function my_filter( event, message, sender, something, els )
  M:PrettyPrint( string.format( "%s '%s' %s %s %s", event or "nil event", message or "nil message", sender or "nil sender", something or "nil something",
    els or "nil els" ) )
  return true
end

function M.Initialize()
  --DEFAULT_CHAT_FRAME:SetScript("OnEvent", my_filter)
  --ChatFrame_AddMessageEventFilter( "CHAT_MSG_CHANNEL", my_filter )
end
