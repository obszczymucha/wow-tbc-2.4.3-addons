local M = ModUi:NewModule( "farmingtimer", { "FarmingMode", "TimerFormat" } )

local function Show()
	M:Show( M.timer.frame )
end

local function Hide()
	M:Hide( M.timer.frame )
end

local function Toggle()
	if M:IsFarmingMode() then
		Show()
		M:MoveFrameByPoint( M.timer.frame, { "TOPLEFT", BattlefieldMinimap, "TOPLEFT", 0, 22 } )
	else
		Hide()
	end
end

local function StartTimer()
	if not M.timer:IsRunning() then
		M.timer:Reset()
		M.timer:Start()
	end
end

local function StopTimer()
	if M.timer:IsRunning() then
		M.timer:Stop()
		local elapsed = M.timer.elapsed
		M.timer:Reset()

		if elapsed then
			M:Print( format( "Farming time: %s", M:PrettyFormat( elapsed ) ) )
		end
	end
end

local function OnEnterWorld()
	Toggle()
end

function M.Initialize()
	if not Timer then
		M:DebugMsg( "Disabling: No Timer addon found." )
		M.enabled = false
		return
	end

	M.timer = Timer.New( "Farming" )
	M:OnEnterWorld( OnEnterWorld )
	M:OnFarmingMode( Toggle )
	M:OnEnterCombat( Hide )
	M:OnRegenDisabled( Hide )
	M:OnLeaveCombat( Toggle )
	M:OnRegenEnabled( Toggle )
	M:OnHerbGathered( StartTimer )
	M:OnGasExtracted( StartTimer )
	M:OnAreaChanged( StopTimer )
end
