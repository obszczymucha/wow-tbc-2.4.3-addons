local M = ModUi:NewModule( "restodruidmacros", { "Class", "TankTarget" } )

local targetTemplate = "/target %s\n"
local macroTemplate1 = "#showtooltip\n%s/startattack [noform:5]\n/cast [mod:alt,target=player]Lifebloom;[form:1]Maul;[form:3]Claw;Lifebloom\n"
local macroTemplate2 =
"#showtooltip\n%s/startattack [noform:5]\n/cast [mod:alt,form:0]Moonfire;[form:1]Lacerate;[mod:alt,target=player]Rejuvenation;Rejuvenation\n"
local macroTemplate3 =
"#showtooltip\n%s/startattack [noform:5]\n/castsequence [nomod:alt,form:5] reset=5 Nature's Swiftness, Healing Touch\n/cast [mod:alt,form:1]Cat Form;[mod:alt]Healing Touch;[noform:5]Tree of Life\n"
local macroTemplate4 = "#showtooltip\n%s/startattack [noform:5]\n/cast [mod:alt,form:5,target=player]Regrowth;[mod:alt]Regrowth;[form:5]Regrowth;Swipe\n"
local macroTemplate5 = "#showtooltip\n%s/startattack [noform:5]\n/cast [mod:alt,noform:5]Healing Touch;[form:3]Prowl;[form:5]Swiftmend;[form:1]Bash;Moonfire\n"

local function UpdateMacro( macroName, macroBody )
  local index = GetMacroIndexByName( macroName )
  EditMacro( index, nil, nil, macroBody, true, true )
end

local function MakeTargetCommand( tankTarget )
  if tankTarget then
    return format( targetTemplate, tankTarget )
  else
    return ""
  end
end

local function UpdateMacros()
  if InCombatLockdown() then return end

  local tankTarget = M:GetTankTarget()
  local targetCommand = MakeTargetCommand( tankTarget )

  UpdateMacro( "1", format( macroTemplate1, targetCommand ) )
  UpdateMacro( "2", format( macroTemplate2, targetCommand ) )
  UpdateMacro( "3", format( macroTemplate3, targetCommand ) )
  UpdateMacro( "4", format( macroTemplate4, targetCommand ) )
  UpdateMacro( "5", format( macroTemplate5, targetCommand ) )
end

function M.Initialize()
  if not M:IsDruid() then
    M.enabled = false
    return
  end

  M:OnEnterWorld( UpdateMacros )
  M:OnTankTarget( UpdateMacros )
end
