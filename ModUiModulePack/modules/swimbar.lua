local M = ModUi:NewModule( "swimbar" )

local function MoveSwimBar()
    M:MoveFrameByPoint( MirrorTimer1, { "BOTTOM", UIParent, "BOTTOM", 0, 80 } )
end

function M.Initialize()
    M:OnFirstEnterWorld( MoveSwimBar )
end
