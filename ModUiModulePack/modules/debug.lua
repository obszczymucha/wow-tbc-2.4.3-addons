local M = ModUi:NewModule( "debug", { "FarmingMode" } )
local frame = ChatFrame3

local function Resize( y )
	M:MoveFrameByPoint( ChatFrame3, { "TOPLEFT", UIParent, "TOPLEFT", 12, y } )
	M:SetHeight( ChatFrame3, 840 + y )
end

local function GroupChanged()
	if not M:IsInGroup() or M:IsFarmingMode() then
		Resize( -221 )
		return
	end

	local groupSize = M:GetNumGroupMembers()
	if groupSize > 13 then groupSize = 13 end
	local height = 30 * 0.85
	local y = -5 - ( height * groupSize ) - 5

	Resize( y - 142 )
end

local function OnEnterWorld()
	frame:Hide()
	GroupChanged()
end

local function Toggle( toggleFunc )
	M.debug = true
	M.enabled = true

	toggleFunc()
end

local function SayHello()
	M:DebugMsg( "Hello." )
end

local function ToggleDebugForModule( moduleName )
	local m = ModUi:GetModule( moduleName )

	if not m then
		M:PrettyPrint( format( "Module %s not found.", moduleName ) )
		return
	end

	m.debug = not m.debug
	
	local state = "debug off"
	if m.debug then state = "debug on" end
	m:PrettyPrint( state )

	if m.debug and not frame:IsVisible() then
		Toggle( function()
			frame:Show()
			SayHello()
		end )
	end
end

local function ToggleAndPrint()
	if frame:IsVisible() then
		frame:Hide()
	else
		frame:Show()
	end

	if frame:IsVisible() then
		M:PrettyPrint( "on" )
		SayHello()
	else
		M:PrettyPrint( "off" )
		M:DebugMsg( "Bye!" )
	end
end

local function ProcessSlashCommand( moduleName )
	if not moduleName or moduleName == "" or moduleName == " " then
		Toggle( ToggleAndPrint )
	else
		ToggleDebugForModule( moduleName )
	end
end

function M.Initialize()
	M:OnEnterWorld( OnEnterWorld )
	M:OnGroupChanged( GroupChanged )

	SLASH_DEBUG1 = "/debug"
	SlashCmdList[ "DEBUG" ] = ProcessSlashCommand
end
