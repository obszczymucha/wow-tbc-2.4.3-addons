local M = ModUi:NewModule( "misctext" )
local api = ModUi.facade.api

local zones_to_show = {
  "Zul'Aman",
  "The Black Morass",
  "Arathi Basin",
  "Eye of the Storm",
  --"Hellfire Peninsula"
}

local function should_show( zone_name )
  for _, z in ipairs( zones_to_show ) do
    if z == zone_name then
      return true
    end
  end

  return false
end

local function calculate_max_width()
  local result = 0

  for i = 1, 3 do
    local frame = _G[ string.format( "AlwaysUpFrame%dText", i ) ]
    if frame then
      local width = frame:GetWidth()
      if width > result then result = width end
    end
  end

  return result
end

local function decorate( frame )
  frame.OldShow = frame.Show
  frame.Show = function()
    local zone_name = api.GetRealZoneText()

    if should_show( zone_name ) then
      frame:OldShow()
      local max_width = (calculate_max_width() / 2) - 23
      M:MoveFrameByPoint( frame, { "LEFT", UIParent, "BOTTOM", -max_width, 60 } )
      --M:MoveFrameByPoint( WorldStateCaptureBar1, { "BOTTOM", frame, "TOP", 0, 30 } )
    else
      M:Hide( frame )
    end
  end
end

local function Show()
  ---@diagnostic disable-next-line: undefined-global
  WorldStateAlwaysUpFrame:Show()
end

local function OnFirstEnterWorld()
  ---@diagnostic disable-next-line: undefined-global
  local frame = WorldStateAlwaysUpFrame
  M:HideFunction( frame, "ClearAllPoints" )
  M:HideFunction( frame, "SetPoint" )
  decorate( frame )
  Show()
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnAreaChanged( function() M:ScheduleTimer( Show, 0.1 ) end )
  M:OnZoneChanged( function() M:ScheduleTimer( Show, 0.1 ) end )
end
