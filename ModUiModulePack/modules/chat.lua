local M = ModUi:NewModule( "chat", { "ShadowUFTarget", "FarmingMode", "ToggleMinimap", "ChatLeftSide" } )
local frame = ChatFrame1
local api = ModUi.facade.api
local m_left_side = false

local function PositionToTheLeft()
  M:MoveFrameByPoint( frame, { "TOPLEFT", UIParent, "TOPLEFT", 20, -20 } )
end

local function Position( isCombat, mapVisible )
  if M:IsFarmingMode() then
    M:MoveFrameByPoint( frame, { "TOPLEFT", MinimapCluster, "TOPRIGHT", 1, -26 } )
    frame:SetHeight( 145 )
    return
  end

  frame:SetHeight( 129 )

  if m_left_side == true then
    PositionToTheLeft()
    return
  end

  if mapVisible then
    if M:IsTargetting() then
      M:MoveFrameByPoint( frame, { "TOPLEFT", MinimapCluster, "TOPRIGHT", 1, -15 } )
    else
      M:MoveFrameByPoint( frame, { "TOPLEFT", MinimapCluster, "TOPRIGHT", 50, -15 } )
    end
  else
    if isCombat or M:IsTargetting() then
      M:MoveFrameByPoint( frame, { "TOPLEFT", SUFUnittarget, "TOPRIGHT", 13, 23 } )
    else
      M:MoveFrameByPoint( frame, { "TOPLEFT", SUFUnitplayer, "TOPRIGHT", 51, 22 } )
    end
  end
end

local function OnCombat()
  local mapVisible = M.ToggleMinimap.ShouldBeOnInCombat()
  Position( true, mapVisible )
end

local function OnOutOfCombat()
  local mapVisible = M.ToggleMinimap.ShouldBeOnNotInCombat()
  Position( false, mapVisible )
end

local function OnToggleMinimap()
  if api.InCombatLockdown() then return end
  OnOutOfCombat()
end

local function OnEnterWorld()
  M:SetWidth( frame, 405 )
  OnOutOfCombat()

  M:OnTargetAcquired( OnOutOfCombat )
  M:OnTargetLost( OnOutOfCombat )

  -- This refreshes the frame (I think)
  M:Hide( frame )
  M:Show( frame )
end

local function OnChatLeftSide()
  m_left_side = not m_left_side
  OnOutOfCombat()
end

function M.Initialize()
  M:OnEnterWorld( OnEnterWorld )
  M:OnZoneChanged( OnOutOfCombat )
  M:OnEnterCombat( OnCombat )
  M:OnRegenDisabled( OnCombat )
  M:OnLeaveCombat( OnOutOfCombat )
  M:OnRegenEnabled( OnOutOfCombat )
  M:OnFarmingMode( OnOutOfCombat )
  M:OnToggleMinimap( OnToggleMinimap )
  M:OnChatLeftSide( OnChatLeftSide )
end
