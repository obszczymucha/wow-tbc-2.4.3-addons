local M = ModUi:NewModule( "castingbar" )

local function OnFirstEnterWorld()
	local frame = CastingBarFrame
	if not frame then return end

	M:HideFunction( frame, "SetPoint" )
	M:MoveFrameByPoint( frame, { "CENTER", UIParent, "BOTTOM", 0, 85 } )
end

function M.Initialize()
	M:OnFirstEnterWorld( OnFirstEnterWorld )
end
