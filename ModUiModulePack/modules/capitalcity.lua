local M = ModUi:NewModule( "capitalcity", { "FarmingMode" } )

local function Toggle()
	if not M:IsFarmingMode() or InCombatLockdown() then return end

	local zone = GetRealZoneText()

	if zone == "Shattrath City" or zone == "Orgrimmar" or zone == "Thunder Bluff" or zone == "Undercity" then
		M:FarmingModeOff()
	end
end

function M.Initialize()
	M:OnAreaChanged( Toggle )
	M:OnLeaveCombat( Toggle )
	M:OnRegenEnabled( Toggle )
end
