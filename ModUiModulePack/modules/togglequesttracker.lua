local M = ModUi:NewModule( "togglequesttracker", { "Debug" } )

local hiddenByDebug = false

function M.Toggle( manual )
    local frame = Questie_BaseFrame

    if not frame then
        return
    end

    if frame:IsVisible() then
        M:Hide(frame)
    else
        M:Show(frame)
    end
end

local function OnDebugOn()
    local frame = Questie_BaseFrame
    if not frame then return end

    if frame:IsVisible() then
        hiddenByDebug = true
    end

    M:Hide(frame)
end

local function OnDebugOff()
    local frame = Questie_BaseFrame
    if not frame then return end

    if not frame:IsVisible() and hiddenByDebug == true then
        M:Show(frame)
    end

    hiddenByDebug = false
end

function M.Initialize()
    M:OnDebugOn( OnDebugOn )
    M:OnDebugOff( OnDebugOff )
end
