local M = ModUi:NewModule( "zoneinfo" )

local m_frame

local function OnFirstEnterWorld()
  ---@diagnostic disable-next-line: undefined-global
  if not ZoneTextFrame then
    error( "ZoneTextFrame doesn't exist!" )
    return
  end

  ---@diagnostic disable-next-line: undefined-global
  m_frame = ZoneTextFrame
end

---@diagnostic disable-next-line: unused-function, unused-local
local function top()
  M:MoveFrameByPoint( m_frame, { "TOP", UIParent, "TOP", 0, 32 } )
end

local function bottom()
  M:MoveFrameByPoint( m_frame, { "BOTTOM", UIParent, "BOTTOM", 0, 50 } )
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( bottom )
end
