local M = ModUi:NewModule( "target" )
local macro = ModUi.facade.macro
local api = ModUi.facade.api
local macroName = "T"
local targetIconIndex = 125

local function CreateFishingMacro()
  local pole = "Seth's Graphite Fishing Pole"

  if M:IsPlayerName( "Blanchot" ) then
    pole = "Fishing Pole"
  end

  local template = "#showtooltip\n/dismount [nomod,mounted]\n/equip [nomod,noequipped:Fishing Pole]%s\n/cast [nomod,equipped: Fishing Pole]Fishing"

  macro.edit( macroName, string.format( template, pole ), true )
  M:Print( "T: Fishing" )
end

local function CreateTargetMacro( args )
  local body = string.format(
    "/target %s\n/script local p=\"%s\";local n=UnitName(\"target\"); if n and string.sub(n, 1, string.len(p)) == p then SetRaidTarget(\"target\", 8) end", args,
    args )
  macro.edit( macroName, body, true, targetIconIndex )
  M:Print( string.format( "T: %s", args ) )
end

local function CreateProspectingMacro()
  macro.edit( macroName, "#showtooltip\n/cast Prospecting" )
  M:Print( "T: Prospecting" )
end

local function ProcessTargetSlashCommand( args )
  if api.InCombatLockdown() then
    M:Print( "Cannot change target macro in combat." )
    return
  end

  if not args or args == "" or args == " " then
    CreateFishingMacro()
  elseif args == "prospect" then
    CreateProspectingMacro()
  else
    CreateTargetMacro( args )
  end
end

local function Init()
  SLASH_TA1 = "/ta"
  api.SlashCmdList[ "TA" ] = ProcessTargetSlashCommand
  macro.create( macroName, nil, true, targetIconIndex )
end

function M.Initialize()
  M:OnFirstEnterWorld( Init )
end
