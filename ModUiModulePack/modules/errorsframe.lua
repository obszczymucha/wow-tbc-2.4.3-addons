local M = ModUi:NewModule( "errorsframe", { "FarmingMode" } )

local function NoCombat()
	M:MoveFrameByPoint( UIErrorsFrame, { "TOPLEFT", UIParent, "TOPLEFT", 400, -180 } )
end

local function Combat()
	M:MoveFrameByPoint( UIErrorsFrame, { "TOPLEFT", UIParent, "TOPLEFT", 1405, -46 } )
end

local function OnFarmingMode()
	if M:IsFarmingMode() then
		M:MoveFrameByPoint( UIErrorsFrame, { "TOP", BattlefieldMinimap, "BOTTOM", 8, -40 } )
	else
		NoCombat()
	end
end

local function OnEnterWorld()
	OnFarmingMode()
end

function M.Initialize()
	--M:OnEnterWorld( OnEnterWorld )
	--M:OnEnterCombat( NoCombat )
	--M:OnRegenDisabled( NoCombat )
	--M:OnLeaveCombat( OnFarmingMode )
	--M:OnRegenEnabled( OnFarmingMode )
	M:OnFarmingMode( OnFarmingMode )
end
