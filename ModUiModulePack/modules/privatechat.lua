local M = ModUi:NewModule( "privatechat" )

local frame

local function OnFirstEnterWorld()
	frame = ChatFrame5

	if not frame then
		M:PrettyPrint( "No private chat frame found!" )
		return
	end

	M:Show( frame )
	M:SetSize( frame, 430, 120 )
	M:MoveFrameByPoint( frame, { "BOTTOMLEFT", UIParent, "BOTTOMLEFT", 15, 20 } )
    M:Hide( ChatFrame5ChatSearchEditBox )
	M:Hide( ChatFrame5ResizeButton )
	M:Hide( ChatFrame5PratCCReminder )
end

local function Clear()
	if not frame then
		M:PrettyPrint( "No private chat frame found." )
		return
	end

	frame:Clear()
	M:PrettyPrint( "Cleared." )
end

function M.Initialize()
	SLASH_PC1 = "/pc"
	SlashCmdList[ "PC" ] = Clear

	M:OnFirstEnterWorld( OnFirstEnterWorld )
end
