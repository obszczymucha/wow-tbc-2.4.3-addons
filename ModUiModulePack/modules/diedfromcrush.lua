local M = ModUi:NewModule( "diedfromcrush" )
local PLAYER_NAME = UnitName( "player" )
local lastHitCrushed = false
local warningFrame

local function CreateWarningFrame()
  local frame = CreateFrame( "FRAME", "DiedFromCrushWarningFrame", UIParent )
  frame:Hide()

  local label = frame:CreateFontString( nil, "OVERLAY" )
  label:SetFont( "FONTS\\FRIZQT__.TTF", 17, "OUTLINE" )
  label:SetPoint( "TOPLEFT", 0, 0 )
  label:SetText( "Died from a |cffff4040CRUSH|r :(" )
  M:SetSize( frame, label:GetWidth(), label:GetHeight() )
  frame:SetPoint( "TOPLEFT", UIParent, "TOPLEFT", (UIParent:GetWidth() / 2) - (frame:GetWidth() / 2), -270 )

  return frame
end

local function ShowWarning()
  M:DisplayAndFadeOut( warningFrame )
end

local function Reset()
  lastHitCrushed = false
end

local function Update( crushing )
  if crushing then
    lastHitCrushed = true
  else
    lastHitCrushed = false
  end
end

local function OnSpellDamage( spellName, sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing, crushing )
  if destName == PLAYER_NAME then
    Update( crushing )
  end
end

local function OnSwingDamage( sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing, crushing )
  if destName == PLAYER_NAME then
    Update( crushing )
  end
end

local function OnRangeDamage( sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing, crushing )
  if destName == PLAYER_NAME then
    Update( crushing )
  end
end

local function OnUnitDied( unitName )
  if unitName ~= PLAYER_NAME then return end

  if lastHitCrushed then
    ShowWarning()
  end
end

local function OnCombatLogEventUnfiltered( _, event, _, sourceName, _, _, destName, _, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19,
                                           arg20 )
  if event == "SPELL_DAMAGE" then
    OnSpellDamage( arg10, sourceName, destName, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20 )
  elseif event == "SWING_DAMAGE" then
    OnSwingDamage( sourceName, destName, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20 )
  elseif event == "RANGE_DAMAGE" then
    OnRangeDamage( sourceName, destName, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20 )
  elseif event == "UNIT_DIED" then
    OnUnitDied( destName )
  end
end


function M.Initialize()
  warningFrame = CreateWarningFrame()
  M:OnCombatLogEventUnfiltered( OnCombatLogEventUnfiltered )
  M:OnEnterCombat( Reset )
  M:OnRegenDisabled( Reset )
end

SShowWarning = ShowWarning
