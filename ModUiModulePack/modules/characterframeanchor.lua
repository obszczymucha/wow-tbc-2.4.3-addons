local M = ModUi:NewModule( "characterframeanchor" )
local frame
local isVisible = false
local position = { "TOP", "TOP", 0, -230 }

local function Update()
	if isVisible then
		M:Show( frame )
        CharacterFrame:Show()
        M:MoveFrameByPoint( CharacterFrame, { "TOPLEFT", frame, "TOPLEFT", 0, 0 } )
        M:HideFunction( CharacterFrame, "SetPoint" )
		M:DebugMsg( "Visible" )
	else
		M:Hide( frame )
		M:DebugMsg( "Hidden" )
	end
end

local function OnDragStop()
	frame:StopMovingOrSizing()

	local p, _, parentPoint, x, y = frame:GetPoint()
	M:DebugMsg( format( "%s %s |cffff9f69%d|r |cffff9f69%d|r", p, parentPoint, x, y ) )
	ModUiDb.characterFrameAnchor.position = { p, "UIParent", parentPoint, x, y }
end

local function CreateAnchor()
	frame = CreateFrame( "FRAME", "CharacterFrameAnchorFrame", UIParent, BackdropTemplateMixin and "BackdropTemplate")
	frame:SetBackdrop({
		bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
		edgeFile = "Interface\\ChatFrame\\ChatFrameBackground",
		tile = true,
		tileSize = 1,
		edgeSize = 1,
	})

	frame:SetBackdropColor( 0.1, 0.1, 0.1, 0.6 )
	frame:SetBackdropBorderColor( 0.1, 0.1, 0.1, 0.8 )
	frame:SetFrameStrata( "HIGH" )
	frame:SetMovable( true )
	frame:EnableMouse( true )
	frame:RegisterForDrag( "LeftButton" )
	frame:SetScript( "OnDragStart", frame.StartMoving )
	frame:SetScript( "OnDragStop", OnDragStop )
	frame:SetClampedToScreen( false )
	frame:SetAlpha( 0.7 )

	Update()
	
	local label = frame:CreateFontString( nil, "OVERLAY" )
	label:SetFont( "FONTS\\FRIZQT__.TTF", 9, "OUTLINE" )
	label:SetPoint( "CENTER", 1, 0 )
	label:SetText( "FA" )

	M:SetSize( frame, 23, 20 )
	local point, parentPoint, x, y = unpack( position )
	M:MoveFrameByPoint( CharacterFrameAnchorFrame, { point, UIParent, parentPoint, x, y } )
end

local function Toggle()
	isVisible = not isVisible
	Update()
end

local function ProcessSlashCommand( command )
	if not command or command == "" or command == " " then
		Toggle()
	elseif command == "reset" then
		local minimapScale = Minimap:GetScale()
		M:MoveFrameByPoint( frame, { "BOTTOMRIGHT", UIParent, "CENTER", 0, 0 } )
	else
		M:Print( format( "Unknown command: |cffff9f69%s|r", command ) )
	end
end

local function LoadPosition()
	if not ModUiDb.characterFrameAnchor then
        ModUiDb.characterFrameAnchor = {}
    end

    if ModUiDb.characterFrameAnchor.position then
		M:MoveFrameByPoint( frame, ModUiDb.characterFrameAnchor.position )
	end
end

local function OnFirstEnterWorld()
	SLASH_CFA1 = "/cfa"
	SlashCmdList[ "CFA" ] = ProcessSlashCommand
end

function M.Initialize()
	CreateAnchor()
	M:OnFirstEnterWorld( OnFirstEnterWorld )
	M:OnEnterWorld( LoadPosition )
end
