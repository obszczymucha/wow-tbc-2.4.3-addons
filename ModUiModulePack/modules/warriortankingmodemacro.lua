local M = ModUi:NewModule( "warriortankingmodemacro", { "TankingMode" } )
local TANKING_MODE_MACRO_TEMPLATE = "#showtooltip\n/startattack [harm]\n/cast [mod:shift]16;[mod:ctrl]17;[mod:alt] %s; %s\n/equipslot 16 The Brutalizer"
local HEROIC_STRIKE = "Heroic Strike"
local CLEAVE = "Cleave"

local function GetTankingModeParams()
	if M:IsBossMode() then
		return HEROIC_STRIKE, CLEAVE
	else
		return CLEAVE, HEROIC_STRIKE
	end
end

local function OnTankingMode()
	local index = GetMacroIndexByName( "TankingMode" )
	local primaryAttack, secondaryAttack = GetTankingModeParams()

	EditMacro( index, nil, nil, format( TANKING_MODE_MACRO_TEMPLATE, secondaryAttack, primaryAttack ), true, true )
end

function M.Initialize()
	M:OnFirstEnterWorld( OnTankingMode )
	M:OnTankingMode( OnTankingMode )
end
