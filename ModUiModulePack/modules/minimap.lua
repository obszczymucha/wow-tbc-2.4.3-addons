local M = ModUi:NewModule( "minimap", { "ShadowUFTarget", "FarmingMode", "FarmingAnchor", "ToggleMinimap" } )
local api = ModUi.facade.api
local frames
local addon

local function SetMinimapSkin( skinIndex )
  if not addon then return end

  addon.modules.skins.db.profile.skin = skinIndex
  addon:UpdateScreen()
end

local function Position()
  if M:IsFarmingMode() then
    SetMinimapSkin( 2 )
    frames.minimap:SetScale( 1.1045262813568 )
    M:MoveFrameByPoint( frames.minimap, { "TOPLEFT", M:GetFarmingAnchor(), "TOPLEFT", -35, 22 } )
    return
  end

  SetMinimapSkin( 2 )
  frames.minimap:SetScale( 0.8 )

  if M:IsTargetting() then
    M:MoveFrameByPoint( frames.minimap, { "TOPLEFT", frames.target, "TOPRIGHT", 0, 49 } )
  else
    M:MoveFrameByPoint( frames.minimap, { "TOPLEFT", frames.player, "TOPRIGHT", 51, 46 } )
  end
end

local function OnOutOfCombat()
  if M:IsFarmingMode() or M.ToggleMinimap.ShouldBeOnNotInCombat() then
    M:Show( frames.minimap )
    Position()
  else
    M:Hide( frames.minimap )
  end
end

local function OnCombat()
  if M.ToggleMinimap.ShouldBeOnInCombat() then
    M:Show( frames.minimap )
    Position()
  else
    M:Hide( frames.minimap )
  end
end

local function SetGlobals()
  ---@diagnostic disable-next-line: undefined-global
  addon = simpleMinimap

  frames = {
    ---@diagnostic disable-next-line: undefined-global
    player = SUFUnitplayer,
    ---@diagnostic disable-next-line: undefined-global
    target = SUFUnittarget,
    ---@diagnostic disable-next-line: undefined-global
    minimap = MinimapCluster
  }
end

local function OnToggleMinimap()
  if api.InCombatLockdown() then return end
  OnOutOfCombat()
end

local function OnFirstEnterWorld()
  SetGlobals()

  if not addon or not frames.minimap then
    M.enabled = false
    return
  end

  M:OnTargetAcquired( Position )
  M:OnTargetLost( Position )
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( OnOutOfCombat )
  M:OnEnterCombat( OnCombat )
  M:OnRegenDisabled( OnCombat )
  M:OnLeaveCombat( OnOutOfCombat )
  M:OnRegenEnabled( OnOutOfCombat )
  M:OnFarmingMode( OnToggleMinimap )
  M:OnToggleMinimap( OnToggleMinimap )
end
