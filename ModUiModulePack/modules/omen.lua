local M = ModUi:NewModule( "omen" )

local function Show()
  M:Show( OmenAnchor )
  Omen:EnableLastModule()
  Omen:UpdateDisplay()
end

local function Hide()
  M:Hide( OmenAnchor )
  Omen:DisableModules()
end

local function OnFirstEnterWorld()
  OmenAnchor:SetWidth( 136 )
  OmenBarList:SetWidth( 135 )
  Omen:EnableLastModule()
  Omen:UpdateDisplay()
  M:HideFunction( OmenAnchor, "Show" )
  M:HideFunction( OmenAnchor, "Hide" )
end

local function OnEnterWorld()
  if not M:IsInCombat() then
    Hide()
  end
end

function M.Initialize()
  if not Omen or not OmenAnchor then
    M.enabled = false
    return
  end

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( OnEnterWorld )
  M:OnEnterCombat( Show )
  M:OnRegenDisabled( Show )
  M:OnLeaveCombat( Hide )
  M:OnRegenEnabled( Hide )
end
