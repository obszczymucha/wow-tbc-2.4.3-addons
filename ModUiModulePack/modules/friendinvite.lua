local M = ModUi:NewModule( "friendinvite" )

function IsPlayerAFriend( playerName )
	local index = 1

	repeat
		local name = GetFriendInfo( index )	
		if name == playerName then return true end
		index = index + 1
	until not name or name == "Unknown"
	
	return false
end

local function OnGroupInvite( playerName )
	if IsPlayerAFriend( playerName ) then
		ModUi:ScheduleTimer( function() SendChatMessage( format( "Oh, hai %s! I will accept your invite because you're my frend.", playerName ), "WHISPER", nil, playerName ) end, 2 )
		ModUi:ScheduleTimer( function()
			AcceptGroup()
			if StaticPopup1 and StaticPopup1:IsVisible() and StaticPopup1Button1 then StaticPopup1Button1:Click() end
		end, 3 )
	end
end

function M.Initialize()
	M:OnFirstEnterWorld( ShowFriends )
	M:OnPartyInviteRequest( OnGroupInvite )
end
