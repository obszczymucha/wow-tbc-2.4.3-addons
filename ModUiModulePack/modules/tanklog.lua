local M = ModUi:NewModule( "tanklog", { "FarmingMode" } )
local tanklog = ChatFrame4
local api = ModUi.facade.api

local function Show()
  tanklog:SetAlpha( 1 )
end

local function Hide()
  tanklog:SetAlpha( 0 )
end

local function Combat()
  M:MoveFrameByPoint( tanklog, { "TOPLEFT", UIParent, "TOPLEFT", 890, -198 } )
  M:SetHeight( tanklog, 86 )
  Show()
end

local function Toggle()
  if M:IsFarmingMode() then
    Hide()
  else
    Show()
  end
end

local function NoCombat()
  M:MoveFrameByPoint( tanklog, { "TOPLEFT", UIParent, "TOPLEFT", 890, -180 } )
  M:SetHeight( tanklog, 86 )
  Toggle()
end

local function OnEnterCombat()
  Combat()
end

local function OnRegenDisabled()
  Combat()
end

local function OnLeaveCombat()
  NoCombat()
end

local function OnRegenEnabled()
  NoCombat()
end

local function OnFirstEnterWorld()
  if api.InCombatLockdown() then return end
  NoCombat()
  M:DebugMsg( "ok" )
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( NoCombat )
  M:OnEnterCombat( OnEnterCombat )
  M:OnRegenDisabled( OnRegenDisabled )
  M:OnLeaveCombat( OnLeaveCombat )
  M:OnRegenEnabled( OnRegenEnabled )
  M:OnFarmingMode( Toggle )
end
