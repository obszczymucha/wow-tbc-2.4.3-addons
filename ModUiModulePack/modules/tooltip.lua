local M = ModUi:NewModule( "tooltip", { "FarmingMode" } )

local function Normal()
	M:MoveFrameByPoint( TipTacAnchor, { "TOPRIGHT", UIParent, "TOPRIGHT", -15, -180 } )
end

local function OnEnterWorld()
	GameTooltip_SetTooltipAnchor( "TOPRIGHT" )
	Normal()
	M:MoveFrameByPoint( GameTooltip, { "TOPRIGHT", "TipTacAnchor", "TOPRIGHT", 0, 0 } )
end

local function OnFarmingMode()
	if M:IsFarmingMode() then
		M:MoveFrameByPoint( TipTacAnchor, { "TOPRIGHT", BattlefieldMinimap, "TOPLEFT", -17, 0 } )
	else
		Normal()
	end
end

function M.Initialize()
	if not GameTooltip_SetTooltipAnchor or not GameTooltip then
		M.enabled = false
		M:DebugMsg( "Disabling. No GameTooltip/anchor found." )
		return
	end

	M:OnEnterWorld( OnEnterWorld )
	M:OnFarmingMode( OnFarmingMode )
end
