local M = ModUi:NewModule( "questwatch" )

local function TopLeft()
	M:MoveFrameByPoint( QuestWatchFrame, { "TOPLEFT", UIParent, "TOPLEFT", 20, -10 } )
	M:MoveFrameByPoint( QuestTimerFrame, { "TOPLEFT", UIParent, "TOPLEFT", 230, -20 } )
end

local function Move()
	M:HideFunction( QuestTimerFrame, "ClearAllPoints" )
	M:HideFunction( QuestTimerFrame, "SetPoint" )

	if M:IsInGroup() then
		if Recount_MainWindow then
			M:MoveFrameByPoint( QuestWatchFrame, { "TOPLEFT", Recount_MainWindow, "BOTTOMLEFT", 10, 0 } )
			M:MoveFrameByPoint( QuestTimerFrame, { "TOPLEFT", Recount_MainWindow, "RIGHT", 10, 20 } )
		else
			TopLeft()
		end
	else
		TopLeft()
	end
end

function M.Initialize()
	M:OnEnterWorld( Move )
	M:OnGroupChanged( Move )
end
