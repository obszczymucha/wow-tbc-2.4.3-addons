local M = ModUi:NewModule( "nameplates" )

local function IsShowingEnemyNameplates()
    return GetCVar( "nameplateShowEnemies" ) == "1"
end

local function ShowEnemyNameplates()
    M:PrettyPrint( "Showing enemy nameplates." )
    SetCVar( "nameplateShowEnemies", 1 )
end

local function HideEnemyNameplates()
    M:PrettyPrint( "Hiding enemy nameplates." )
    SetCVar( "nameplateShowEnemies", 0 )
end

local function Toggle()
    local zoneText = GetZoneText()

    if zoneText == "Shattrath City" then
        if not IsShowingEnemyNameplates() then return end
        HideEnemyNameplates()
    else
        if IsShowingEnemyNameplates() then return end
        ShowEnemyNameplates()
    end
end

function M.Initialize()
    M.enabled = false
    M:OnEnterWorld( Toggle )
	M:OnZoneChanged( Toggle )
    M:OnAreaChanged( Toggle )
end
