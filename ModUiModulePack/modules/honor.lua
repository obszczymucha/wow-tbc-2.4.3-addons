local M = ModUi:NewModule( "honortracker" )
--C_CurrencyInfo.GetCurrencyInfo(1901).quantity

local api = {
  CreateFrame = CreateFrame,
  UIParent = UIParent
}

local frame

local function create_frame()
	frame = api.CreateFrame( "FRAME", "HonorTrackingFrame", api.UIParent )
	frame:Hide()

	local label = frame:CreateFontString( nil, "OVERLAY" )
	label:SetFont( "FONTS\\FRIZQT__.TTF", 17, "OUTLINE" )
	label:SetPoint( "TOPLEFT", 0, 0 )
	label:SetText( "Honor: 0" )
	M:SetSize( frame, label:GetWidth(), label:GetHeight() )
	frame:SetPoint( "BOTTOMLEFT", "SUFUnitplayer", "TOPLEFT", 0, 10 )
end

function M.Initialize()
  M:OnFirstEnterWorld( create_frame )
end
