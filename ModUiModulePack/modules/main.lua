local function IsInGroup()
    return false
end

local function IsInRaid()
    return false
end

local function GetAllPlayersInMyGroup( component )
	local result = {}

	if not ( IsInGroup() or IsInRaid() ) then
		component:Print(component:MyName())
		table.insert( result, component:MyName() )
		return result
	end

	for i = 1, 40 do
		local name = GetRaidRosterInfo( component, i )
		if name then table.insert( result, name ) end
	end

	return result
end

local component = {}
component.Print = function( this, text )
    print( text )
end

component.MyName = function( this )
    return "Psikutas"
end

local result = GetAllPlayersInMyGroup( component )

print( #result )

function string_simil (fx, fy)
	local n = string.len(fx)
	local m = string.len(fy)
	local ssnc = 0
  
	if n > m then
	  fx, fy = fy, fx
	  n, m = m, n
	end
  
	for i = n, 1, -1 do
	  if i <= string.len(fx) then
	  for j = 1, n-i+1, 1 do
		  local pattern = string.sub(fx, j, j+i-1)
		  if string.len(pattern) == 0 then break end
		  local found_at = string.find(fy, pattern)
		  if found_at ~= nil then
			ssnc = ssnc + (2*i)^2
			fx = string.sub(fx, 0, j-1) .. string.sub(fx, j+i)
			fy = string.sub(fy, 0, found_at-1) .. string.sub(fy, found_at+i)
			break
		  end
		end
	  end
	end
  
	return (ssnc/((n+m)^2))^(1/2)
  
  end

  print( string_simil( "Psikutas", "Psikutas" ) )
  print( string_simil( "Asbern", "Ásbern" ) )
  print( string_simil( "Angelababee", "Agenlababee" ) )
  print( string_simil( "Chuj", "Juch" ) )
