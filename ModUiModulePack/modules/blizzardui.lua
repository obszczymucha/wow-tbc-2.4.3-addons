local M = ModUi:NewModule( "blizzardui" )

local function OnFirstEnterWorld()
  M:ScheduleTimer( function()
    ---@diagnostic disable-next-line: undefined-global
    M:MoveFrameByPoint( TicketStatusFrame, { "TOPRIGHT", UIParent, "TOPRIGHT", -15, -15 } )
  end, 1 )
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
end
