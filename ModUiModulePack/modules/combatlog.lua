local M = ModUi:NewModule( "combatlog" )
local frame = ChatFrame2

local function Toggle()
  if not frame then return end

  if frame:IsVisible() then
    M:Hide( frame )
  else
    M:Show( frame )
  end
end

local function OnFirstEnterWorld()
  SLASH_CL1 = "/cl"
  SlashCmdList[ "CL" ] = Toggle

  M:Hide( ChatFrame2TabText )
  M:Hide( ChatFrame2TabGlow )
  M:Hide( ChatFrame2TabHighlightLeft )
  M:Hide( ChatFrame2TabSelectedLeft )
  M:Hide( ChatFrame2TabLeft )
  M:Hide( ChatFrame2TabHighlightMiddle )
  M:Hide( ChatFrame2TabSelectedMiddle )
  M:Hide( ChatFrame2TabMiddle )
  M:Hide( ChatFrame2TabHighlightRight )
  M:Hide( ChatFrame2TabSelectedRight )
  M:Hide( ChatFrame2TabRight )

  frame:Hide()
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
end

