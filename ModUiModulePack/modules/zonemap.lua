local M = ModUi:NewModule( "zonemap", { "FarmingMode", "FarmingAnchor" } )

local function SetUp()
  LoadAddOn( "Blizzard_BattlefieldMinimap" )

  if not IsAddOnLoaded( "Blizzard_BattlefieldMinimap" ) then
    while NextEvent( "ADDON_LOADED" ) ~= "Blizzard_BattlefieldMinimap" do end
  end

  BattlefieldMinimapBackground:Hide()
  BattlefieldMinimapCloseButton:Hide()
  BattlefieldMinimapCorner:Hide()

  BattlefieldMinimapTab:SetFrameStrata( "HIGH" )
  BattlefieldMinimapTabLeft:SetTexture( nil )
  BattlefieldMinimapTabMiddle:SetTexture( nil )
  BattlefieldMinimapTabRight:SetTexture( nil )
  BattlefieldMinimapTabText:Hide()

  BattlefieldMinimap:EnableMouseWheel( false )
  BattlefieldMinimap:SetMovable( false )

  -- Fix the BattlefieldMinimap's w/h ratio
  local worldMapWidth, worldMapHeight = WorldMapDetailFrame:GetWidth(), WorldMapDetailFrame:GetHeight()
  local tileSize = BattlefieldMinimap1:GetWidth()
  local worldTileSize = WorldMapDetailTile1:GetWidth()

  BattlefieldMinimap:SetWidth( worldMapWidth / worldTileSize * tileSize )
  BattlefieldMinimap:SetHeight( (worldMapHeight / worldTileSize * tileSize) + 1 )

  BattlefieldMinimap:SetScale( 1.049165725708 )
  M:MoveFrameByPoint( BattlefieldMinimap, { "TOPRIGHT", M:GetFarmingAnchor(), "TOPLEFT", 1, -0.5 } )
end

local function HideBattlefieldMinimapPOIs()
  local s = "BattlefieldMinimapPOI"
  local i = 1

  while _G[ s .. i ] do
    local frame = _G[ s .. i ]
    frame.Show = function() end
    frame:Hide()

    i = i + 1
  end
end

local function Show()
  if not BattlefieldMinimap then return end

  M:Show( BattlefieldMinimap )
  HideBattlefieldMinimapPOIs()
  BattlefieldMinimapBackground:Hide()
  BattlefieldMinimapCloseButton:Hide()
  BattlefieldMinimapCorner:Hide()
end

local function Hide()
  M:Hide( BattlefieldMinimap )
end

local function Toggle()
  if M:IsFarmingMode() then
    Show()
  else
    Hide()
  end
end

local function OnEnterWorld()
  Toggle()
end

function M.Initialize()
  SetUp()
  M:OnEnterWorld( OnEnterWorld )
  M:OnFarmingMode( Toggle )
  M:OnEnterCombat( Hide )
  M:OnRegenDisabled( Hide )
  M:OnLeaveCombat( Toggle )
  M:OnRegenEnabled( Toggle )
  M:OnWorldStatesUpdated( HideBattlefieldMinimapPOIs )
end
