local M = ModUi:NewModule( "ctc", { "OutfitterEvents", "Class" } )
local api = ModUi.facade.api
local hl = ModUi.colors.hl

local function PrintCtc()
  local ctc = api.GetDodgeChance() + api.GetBlockChance() + api.GetParryChance()

  M:PrettyPrint( string.format( "%.2f%%", ctc ) )
end

local function OnOutfitChanged()
  PrintCtc()
end

function M.Initialize()
  if not M:IsWarrior() then
    M.enabled = false
    return
  end

  SLASH_CTC1 = "/ctc"
  api.SlashCmdList[ "CTC" ] = PrintCtc

  M:OnOutfitChanged( OnOutfitChanged )
end
