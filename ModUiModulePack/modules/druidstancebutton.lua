local M = ModUi:NewModule( "druidstancebutton", { "Class" } )

local function Init()
    M:Hide( BT4StanceButton1 )
end

function M.Initialize()
    if not M:IsDruid() then
        M.enabled = false
    end

    M:OnFirstEnterWorld( Init )
end
