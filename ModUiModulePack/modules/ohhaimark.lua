local M = ModUi:NewModule( "ohhaimark" )

local was_in_party = false
local greetings = {
  { "Oh, hai %s! Didn't know it was you." },
  { "Oh, hai %s. You're my favourite customer!" },
  { "You're tearing me apart, %s!" },
  { "%s is cranky today hahah! Girl trouble, I guess..." },
  { "Lisa loves you too %s, as a person, as a human bean." },
  { "Hai %s. I am so happy I have you as my best friend, and I love Lisa so much!" },
  { "I did not hit her! It's not true, it's bullshit! I did not hit her, I DID NAAT! Oh, hai %s." },
  { "What a story, %s!" },
  { "Only %s can challenge a bald man!" },
  { "What is love? %s don't hurt me!" },
  { "%s. The one. The only. The legend." },
  { "Do you smoke a lot %s?" },
  { "%s, do you have a liver when you're drinking?" },
  { "Give it to me %s! Mhmm, mhmm. <3" },
  { "%s, I thought you gave your liver to your dad..." },
  { "How can %s slap???" },
  { "Leeeeeeeroooooyyyy %skins!!" },
  { "Hasta la vista, %s!" },
  { "You can love %s deep inside your heart and there is nothing wrong about it." },
  { "Keep in mind %s, if you have any problems, talk to me and I will help you!" },
  { "Do you want me to order a pizza, %s?" },
  { "I got the results of the tests back. You definitely have a breast cancer, %s." },
  { "You are lying %s, I never hit you!!" },
  { "You are part of our family %s and we love you very much!" },
  { "HELL YEAH %s!",                                                                              string.upper },
  { "Hi %s. Did you get your GDKP-19 vaccine yet?" },
  { "Good mornink %s, it's me (._.)" },
  { "Do you like turtles %s?" },
  { "You are not prepared %s!" },
  { "%s, why are you gae?" },
  { "%s, are you DN certified?" },
  { "Welcome %s to this evening's presentation!" },
  { "%s 2024!" },
  { "May the NI HAO be with you %s!" },
  { "I'm tired, I'm wasted, I love you darlink %s! UwU" },
  { "NI HAO %s!!",                                                                                string.upper },
  { "I love Cheesy Poofs, %s loves Cheesy Poofs. If we didn't eat Cheesy Poofs, we'd be LAME!" }
}

local groupPartyGreetings = { "Oh, hai everybody! Didn't know it was you.", "Oh, hai everybody! You're my favourite customers." }

local party_message_cache = {}
local guild_message_cache = {}

math.randomseed( time() )

local function shuffle( list )
  for i = #list, 2, -1 do
    local j = math.random( i )
    list[ i ], list[ j ] = list[ j ], list[ i ]
  end
end

local function GetPartyGreetings()
  return ModUiDb.ohhaimark.partyGreetings
end

local function SetPartyGreetings( partyGreetings )
  ModUiDb.ohhaimark.partyGreetings = partyGreetings
end

local function GetGuildGreetings()
  return ModUiDb.ohhaimark.guildGreetings
end

local function SetGuildGreetings( guildGreetings )
  ModUiDb.ohhaimark.guildGreetings = guildGreetings
end

local function ShuffleGreetings()
  local greetings = greetings
  shuffle( greetings )

  return greetings
end

local function schedule_message( message_cache, message, channel, min, max )
  table.insert( message_cache, message ) -- Add message to the list

  local function send_next_message( _cache, _channel, _min, _max )
    if #_cache > 0 then
      local next_message = table.remove( _cache, 1 )
      SendChatMessage( next_message, _channel )

      M:ScheduleTimer( function() send_next_message( _cache, _channel, _min, _max ) end, math.random( _min, _max ) )
    end
  end

  -- Schedule the initial message after 6-9 seconds
  if #message_cache == 1 then
    M:ScheduleTimer( function() send_next_message( message_cache, channel, min, max ) end, math.random( min, max ) )
  end
end

local function IsInParty()
  return GetNumRaidMembers() == 0 and GetNumPartyMembers() > 0
end

local function UpdatePartyStatus()
  was_in_party = IsInParty()
end

local function OnPlayerJoinsTheParty( playerName )
  UpdatePartyStatus()

  local greetings = GetPartyGreetings()
  local choice = table.remove( greetings )

  if #greetings == 0 then
    SetPartyGreetings( ShuffleGreetings() )
  else
    SetPartyGreetings( greetings )
  end

  local message = choice[ 1 ]
  local f = choice[ 2 ]
  schedule_message( party_message_cache, string.format( message, f and f( playerName ) or playerName ), "PARTY", 7, 9 )
end

local function OnPlayerJoinsTheGuild( playerName )
  local greetings = GetGuildGreetings()
  local choice = table.remove( greetings )

  if #greetings == 0 then
    SetGuildGreetings( ShuffleGreetings() )
  else
    SetGuildGreetings( greetings )
  end

  local message = choice[ 1 ]
  local f = choice[ 2 ]
  schedule_message( guild_message_cache, string.format( message, f and f( playerName ) or playerName ), "GUILD", 8, 11 )
end

local function OnPlayerHasLeftTheGuild( playerName )
  M:ScheduleTimer( function() SendChatMessage( "welcome", "GUILD" ) end, 3 )
end

local function OnPlayerHasBeenKickedOutOfTheGuild( playerName )
  M:ScheduleTimer( function() SendChatMessage( "welcome", "GUILD" ) end, 3 )
end

local function OnChatMsgSystem( message )
  if string.match( message, "joins the party." ) then
    OnPlayerJoinsTheParty( string.gmatch( message, "[^%s]+" )() )
  elseif string.match( message, "has joined the guild." ) then
    OnPlayerJoinsTheGuild( string.gmatch( message, "[^%s]+" )() )
  elseif string.match( message, "has left the guild" ) then
    OnPlayerHasLeftTheGuild( string.gmatch( message, "[^%s]+" )() )
  elseif string.match( message, "has been kicked out of the guild" ) then
    OnPlayerHasBeenKickedOutOfTheGuild( string.gmatch( message, "[^%s]+" )() )
  end
end

local function GreetPartyMembers()
  if GetNumPartyMembers() == 1 then
    OnPlayerJoinsTheParty( UnitName( "party1" ) )
  else
    local choice = math.random( #groupPartyGreetings )
    M:ScheduleTimer( function() SendChatMessage( groupPartyGreetings[ choice ], "PARTY" ) end, 3 )
  end
end

local function GroupChanged()
  if not was_in_party and IsInParty() then
    GreetPartyMembers()
  end

  UpdatePartyStatus()
end

local function SetupStorage()
  if not ModUiDb.ohhaimark then ModUiDb.ohhaimark = {} end

  if not ModUiDb.ohhaimark.partyGreetings or #ModUiDb.ohhaimark.partyGreetings == 0 then
    SetPartyGreetings( ShuffleGreetings() )
  end

  if not ModUiDb.ohhaimark.guildGreetings or #ModUiDb.ohhaimark.guildGreetings == 0 then
    SetGuildGreetings( ShuffleGreetings() )
  end
end

local function OnFirstEnterWorld()
  SetupStorage()
  UpdatePartyStatus()
end

function M.Initialize()
  if UnitName( "player" ) ~= "Ohhaimark" then
    M.enabled = false
    return
  end

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnChatMsgSystem( OnChatMsgSystem )
end
