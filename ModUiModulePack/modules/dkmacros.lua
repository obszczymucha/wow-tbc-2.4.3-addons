local M = ModUi:NewModule( "dkmacros" )
if not M then return end
local api = ModUi.facade.api
local macro = ModUi.facade.macro
local lua = ModUi.facade.lua

local function UpdateButtonF1()
  macro.edit( "F1", "#showtooltip\n/cast [mod:alt]Frost Presence;Pestilence" )
end

local function UpdateButtonF2()
  macro.edit( "F2", "#showtooltip\n/startattack [harm]\n/cast [mod:alt]Blood Presence;Blood Boil" )
end

local function UpdateButtonF3()
  macro.edit( "F3", "#showtooltip\n/startattack [harm]\n/cast War Stomp" )
end

local function UpdateButtonF4()
  macro.edit( "F4", "#showtooltip\n/startattack [harm]\n/cast Chains of Ice" )
end

local function UpdateButtonF5()
  macro.edit( "F5", "#showtooltip\n/cast Raise Dead" )
end

local function UpdateButtonF6()
  macro.edit( "F6", "#showtooltip\n/cast Death Pact" )
end

local function UpdateButton1()
  -- 22829: Super Healing Potion
  -- 32947: Auchenai Healing Potion
  local hs = (M:CountItemsById( 22103 ) + M:CountItemsById( 22104 ) + M:CountItemsById( 22105 )) > 0 and "Master Healthstone" or nil
  local pot = M:CountItemsById( 22829 ) > 0 and "Super Healing Potion" or M:CountItemsById( 32947 ) > 0 and "Auchenai Healing Potion" or nil
  local consume = hs or pot or nil
  local consumeTemplate = consume and lua.format( "/use [mod:shift]%s\n", consume ) or ""
  local template = "#showtooltip\n/startattack [harm]\n%s/cast [mod:alt]Vampiric Blood;Icy Touch"
  macro.edit( "1", lua.format( template, consumeTemplate ) )
end

local function UpdateButton2()
  macro.edit( "2", "#showtooltip\n/startattack [harm]\n/cast [mod:alt]Icebound Fortitude;Plague Strike" )
end

local function UpdateButton3()
  macro.edit( "3", "#showtooltip\n/startattack [harm]\n/cast [mod:alt]Heart Strike;Blood Strike" )
end

local function UpdateButton4()
  macro.edit( "4", "#showtooltip\n/startattack [harm]\n/cast [mod:alt]Blood Tap;Death Strike" )
end

local function UpdateButton5()
  macro.edit( "5", "#showtooltip\n/cast [mod:alt]Mark of Blood;Rune Tap" )
end

local bandage = "Heavy Netherweave Bandage"

local function UpdateButtonF()
  -- 34062: Conjured Mana Biscuit
  -- 29448: Mag'har Mild Cheese
  local c = function( id ) return M:CountItemsById( id ) > 0 end
  local food = c( 34062 ) and "Conjured Mana Biscuit" or c( 29448 ) and "Mag'har Mild Cheese" or ""
  local bandageTemplate = lua.format( "[mod:ctrl,noharm,nodead]%s", bandage )
  local foodTemplate = food == "" and lua.format( "/use %s\n", bandageTemplate ) or
      lua.format( "/use [nocombat,mod:alt]%s;%s\n", food, bandageTemplate )
  local template = "#showtooltip\n%s/cast [mod:alt]Rune Strike;[mod:shift]Horn of Winter;Death Coil"
  macro.edit( "F", lua.format( template, foodTemplate ) )
end

local function UpdateButtonG()
  local template = "#showtooltip\n/use [mod:shift,target=player]%s\n/cast [mod:alt]Anti-Magic Shell;Death and Decay"
  macro.edit( "G", lua.format( template, bandage ) )
end

local function UpdateButtonZ()
  macro.edit( "Z", "#showtooltip\n/cast [mod:shift]Strangulate;[mod:alt]Chains of Ice;Mind Freeze" )
end

local function UpdateMount()
  local realZoneText = api.GetRealZoneText()
  local subZoneText = api.GetSubZoneText()
  local flyable = api.UnitLevel( "player" ) >= 77 and (realZoneText ~= "Dalaran" or realZoneText == "Dalaran" and subZoneText == "Krasus' Landing")

  if flyable then
    macro.edit( "Mount", "#showtooltip\n/dismount [mounted]\n/cast [mod:shift]Great Brewfest Kodo;[flyable]Tawny Wind Rider;Great Brewfest Kodo " )
  else
    macro.edit( "Mount", "#showtooltip\n/dismount [mounted]\n/cast [mod:shift]Tawny Wind Rider;Great Brewfest Kodo" )
  end
end

local function UpdateTaunt()
  macro.edit( "Taunt", "#showtooltip\n/startattack [harm]\n/cast [mod:alt]Dark Command;Death Grip" )
end

local function UpdateMacros()
  UpdateButtonF1()
  UpdateButtonF2()
  UpdateButtonF3()
  UpdateButtonF4()
  UpdateButtonF5()
  UpdateButtonF6()
  UpdateButton1()
  UpdateButton2()
  UpdateButton3()
  UpdateButton4()
  UpdateButton5()
  UpdateButtonF()
  UpdateButtonG()
  UpdateButtonZ()
  UpdateMount()
  UpdateTaunt()
end

local function OnFirstEnterWorld()
  macro.create( "1" )
  macro.create( "2" )
  macro.create( "3" )
  macro.create( "4" )
  macro.create( "5" )
  macro.create( "F1" )
  macro.create( "F2" )
  macro.create( "F3" )
  macro.create( "F4" )
  macro.create( "F5" )
  macro.create( "F" )
  macro.create( "G" )
  macro.create( "Z" )
  macro.create( "Mount" )
  macro.create( "Taunt" )
  UpdateMacros()
end

ModUi.DeleteMacros = function()
  macro.delete( "1" )
  macro.delete( "2" )
  macro.delete( "3" )
  macro.delete( "4" )
  macro.delete( "5" )
  macro.delete( "F1" )
  macro.delete( "F2" )
  macro.delete( "F3" )
  macro.delete( "F4" )
  macro.delete( "F5" )
  macro.delete( "Mount" )
  macro.delete( "Taunt" )
  macro.delete( "Shout" )
  macro.delete( "Follow" )
end

local function UpdateQuantityBasedMacros()
  if api.InCombatLockdown() then return end

  UpdateButton1()
  UpdateButtonF()
end

function M.Initialize()
  if M:MyName() ~= "Guildhamster" then
    M.enabled = false
    return
  end

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnAreaChanged( UpdateQuantityBasedMacros )
  M:OnRegenEnabled( UpdateQuantityBasedMacros )
  M:OnBagUpdate( UpdateQuantityBasedMacros )
  M:OnZoneChanged( UpdateMount )
  M:OnAreaChanged( UpdateMount )
end
