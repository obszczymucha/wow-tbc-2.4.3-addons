local M = ModUi:NewModule( "skillincreased" )
local commPrefix = "ModUi-skill"

function OnSkillIncreased( message )
	for skill, level in (message):gmatch "Your skill in ([%w%-%s]+) has increased to (%d+)." do
		if not IsInGroup() then return end

		local sex = M:MySex()
		local pronoun = sex == 1 and "their" or sex == 2 and "his" or "her"
		ModUi:SendCommMessage( commPrefix, format( "%s increased %s skill in %s to %d.", M:MyName(), pronoun, skill, level ), M:GetGroupChatType() )
	end
end

local function PlayDing()
	PlaySoundFile( "Interface\\AddOns\\Music\\AuctionSold.mp3", "Master" )
end

local function OnSkillComm( prefix, message )
	for player in (message):gmatch "(%w+) increased .+." do
		if player == M:MyName() then return end
		M:Print( format( "|cff5858ef%s|r", message ) )
		PlayDing()
	end
end

local function Init()
	ModUi:RegisterComm( commPrefix, OnSkillComm )
end

function M.Initialize()
	Init()
	M:OnSkillIncreased( OnSkillIncreased )
end
