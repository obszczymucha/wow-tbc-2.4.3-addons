local M = ModUi:NewModule( "moveableframes" )
local api = ModUi.facade.api

local function on_first_enter_world()
  --create_macro_frame_anchor()
  ---@diagnostic disable-next-line: undefined-global
  M.EnableDrag( InterfaceOptionsFrame )
  ---@diagnostic disable-next-line: undefined-global
  M.EnableDrag( GameMenuFrame )
  ---@diagnostic disable-next-line: undefined-global

  --MacroFrame is created the first time it's accessed.
  --For now use /script EnableDrag(MacroFrame)
  --enable_drag( MacroFrame )
end

function M.Initialize()
  M:OnFirstEnterWorld( on_first_enter_world )
end
