local M = ModUi:NewModule( "battlefieldminimapnodes", { "FarmingMode" } )
local Astrolabe
local OverlayParentFrame
local Overlays = {}

local function PositionOverlay( frame )
  if not BattlefieldMinimap then return end

  frame:ClearAllPoints()
  frame:SetWidth( BattlefieldMinimap:GetWidth() + 10 )
  frame:SetHeight( BattlefieldMinimap:GetHeight() + 5 )
  frame:SetPoint( "TOPLEFT", BattlefieldMinimap, "TOPLEFT", 0, 0 )
end

local function GetNoteObject( noteNumber )
  local button = getglobal( "GatherBattleMain" .. noteNumber )
  if not (button) then
    local overlayFrameNumber = math.ceil( noteNumber / 100 )
    local overlayFrame = Overlays[ overlayFrameNumber ]
    if not (overlayFrame) then
      overlayFrame = CreateFrame( "Frame", "GathererBattlemapOverlayParentFrame" .. overlayFrameNumber, OverlayParentFrame )
      PositionOverlay( overlayFrame )
      Overlays[ overlayFrameNumber ] = overlayFrame
    end
    button = CreateFrame( "Button", "GatherBattleMain" .. noteNumber, overlayFrame, "GatherMainTemplate" )
    button:SetFrameStrata( "BACKGROUND" )
    button:SetID( noteNumber )
    overlayFrame[ (noteNumber - 1) % 100 + 1 ] = button
    --Gatherer.Util.Debug("create id "..noteNumber.." frame ".. overlayFrameNumber)
  end
  return button
end

local function Draw()
  local GathererMapOverlayParent = OverlayParentFrame
  if not (GathererMapOverlayParent:IsVisible()) then
    return
  end
  local setting = Gatherer.Config.GetSetting
  local maxNotes = setting( "mainmap.count", 600 )
  local noteCount = 0

  local isBlastedLands = GetRealZoneText() == "Blasted Lands"

  -- prevent the function from running twice at the same time.
  if (Gatherer.Var.UpdateBattlefieldMinimap == 0) then return; end
  Gatherer.Var.UpdateBattlefieldMinimap = 0

  local showType, showObject
  local mapContinent = GetCurrentMapContinent()
  local mapZone = GetCurrentMapZone()
  if (Gatherer.Storage.HasDataOnZone( mapContinent, mapZone )) then
    for nodeId, gatherType, num in Gatherer.Storage.ZoneGatherNames( mapContinent, mapZone ) do
      if (Gatherer.Config.DisplayFilter_MainMap( nodeId )) then
        for index, xPos, yPos, count in Gatherer.Storage.ZoneGatherNodes( mapContinent, mapZone, nodeId ) do
          if (noteCount < maxNotes) then
            noteCount = noteCount + 1
            local mainNote = GetNoteObject( noteCount )
            local iconsize = 6


            if isBlastedLands then
              mainNote:SetAlpha( 1 )
              iconsize = 7
            else
              mainNote:SetAlpha( 0.3 )
            end

            local texture = Gatherer.Util.GetNodeTexture( nodeId )
            getglobal( mainNote:GetName() .. "Texture" ):SetTexture( texture )

            mainNote:SetWidth( iconsize )
            mainNote:SetHeight( iconsize )

            mainNote.continent = mapContinent
            mainNote.zone = mapZone
            mainNote.id = nodeId
            mainNote.index = index

            local tooltip = false
            if (tooltip and not mainNote:IsMouseEnabled()) then
              mainNote:EnableMouse( true )
            elseif (not tooltip and mainNote:IsMouseEnabled()) then
              mainNote:EnableMouse( false )
            end

            Astrolabe:PlaceIconOnWorldMap( OverlayParentFrame, mainNote, mapContinent, mapZone, xPos, yPos )
          else -- reached note limit
            break
          end
        end
      end
    end
  end


  numUsedOverlays = math.ceil( noteCount / 100 )
  local partialOverlay = Overlays[ numUsedOverlays ]
  for i = (noteCount - ((numUsedOverlays - 1) * 100) + 1), 100 do
    local note = partialOverlay[ i ]
    if not (note) then
      break
    end
    note:Hide()
  end
  for i, overlay in ipairs( Overlays ) do
    if (i <= numUsedOverlays) then
      overlay:Show()
    else
      overlay:Hide()
    end
  end

  Gatherer.Var.UpdateBattlefieldMinimap = -1
end

local function OnFirstEnterWorld()
  Astrolabe = DongleStub( Gatherer.AstrolabeVersion )
  OverlayParentFrame = CreateFrame( "FRAME", "BattlefieldMinimapNodesFrame", UIParent )
end

local function OnEnterWorld()
  PositionOverlay( OverlayParentFrame )
  OverlayParentFrame:Show()
end

local function Hide()
  OverlayParentFrame:Hide()

  if not numUsedOverlays then return end

  for i = 1, numUsedOverlays do
    if Overlays[ i ] then Overlays[ i ]:Hide() end
  end
end

local function OnFarmingMode()
  if M:IsFarmingMode() and not UnitAffectingCombat( "player" ) then
    OverlayParentFrame:Show()
    Draw()
  else
    Hide()
  end
end

function M.Initialize()
  if not Gatherer then
    M.enabled = false
    M:DebugMsg( "Disabling. Gatherer not found." )
    return
  end

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( OnEnterWorld )
  M:OnFarmingMode( OnFarmingMode )
  M:OnGasExtracted( function() ModUi:ScheduleTimer( OnFarmingMode, 1 ) end )
  M:OnEnterCombat( Hide )
  M:OnRegenDisabled( Hide )
  M:OnLeaveCombat( OnFarmingMode )
  M:OnRegenEnabled( OnFarmingMode )
  M:OnAreaChanged( function() ModUi:ScheduleTimer( OnFarmingMode, 1 ) end )
end
