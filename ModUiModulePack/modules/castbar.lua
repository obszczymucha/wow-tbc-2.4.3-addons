local M = ModUi:NewModule( "castbar", { "ShadowUFTarget", "FarmingMode" } )

function CenterPoint( y )
	return { "TOP", UIParent, "TOP", 0, y }
end

local function UnderShoutIndicators()
	M:MoveFrameByPoint( QuartzCastBar, CenterPoint( -234 ) )
end

local function Top()
	M:MoveFrameByPoint( QuartzCastBar, CenterPoint( -20 ) )
end

local function Targetting()
	if M:IsTargetOfTarget() then
		UnderShoutIndicators()
	else
		Top()
	end
end		

local function UnderMinimap()
	M:MoveFrameByPoint( QuartzCastBar, CenterPoint( -140 ) )
end

local function OverFarmingModeMinimap()
	M:MoveFrameByPoint( QuartzCastBar, { "TOP", MinimapCluster, "TOP", 10, 28 } )
end

local function OnFarmingMode()
	if not M:IsInCombat() and M:IsFarmingMode() then
		OverFarmingModeMinimap()
	elseif M:IsTargetting() then
		Targetting()
	else
		UnderMinimap()
	end
end

local function TargetOfTargetLost()
	if M:IsTargetting() then
		Top()
	else
		UnderMinimap()
	end
end

local function OnEnterWorld()
	OnFarmingMode()

	M:OnTargetAcquired( OnFarmingMode )
	M:OnTargetLost( OnFarmingMode )
	M:OnTargetOfTargetAcquired( OnFarmingMode )
	M:OnTargetOfTargetLost( TargetOfTargetLost )
end

function M.Initialize()
	M:OnEnterWorld( OnEnterWorld )
	M:OnEnterCombat( UnderShoutIndicators )
	M:OnRegenDisabled( UnderShoutIndicators )
	M:OnLeaveCombat( OnFarmingMode )
	M:OnRegenEnabled( OnFarmingMode )
	M:OnFarmingMode( OnFarmingMode )
end
