local M = ModUi:NewModule( "trackitems", { "FarmingMode", "FarmingAnchor" } )

local frame = nil
local trackedItemFrames = {}
local itemCount = 0
local trackedItems = nil
local trackedItemsCount = 0

local function GetAnchor()
  --if herbCountFrame and herbCountFrame:IsVisible() then
    --return herbCountFrame
  if SUFHeaderparty and SUFHeaderparty:IsVisible() then
    return SUFHeaderparty
  elseif SUFUnitplayer and SUFUnitplayer:IsVisible() then
    return SUFUnitplayer
  else
    return UIParent
  end
end

local function PositionFrame()
  if M:IsFarmingMode() then
    M:MoveFrameByPoint( frame, { "TOPLEFT", BattlefieldMinimap, "BOTTOMLEFT", 0, -5 } )
  else
    M:MoveFrameByPoint( frame, { "TOPRIGHT", GetAnchor(), "BOTTOMRIGHT", 0, -20 } )
  end
end

local function GetTrackedItems()
  return ModUiDb.trackitems.trackedItems
end

local function SetTrackedItems( trackedItems )
  ModUiDb.trackitems.trackedItems = trackedItems
end

local function IsVisible()
  return ModUiDb.trackitems.visible and frame:IsVisible()
end

local function SetVisible( visible )
  ModUiDb.trackitems.visible = visible

  if IsVisible() then
    M:Show( frame )
    PositionFrame()
  else
    M:Hide( frame )
  end
end

local function ParseItem( item )
  for colour, id, name in (item):gmatch "|(.+)|Hitem:(%d+):.+%[(.+)%]" do
    return colour, id, name
  end

  return "", 0, "Unknown"
end

local function arrayContains( array, item )
  for i = 1, table.getn( array ) do
    if array[ i ] == item then
      return true
    end
  end

  return false
end

local function CreateTrackingItemFrame( parent, itemLink )
  itemCount = itemCount + 1

  parent:SetWidth( 150 )
  parent:SetHeight( itemCount * 15 + 5 )

  local colour, itemId, itemName = ParseItem( itemLink )
  local frameName = "ItemTrackFrameItem" .. itemCount
  local frame = CreateFrame( "frame", frameName, parent )
  frame.itemLink = itemLink
  frame.itemName = format( "|%s%s|r", colour, itemName )
  frame.itemId = itemId
  frame:SetWidth( 12 )
  frame:SetHeight( 12 )

  local icon = CreateFrame( "frame", "icon", frame )
  icon:SetHeight( 12 )
  icon:SetWidth( 12)
  icon:SetPoint( "TOPRIGHT", 0, 0 )

  local texture = icon:CreateTexture( nil, "BACKGROUND" )
  texture:SetTexture( GetItemIcon( itemId ) )
  texture:SetAllPoints( icon )
  icon.texture = texture

  local label = frame:CreateFontString( nil, "OVERLAY" )
  label:SetFont( "FONTS\\FRIZQT__.TTF", 10, "OUTLINE" )
  frame:SetPoint( "TOPLEFT", 5, 10 - itemCount * 15 )
  label:SetPoint( "TOPRIGHT", -20, -1 )
  label:SetText( frame.itemName )

  frame.label = label
  frame.tracked = true
  frame:Show()

  table.insert( trackedItemFrames, frame )
end

local function CreateItemTrackFrame()
  local frame = CreateFrame( "frame", "ItemTrackFrame", UIParent, BackdropTemplateMixin and "BackdropTemplate" )
  frame:SetBackdrop( {
    bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
    edgeFile = "Interface\\ChatFrame\\ChatFrameBackground",
    tile = false,
    tileSize = 0,
    edgeSize = 0,
  } )

  frame:SetBackdropColor( 0.1, 0.1, 0.1, 0 )
  frame:SetBackdropBorderColor( 0.1, 0.1, 0.1, 0 )
  frame:SetFrameStrata( "LOW" )
  frame:SetMovable( false )
  frame:EnableMouse( false )
  frame:RegisterForDrag( "LeftButton" )
  frame:SetScript( "OnDragStart", frame.StartMoving )
  frame:SetScript( "OnDragStop", frame.StopMovingOrSizing )
  frame:SetClampedToScreen( false )

  return frame
end

local function OnBagUpdate()
  local visibleFrames = 0
  trackedItemsCount = 0

  for i = 1, #trackedItemFrames do
    local frame = trackedItemFrames[ i ]
    local count = M:CountItemsByLink( frame.itemLink )

    frame.label:SetText( format( "%s: %d", frame.itemName, count ) )

    if count == 0 then
      frame:Hide()
    elseif frame.tracked then
      trackedItemsCount = trackedItemsCount + count
      frame:Show()
      visibleFrames = visibleFrames + 1
      frame:SetPoint( "TOPLEFT", 5, 10 - visibleFrames * 15 )
    end
  end
end

local function GetItemFrame( itemLink )
  for i = 1, #trackedItemFrames do
    local frame = trackedItemFrames[ i ]
    if frame.itemLink == itemLink then
      return frame
    end
  end

  return nil
end

local function Untrack( itemLink, dontUpdate )
  local itemFrame = GetItemFrame( itemLink )

  if itemFrame then
    itemFrame.tracked = false
    itemFrame:Hide()
  end

  for i = 1, #trackedItems do
    if trackedItems[ i ] == itemLink then
      table.remove( trackedItems, i )
    end
  end

  SetTrackedItems( trackedItems )
  OnBagUpdate()
end

local function Track( itemLink, load )
  local itemFrame = GetItemFrame( itemLink )

  if not itemFrame then
    CreateTrackingItemFrame( frame, itemLink )
  elseif itemFrame and itemFrame.tracked and not load then
    Untrack( itemLink )
    return
  else
    itemFrame.tracked = true
    itemFrame:Show()
  end

  OnBagUpdate()

  if load then return end

  table.insert( trackedItems, itemLink )
  SetTrackedItems( trackedItems )
  SetVisible( true )
end

local function UntrackAll()
  for i = 1, #trackedItemFrames do
    local frame = trackedItemFrames[ i ]
    frame.tracked = false
    frame:Hide()
  end

  for i = 1, #trackedItems do
    table.remove( trackedItems, i )
  end

  SetTrackedItems( trackedItems )
  trackedItemsCount = 0
end

local function Toggle()
  if trackedItemsCount == 0 then
    SetVisible( true )
    M:PrettyPrint( "No items are being tracked." )
    return
  end

  SetVisible( not IsVisible() )
end

local function PrintUsage( command )
  M:Print( format( "|cffefefef%s|r |cffff9f69%s|r |cfffafafa%s|r", "Usage:", command, "<item>" ) )
end

local function ProcessTrackSlashCommand( args )
  if args and strlower( args ) == "clear" then
    UntrackAll()
    return
  end

  for item in (args):gmatch "(|%w+|Hitem.+|r)" do
    Track( item )
    return
  end

  Toggle()
end

local function ProcessUntrackSlashCommand( args )
  for item in (args):gmatch "(|%w+|Hitem.+|r)" do
    Untrack( item )
    return
  end

  PrintUsage( "/untrack" )
end

local function SetupStorage()
  if not ModUiDb.trackitems then
    ModUiDb.trackitems = {}
    SetVisible( true )
  end

  if not GetTrackedItems() then
    SetTrackedItems( {} )
  end

  trackedItems = GetTrackedItems()
  itemCount = #trackedItems
end

local function LoadItems()
  local items = trackedItems

  for i = 1, #items do
    Track( items[ i ], true )
  end
end

local function OnFirstEnterWorld()
  SLASH_TRACK1 = "/track"
  SlashCmdList[ "TRACK" ] = ProcessTrackSlashCommand
  SLASH_UNTRACK1 = "/untrack"
  SlashCmdList[ "UNTRACK" ] = ProcessUntrackSlashCommand
  SetupStorage()

  frame = CreateItemTrackFrame()
  SetVisible( IsVisible() )
  LoadItems()
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnBagUpdate( OnBagUpdate )
  M:OnGroupChanged( PositionFrame )
  M:OnFarmingMode( PositionFrame )
end
