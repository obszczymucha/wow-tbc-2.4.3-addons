local M = ModUi:NewModule( "music" )
local api = ModUi.facade.api
local lua = ModUi.facade.lua

local timerId
local playing = false
local playlist
local ready_to_play = false

local silence = "Interface\\AddOns\\Music\\silence.mp3"

local playlists = {
  elwynn = {
    name = "Elwynn Forest",
    tracks = {
      { "Interface\\AddOns\\Music\\Forest\\DayForest01.mp3", 55 },
      { "Interface\\AddOns\\Music\\Forest\\DayForest02.mp3", 72 },
      { "Interface\\AddOns\\Music\\Forest\\DayForest03.mp3", 64 }
    }
  },
  default = {
    name = "Default",
    tracks = {
      { "Interface\\AddOns\\Music\\Forest\\DayForest01.mp3", 55 },
      { "Interface\\AddOns\\Music\\Forest\\DayForest02.mp3", 72 },
      { "Interface\\AddOns\\Music\\Forest\\DayForest03.mp3", 64 },
      { "Interface\\AddOns\\Music\\Zangarmarsh\\ZA_GeneralWalkUni01.mp3", 81 },
      { "Interface\\AddOns\\Music\\Zangarmarsh\\ZA_GeneralWalkUni02.mp3", 120 },
      { "Interface\\AddOns\\Music\\Zangarmarsh\\ZA_GeneralWalkUni03.mp3", 59 },
      { "Interface\\AddOns\\Music\\Zangarmarsh\\ZA_GeneralWalkUni04.mp3", 102 },
      { "Interface\\AddOns\\Music\\Zangarmarsh\\ZA_GeneralWalkUni05.mp3", 71 },
      { "Interface\\AddOns\\Music\\Zangarmarsh\\ZA_GeneralWalkUni06.mp3", 89 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni01.mp3", 206 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni02.mp3", 124 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni02r.mp3", 129 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni03.mp3", 187 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni03r.mp3", 188 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni04.mp3", 158 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni05.mp3", 191 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni06.mp3", 110 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni07r.mp3", 110 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_DraeneiWalkUni08r.mp3", 99 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_ExodarWalkUni01.mp3", 109 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_ExodarWalkUni02.mp3", 107 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_ExodarWalkUni03.mp3", 93 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_NagaWalkUni01.mp3", 103 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_NagaWalkUni02.mp3", 74 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_NagaWalkUni03.mp3", 149 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_NagaWalkUni04.mp3", 73 },
      { "Interface\\AddOns\\Music\\Azuremyst\\AI_NagaWalkUni05.mp3", 98 },
      { "Interface\\AddOns\\Music\\Auchindoun\\TF_AuchindounWalkUni01.mp3", 120 },
      { "Interface\\AddOns\\Music\\Auchindoun\\TF_AuchindounWalkUni02.mp3", 150 },
      { "Interface\\AddOns\\Music\\Auchindoun\\TF_AuchindounWalkUni03.mp3", 120 }
    }
  },
  dalaran = {
    name = "Dalaran",
    tracks = {
      { "Interface\\AddOns\\Music\\Forest\\DayForest01.mp3", 55 },
      { "Interface\\AddOns\\Music\\Forest\\DayForest02.mp3", 72 },
      { "Interface\\AddOns\\Music\\Forest\\DayForest03.mp3", 64 },
      { "Interface\\AddOns\\Music\\Auchindoun\\TF_AuchindounWalkUni01.mp3", 120 },
      { "Interface\\AddOns\\Music\\Auchindoun\\TF_AuchindounWalkUni02.mp3", 150 },
      { "Interface\\AddOns\\Music\\Auchindoun\\TF_AuchindounWalkUni03.mp3", 120 }
    }
  },
}

local playlistsByZone = {
  [ "Blasted Lands" ] = playlists.default,
  [ "Orgrimmar" ] = playlists.default,
  [ "Dalaran" ] = playlists.dalaran
}

local function cancel_timers()
  ModUi:CancelTimer( timerId )
end

local function Stop()
  if timerId then
    cancel_timers()
    timerId = nil
  end

  if playing then
    api.StopMusic()
    playing = false
  end
end

local function choose_random_track_index()
  if #playlist.tracks == 1 then return 1 end

  local index = nil

  repeat
    index = math.random( #playlist.tracks )
  until index ~= playlist.last_track_index

  return index
end

local function PlayNext()
  if not playlist then return end

  cancel_timers()

  if playing then
    M:DebugMsg( "Playing silence for 5 seconds..." )
    Stop()
    api.PlayMusic( silence )
    timerId = ModUi:ScheduleTimer( PlayNext, 5 )
    return
  end

  local choice = choose_random_track_index()
  playlist.last_track_index = choice
  local mp3, length = unpack( playlist.tracks[ choice ] )

  M:DebugMsg( lua.format( "Playing: %s", mp3 ) )
  Stop()
  api.PlayMusic( mp3 )
  playing = true
  timerId = ModUi:ScheduleTimer( PlayNext, length )
end

local function print_playlist()
  M:PrettyPrint( lua.format( "Playing: %s", playlist.name ) )
end

local function OverrideMusic( playlist_id )
  ModUiDb.music.force = true

  if playlist_id and playlist_id ~= "" then
    playlist = playlists[ playlist_id ]
  end

  if not playlist_id or playlist_id == "" or not playlist then
    playlist = playlists.default
    ModUiDb.music.playlist_id = "default"
  else
    ModUiDb.music.playlist_id = playlist_id
  end

  print_playlist()
  PlayNext()
end

local function AutoPlay()
  if not ready_to_play then return end

  if ModUiDb.music.force and playing then return end
  if ModUiDb.music.force then
    PlayNext()
    return
  end

  local zone = api.GetRealZoneText()
  local playlistByZone = playlistsByZone[ zone ]

  if not playlistsByZone[ zone ] then
    Stop()
    return
  end

  playlist = playlistByZone
  ModUi:ScheduleTimer( PlayNext, 1 )
end

local function init()
  ModUiDb = ModUiDb or {}
  ModUiDb.music = ModUiDb.music or {}

  playlist = playlists[ ModUiDb.music.playlist_id ]

  if playlist and ModUiDb.music.force then
    print_playlist()
  end

  timerId = ModUi:ScheduleTimer( function() ready_to_play = true; AutoPlay() end, 2 )
end

function M.Initialize()
  --	math.randomseed( time() )
  M:OnFirstEnterWorld( init )
  M:OnEnterWorld( AutoPlay )
  M:OnAreaChanged( AutoPlay )

  SLASH_MUSIC1 = "/music"
  api.SlashCmdList[ "MUSIC" ] = OverrideMusic

  SLASH_NEXT1 = "/next"
  api.SlashCmdList[ "NEXT" ] = function() playing = false; PlayNext() end

  SLASH_STOP1 = "/stop"
  api.SlashCmdList[ "STOP" ] = function() ModUiDb.music.force = false; Stop() end
end
