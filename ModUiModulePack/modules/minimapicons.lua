---@diagnostic disable: undefined-global
local M = ModUi:NewModule( "minimapicons", { "FarmingMode" } )

local function Toggle()
  if M:IsFarmingMode() then
    M:Hide( AtlasButtonFrame )
    M:Hide( OutfitterMinimapButton )
    M:Hide( Gatherer_MinimapOptionsButton )
    M:Hide( Bongos3MinimapButton )
    M:Hide( MiniMapMailFrame )
    GameTimeFrame:SetScale( 0.6 )
    M:MoveFrameByPoint( GameTimeFrame, { "TOPLEFT", MinimapCluster, "TOPRIGHT", -66, -22 } )
    M:Hide( FuBarPluginBugSackFrameMinimapButton )
  else
    M:Show( AtlasButtonFrame )
    M:Show( OutfitterMinimapButton )
    M:Show( Gatherer_MinimapOptionsButton )
    M:Show( Bongos3MinimapButton )

    if HasNewMail() then
      M:Show( MiniMapMailFrame )
    end

    GameTimeFrame:SetScale( 0.7 )
    M:MoveFrameByPoint( GameTimeFrame, { "TOPLEFT", MinimapCluster, "TOPRIGHT", -60, -20 } )
    M:MoveFrameByPoint( FuBarPluginBugSackFrameMinimapButton, { "TOPLEFT", MinimapCluster, "TOPRIGHT", -40, -44 } )
    M:MoveFrameByPoint( RBSMinimapButton, { "TOPLEFT", MinimapCluster, "TOPRIGHT", -40, -71 } )
    M:MoveFrameByPoint( RollForMinimapButton, { "TOPLEFT", MinimapCluster, "TOPRIGHT", -40, -98 } )
    M:MoveFrameByPoint( pfBrowserIcon, { "TOPLEFT", MinimapCluster, "TOPRIGHT", -40, -125 } )
    M:Show( FuBarPluginBugSackFrameMinimapButton )
  end
end

local function OnFirstEnterWorld()
  M:MoveFrameByPoint( AtlasButtonFrame, { "TOPLEFT", MinimapCluster, "TOPLEFT", 12, -20 } )
  M:MoveFrameByPoint( OutfitterMinimapButton, { "TOPLEFT", MinimapCluster, "TOPLEFT", 12, -47 } )
  M:MoveFrameByPoint( Gatherer_MinimapOptionsButton, { "TOPLEFT", MinimapCluster, "TOPLEFT", 13.5, -80 } )
  if Gatherer_MinimapOptionsButton then Gatherer_MinimapOptionsButton:SetScale( 0.95 ) end
  M:MoveFrameByPoint( Bongos3MinimapButton, { "TOPLEFT", MinimapCluster, "TOPLEFT", 12, -105 } )
  M:MoveFrameByPoint( MiniMapMailFrame, { "TOPLEFT", MinimapCluster, "TOPLEFT", 12, -133 } )
  M:Hide( FuBarPluginBugSackFrameMinimapButton )
  M:Hide( TimeManagerClockButton )
end

function M.Initialize()
  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnEnterWorld( Toggle )
  M:OnFarmingMode( Toggle )
  M:OnPendingMail( Toggle )
end
