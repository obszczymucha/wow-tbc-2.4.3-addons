local M = ModUi:NewModule( "auctionnotify" )

local function OnChatMsgSystem( message )
	local match = message:match( "A buyer has been found for your auction of (.*)%.")

	if match == nil then
		return
	end

	SendChatMessage( format( "Sold %s.", match ), "WHISPER", nil, "Obszczymucha" )
end

function M.Initialize()
	if not M:IsPlayerName( "Ohhimark" ) then
		M.enabled = false
		return
	end

	M:OnChatMsgSystem( OnChatMsgSystem )
end
