local M = ModUi:NewModule( "interrupted" )
local api = ModUi.facade.api

---@diagnostic disable-next-line: unused-local
local function OnEvent( _, event, _, sourceName, _, _, destName, _, _, spellName, _, extraSpellId, extraSpellName )
  if event ~= "SPELL_INTERRUPT" or sourceName ~= M:MyName() then return end
  local link = api.GetSpellLink( extraSpellId )
  api.SendChatMessage( string.format( "Interrupted %s's %s.", destName, link or extraSpellName ) )
end

function M.Initialize()
  M:OnCombatLogEventUnfiltered( OnEvent )
end
