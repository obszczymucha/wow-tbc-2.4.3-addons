local M = ModUi:NewModule( "threatmeter" )

local frame

local function On()
	frame:SetAlpha( 1 )
end

local function Off()
	frame:SetAlpha( 0 )
end

local function Initialize()
	frame = ThreatClassic2BarFrame
	local playerFrame = SUFUnitplayer

	if not frame or not playerFrame or not Recount_MainWindow then
		M.enabled = false
		return
	end

	if M:MyName() == "Obszczymucha" then
		--M:MoveFrameByPoint( frame, { "TOPLEFT", ChatFrame1, "BOTTOMLEFT", 0, -80 } )
		M:MoveFrameByPoint( frame, { "TOPRIGHT", Recount_MainWindow, "TOPLEFT", -20, -28 } )
	else	
		M:MoveFrameByPoint( frame, { "TOPRIGHT", playerFrame, "BOTTOMRIGHT", -3, -20 } )
	end

	if InCombatLockdown() then
		On()
	else
		Off()
	end
end

function M.Initialize()
	M:OnFirstEnterWorld( Initialize )
	M:OnEnterCombat( On )
	M:OnRegenDisabled( On )
	M:OnLeaveCombat( Off )
	M:OnRegenEnabled( Off )
end
