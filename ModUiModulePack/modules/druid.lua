-- TODO:
-- Add consume macro.
-- Create Thorns for tank missing aura.

local M = ModUi:NewModule( "druid", { "Class", "TankTarget" } )
local api = ModUi.facade.api
local lua = ModUi.facade.lua
local macro = ModUi.facade.macro

local ALT = macro.modifiers.ALT
local CTRL = macro.modifiers.CTRL
local SHIFT = macro.modifiers.SHIFT
local ALT_CTRL_SHIFT = macro.modifiers.ALT_CTRL_SHIFT
local NOMOD = macro.modifiers.NOMOD
local TARGET_PLAYER = macro.modifiers.TARGET_PLAYER
local HARM = macro.modifiers.HARM
local NOHARM = macro.modifiers.NOHARM
local NODEAD = macro.modifiers.NODEAD
local FLYABLE = macro.modifiers.FLYABLE
local NOFLYABLE = macro.modifiers.NOFLYABLE
local COMBAT = macro.modifiers.COMBAT
local NOCOMBAT = macro.modifiers.NOCOMBAT

local DruidSpec = {
  Balance = "Balance",
  Feral = "Feral",
  Resto = "Resto"
}

local function parse_form_numbers( ... )
  if select( "#", ... ) == 0 then error( "No form numbers provided." ) end
  return table.concat( { ... }, "/" )
end

local function form( ... )
  return string.format( "form:%s", parse_form_numbers( ... ) )
end

local function noform( ... )
  if select( "#", ... ) == 0 then return "noform" end
  return string.format( "noform:%s", parse_form_numbers( ... ) )
end

local BEAR = "1"
--local AQUATIC = "2"
local CAT = "3"
--local TRAVEL = "4"
local TREE = "5"
--local FLIGHT = "6"
--local SWIFT_FLIGHT = "7"

---@diagnostic disable-next-line: unused-local
local balance = false
---@diagnostic disable-next-line: unused-local
local feral = false
local resto = false

local function set_helper_variables( spec )
  if spec == DruidSpec.Balance then
    ---@diagnostic disable-next-line: unused-local
    balance = true; feral = false; resto = false;
  elseif spec == DruidSpec.Feral then
    ---@diagnostic disable-next-line: unused-local
    balance = false; feral = true; resto = false;
  elseif spec == DruidSpec.Resto then
    ---@diagnostic disable-next-line: unused-local
    balance = false; feral = false; resto = true;
  else
    error( string.format( "Unsupported DruidSpec: %s", spec ) )
  end
end

local function determine_spec( callback )
  local name, _, _, _, points = api.GetTalentInfo( 3, 20 )
  if name == "Tree of Life" and points == 1 then
    return DruidSpec.Resto
  end

  name, _, _, _, points = api.GetTalentInfo( 2, 21 )
  if name == "Mangle" and points == 1 then
    return DruidSpec.Feral
  end

  name, _, _, _, points = api.GetTalentInfo( 1, 21 )
  if name == "Force of Nature" and points == 1 then
    return DruidSpec.Balance
  end

  -- At this point it means we couldn't determine the spec.
  -- It happens when we log in sometimes.
  -- Let's try again in 0.5 second.
  M:ScheduleTimer( function()
    local spec = determine_spec( function()
      error( "Couldn't determine warrior's spec!" )
    end )

    if spec ~= nil then callback() end
  end, 0.5 )

  return nil
end

local function TankTargetModifier( modifiers )
  local tank_target = M:GetTankTarget()

  if tank_target then
    return lua.format( "%starget=%s", modifiers and lua.format( "%s,", modifiers ) or "", tank_target )
  else
    return modifiers and lua.format( "%s", modifiers ) or nil
  end
end

local function UpdateButtonF1()
  local name = "F1"

  if resto then
    macro:tooltip()
        :cast( "Entangling Roots", ALT )
        :cast( "Cat Form", noform( CAT ) )
        :cast( "Dash", form( CAT ) )
        :build( name )
  else
    macro:tooltip()
        :startattack()
        :cast( "Cat Form", noform( CAT ) )
        :cast( "Dash", form( CAT ) )
        :cast( "Frenzied Regeneration", form( BEAR ), ALT )
        :cast( "Berserk" )
        :build( name )
  end
end

local function UpdateButtonF2()
  local name = "F2"

  if resto then
    macro:tooltip()
        :cast( "Hibernate", ALT )
        :cast( "Barkskin" )
        :build( name )
  else
    macro:tooltip()
        :cast( "Barkskin", ALT )
        :cast( "Frenzied Regeneration", form( BEAR ) )
        :cast( "Tiger's Fury", form( CAT ) )
        :cast( "Barkskin" )
        :build( name )
  end
end

local function UpdateButtonF3()
  macro:tooltip()
      :cast( "Innervate", ALT, TARGET_PLAYER )
      :cast( "Innervate", SHIFT, NOHARM, NODEAD )
      :cast( "War Stomp" )
      :build( "F3" )
end

local function UpdateButtonF5()
  macro:tooltip()
      :cast( "Mark of the Wild", SHIFT )
      :cast( "Mark of the Wild", ALT, TARGET_PLAYER )
      :cast( "Gift of the Wild" )
      :build( "F5" )
end

local function UpdateButtonF6()
  macro:tooltip()
      :cast( "Tranquility" )
      :build( "F6" )
end

-- name, id1, id2, ...
local count = function( name, ... )
  local total = 0
  for _, id in ipairs( { ... } ) do total = total + M:CountItemsById( id ) end

  return total > 0 and name or nil
end

local function UpdateButtonZ()
  local consume =
      count( "Conjured Manna Biscuit", 34062 ) or
      count( "Conjured Mountain Spring Water", 30703 ) or
      count( "Conjured Glacier Water", 22018 ) or
      count( "Purified Draenic Water", 27860 ) or
      count( "Filtered Draenic Water", 28399 )

  macro:tooltip()
      :cast( "Omen of Clarity", ALT )
      :use( "Heavy Netherweave Bandage", ALT, TARGET_PLAYER )
      :use( "Heavy Netherweave Bandage", CTRL )
      :use( consume, SHIFT )
      :use( "Fel Iron Bomb" )
      :build( "Z" )
end

local function UpdateButtonF()
  local name = "F"

  if resto then
    macro:tooltip()
        :cast( "Hurricane", SHIFT )
        :cast( "Cyclone", ALT )
        :cast( "Faerie Fire" )
        :build( name )
  else
    macro:tooltip()
        :cast( "Faerie Fire (Feral)()", form( BEAR, CAT ) )
        :cast( "Faerie Fire" )
        :build( name )
  end
end

local function UpdateButtonR()
  if resto then
    macro:tooltip()
        :cast( "Remove Curse", TankTargetModifier( ALT ) )
        :cast( "Omen of Clarity", TankTargetModifier( SHIFT ) )
        :cast( "Abolish Poison", TankTargetModifier() )
        :build( "R" )
  else
    macro:tooltip()
        :cast( "Feral Charge", form( BEAR, CAT ) )
        :cast( "Faerie Fire" )
        :build( "R" )
  end
end

local function UpdateButtonG()
  local name = "G"

  if resto then
    macro:tooltip()
        :cast( "Thorns", SHIFT )
        :cast( "Thorns", TankTargetModifier() )
        :build( name )
  else
    macro:tooltip()
        :cast( "Thorns", SHIFT )
        :cast( "Thorns", ALT, TARGET_PLAYER )
        :cast( "Enrage", form( BEAR ) )
        :cast( "Thorns" )
        :build( name )
  end
end

local function UpdateMount()
  macro:tooltip()
      :cast( "Swift Flight Form", NOMOD, FLYABLE )
      :cancelform( NOMOD )
      :use( "Great Brown Kodo", SHIFT )
      :use( "Great Brown Kodo", NOMOD, NOFLYABLE )
      :build( "Mount" )
end

local function UpdateButton1()
  local name = "1"
  local potion = count( "Super Mana Potion", 22832 )

  if resto then
    macro:tooltip()
        :cast( "Cat Form", ALT_CTRL_SHIFT )
        :use( "13", CTRL )
        :use( potion, SHIFT )
        :cast( "Lifebloom", ALT, TARGET_PLAYER )
        :cast( "Claw", form( CAT ) )
        :cast( "Lacerate", form( BEAR ) )
        :cast( "Lifebloom", TankTargetModifier() )
        :use( "13" )
        :global()
        :build( name )
  else
    macro:tooltip()
        :startattack()
        :cast( "Cat Form", ALT )
        :cast( "Maul", form( BEAR ) )
        :cast( "Rip", form( CAT ) )
        :cast( "Lifebloom", ALT, TARGET_PLAYER )
        :cast( "Lifebloom", TankTargetModifier() )
        :global()
        :build( name )
  end
end

local function UpdateButton2()
  local name = "2"

  if resto then
    macro:tooltip()
        :cast( "Dire Bear Form", ALT_CTRL_SHIFT )
        :use( "14", CTRL )
        :cast( "Rejuvenation", ALT, TARGET_PLAYER )
        :cast( "Rejuvenation", TankTargetModifier() )
        :global()
        :build( name )
  else
    macro:tooltip()
        :startattack()
        :use( "14", CTRL )
        :cast( "Dire Bear Form", ALT )
        :cast( "Lacerate", form( BEAR ) )
        :cast( "Shred", form( CAT ) )
        :cast( "Rejuvenation", TankTargetModifier() )
        :global()
        :build( name )
  end
end

local function UpdateButton3()
  local name = "3"

  if resto then
    macro:tooltip()
        :cast( "Aquatic Form", ALT_CTRL_SHIFT )
        :cast( "Tree of Life", ALT )
        :castsequence( "Nature's Swiftness, Healing Touch", "5", TankTargetModifier( form( TREE ) ) )
        :cast( "Tree of Life", noform( TREE ), NOMOD )
        :global()
        :build( name )
  else
    macro:tooltip()
        :startattack( form( BEAR, CAT ) )
        :cast( "Swipe", form( BEAR ) )
        :cast( "Ferocious Bite", form( CAT ) )
        :cast( "Healing Touch", TankTargetModifier() )
        :global()
        :build( name )
  end
end

local function UpdateButton4()
  if resto then
    macro:tooltip()
        :cast( "Bash", form( BEAR ), ALT )
        :cast( "Swipe", form( BEAR ) )
        :cast( "Savage Roar", form( CAT ), COMBAT, HARM, NODEAD )
        :cast( "Regrowth", ALT, TARGET_PLAYER )
        :cast( "Regrowth", TankTargetModifier() )
        :use( "14" )
        :global()
        :build( "4" )
  else
    macro:tooltip()
        :startattack()
        :cast( "Mangle (Bear)()", form( BEAR ) )
        :cast( "Mangle (Cat)()", form( CAT ) )
        :cast( "Regrowth", ALT, TARGET_PLAYER )
        :cast( "Regrowth", TankTargetModifier() )
        :global()
        :build( "4" )
  end
end

local function UpdateButton5()
  local name = "5"

  if resto then
    macro:tooltip()
        :cast( "Swiftmend", ALT, TARGET_PLAYER )
        :cast( "Swiftmend", TankTargetModifier( form( TREE ) ) )
        :cast( "Bash", form( BEAR ) )
        :cast( "Moonfire" )
        :global()
        :build( name )
  else
    macro:tooltip()
        :cast( "Demoralizing Roar", form( BEAR ) )
        :cast( "Maim", form( CAT ) )
        :cast( "Hurricane" )
        :global()
        :build( name )
  end
end

local function UpdateButton6()
  local name = "6"
  local spell = "Healing Touch"

  if resto then
    macro:tooltip( spell )
        :cast( spell, ALT, TARGET_PLAYER )
        :cast( spell, TankTargetModifier() )
        :global()
        :build( name )
  else
    macro:tooltip()
        :cast( "Bash", form( BEAR ) )
        :cast( "Moonfire" )
        :global()
        :build( name )
  end
end

local function UpdateTankTarget()
  local tankTarget = M:GetTankTarget()

  if tankTarget then
    macro.edit( "TT", lua.format( "/target %s\n/assist [nomod]", tankTarget ), false, 125 )
  else
    macro.edit( "TT", "/script local m = ModUi:GetExtension( \"TankTarget\" ); m:PrettyPrint( \"No tank target set.\" ) ", false, 132212 )
  end
end

local function UpdateTaunt()
  if resto then
    macro:tooltip()
        :startattack( form( BEAR ) )
        :cast( "Growl", form( BEAR ) )
        :cast( "Prowl", form( CAT ) )
        :cast( "Faerie Fire", noform() )
        :build( "Taunt" )
  else
    macro:tooltip()
        :startattack()
        :cast( "Dire Bear Form", noform( BEAR ) )
        :cast( "Growl", form( BEAR ) )
        :cast( "Prowl", form( CAT ) )
        :build( "Taunt" )
  end
end

local function show_spec( spec )
  if spec then
    M:PrettyPrint( string.format( "%s macros updated.", spec ) )
  else
    M:PrettyPrint( "Could not determine the spec." )
  end
end

local function UpdateAllMacros()
  if api.InCombatLockdown() then return end

  local spec = determine_spec( UpdateAllMacros )
  if not spec then return end

  set_helper_variables( spec )

  UpdateButtonF1()
  UpdateButtonF2()
  UpdateButtonF3()
  UpdateButtonF5()
  UpdateButtonF6()
  UpdateButtonZ()
  UpdateButtonF()
  UpdateButtonR()
  UpdateButtonG()
  UpdateMount()
  UpdateButton1()
  UpdateButton2()
  UpdateButton3()
  UpdateButton4()
  UpdateButton5()
  UpdateButton6()
  UpdateTaunt()
  UpdateTankTarget()

  show_spec( spec )
end

local function OnSpecChanged()
  M:ScheduleTimer( function()
    UpdateAllMacros()
  end, 0.2 )
end

local function UpdateConsumableMacros()
  if api.InCombatLockdown() then return end

  UpdateButton1()
  UpdateButton2()
  UpdateButton3()
  UpdateButton4()
  UpdateButton5()
  UpdateButtonZ()
end

function M.Initialize()
  if not M:IsDruid() or api.UnitLevel( "player" ) < 60 then
    M.enabled = false
    return
  end

  SLASH_MAC1 = "/mac"
  api.SlashCmdList[ "MAC" ] = UpdateAllMacros

  M:OnFirstEnterWorld( UpdateAllMacros )
  M:OnRegenEnabled( UpdateConsumableMacros )
  M:OnSpecChanged( OnSpecChanged )
  M:OnBagUpdate( function() M:ScheduleTimer( UpdateConsumableMacros, 1 ) end )
  M:OnTankTarget( UpdateAllMacros )
end
