local M = ModUi:NewModule( "pulltimer" )
local api = ModUi.facade.api

local m_default_seconds = 3
local m_min_seconds = 3
local m_interval = 1.5
local m_group_announce = false

local m_pulling = false
local m_seconds_left

local function OnTimer( chat )
  if not m_pulling or not api.IsInGroup() then return end

  if m_seconds_left > 0 then
    api.SendChatMessage( string.format( "%d", m_seconds_left ), chat )
    m_seconds_left = m_seconds_left - 1
    M:ScheduleTimer( function() OnTimer( chat ) end, m_interval )
    return
  end

  m_pulling = false
end

local function OnCmd( args )
  if m_pulling or not api.IsInGroup() then return end
  m_pulling = true

  local seconds = tonumber( args ) or m_default_seconds
  if seconds < m_min_seconds then seconds = m_min_seconds end

  local chat = not m_group_announce and "SAY" or api.IsInParty() and "PARTY" or "RAID"

  m_seconds_left = seconds - 1
  api.SendChatMessage( string.format( "Pulling in %d...", seconds ), chat )

  M:ScheduleTimer( function() OnTimer( chat ) end, m_interval )
end

function M.Initialize()
  SLASH_PULL1 = "/pull"
  api.SlashCmdList[ "PULL" ] = OnCmd
end
