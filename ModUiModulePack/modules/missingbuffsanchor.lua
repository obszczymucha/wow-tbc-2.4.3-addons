local M = ModUi:NewModule( "missingbuffsanchor", { "FarmingMode", "FarmingAnchor" } )
local frame
local position = { "TOP", "TOP", 0, -290 }

local function Update()
    if M:IsFarmingMode() then
        local scale = 0.5
        local minimapScale = Minimap:GetScale()
        M:MoveFrameByPoint( frame, { "BOTTOM", SUFUnitplayer, "TOP", 0, 80 } )
        frame:SetScale( scale )
    else
        local point, parentPoint, x, y = unpack( position )
        M:MoveFrameByPoint( frame, { point, UIParent, parentPoint, x, y } )
        frame:SetScale( 0.75 )
    end
end

local function CreateAnchor()
    frame = CreateFrame( "FRAME", "MissingBuffsAnchor", UIParent )
--    frame:SetBackdrop({
--        bgFile = "Interface\\ChatFrame\\ChatFrameBackground",
--        edgeFile = "Interface\\ChatFrame\\ChatFrameBackground",
--        tile = true,
--        tileSize = 1,
--        edgeSize = 1,
--    })

--    frame:SetBackdropColor( 0.1, 0.1, 0.1, 0.6 )
--    frame:SetBackdropBorderColor( 0.1, 0.1, 0.1, 0.8 )
    frame:SetFrameStrata( "HIGH" )
    frame:SetMovable( false )
    frame:EnableMouse( false )
    frame:SetClampedToScreen( false )

    Update()

--    local label = frame:CreateFontString( nil, "OVERLAY" )
--    label:SetFont( "FONTS\\FRIZQT__.TTF", 9, "OUTLINE" )
--    label:SetPoint( "CENTER", 1, 0 )
--    label:SetText( "MB" )

    M:SetSize( frame, 23, 20 )
    M:Show( frame )
end

function M.Initialize()
    CreateAnchor()
    M:OnFirstEnterWorld( Update )
    M:OnFarmingMode( Update )
end
