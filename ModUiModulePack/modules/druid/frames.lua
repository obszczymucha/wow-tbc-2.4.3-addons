---@diagnostic disable: undefined-global
local M = ModUi:NewModule( "druid/frames", { "Class", "ChatLeftSide" } )
local api = ModUi.facade.api

local m_recount_frame
local m_topleft_anchor
local m_cooldown_tracker_anchor

local Move = ModUi.utils.MoveFrameByPoint

local function create_anchor_frame( name )
  local frame = api.CreateFrame( "Frame", name )
  frame:SetWidth( 1 )
  frame:SetHeight( 1 )
  frame:Show()
  return frame
end

local function create_anchors()
  m_topleft_anchor = create_anchor_frame( "TopLeftAnchor" )
  m_cooldown_tracker_anchor = create_anchor_frame( "CooldownTrackerAnchor" )
end

local function reset_topleft_anchor()
  Move( m_topleft_anchor, { "TOPLEFT", UIParent, "TOPLEFT", 17, -18 } )
end

local function on_chat_left_side()
  if M:IsChatLeftSide() then
    Move( m_topleft_anchor, { "TOPLEFT", ChatFrame1, "BOTTOMLEFT", 1, -36 } )
  else
    reset_topleft_anchor()
  end
end

local function position_dbm_timers()
  if Recount_MainWindow and Recount_MainWindow:IsVisible() then
    Move( DBM_StatusBarTimerAnchor, { "TOPLEFT", TopLeftAnchor, "TOPRIGHT", 237, -9 } )
  else
    if EssentialCooldowns and EssentialCooldowns.get_visible_cooldown_count() > 0 then
      Move( DBM_StatusBarTimerAnchor, { "TOPLEFT", CooldownTrackerAnchor, "TOPRIGHT", 226, -18 } )
    else
      Move( DBM_StatusBarTimerAnchor, { "TOPLEFT", TopLeftAnchor, "TOPLEFT", 75, -8 } )
    end
  end
end

local function position_cooldown_tracker_anchor()
  if m_recount_frame:IsVisible() then
    Move( m_cooldown_tracker_anchor, { "TOPLEFT", Recount_MainWindow, "BOTTOMLEFT", 0, 0 } )
  else
    Move( m_cooldown_tracker_anchor, { "TOPLEFT", m_topleft_anchor, "BOTTOMLEFT", 0, 9 } )
  end

  position_dbm_timers()
end

local function OnFirstEnterWorld()
  m_recount_frame = Recount_MainWindow

  reset_topleft_anchor()
  position_cooldown_tracker_anchor()

  Move( m_recount_frame, { "TOPLEFT", m_topleft_anchor, "TOPLEFT", 2, 7 } )
  Move( OmenAnchor, { "TOPRIGHT", SUFUnitplayer, "TOPLEFT", 0, 43 } )

  M:SetScale( UIErrorsFrame, 0.59413599967957 )
  M:Hide( UIErrorsFrame )

  Move( SUFHeaderraid, { "TOPRIGHT", SUFUnitplayer, "BOTTOMRIGHT", 0, -35 } )

  m_recount_frame:HookScript( "OnShow", position_cooldown_tracker_anchor )
  m_recount_frame:HookScript( "OnHide", position_cooldown_tracker_anchor )

  m_recount_frame:SetWidth( 145 )
  Recount:ResizeMainWindow()

  Move( EssentialCooldownsFrame, { "TOPLEFT", CooldownTrackerAnchor, "BOTTOMLEFT", 0, -10 } )

  if EssentialCooldowns then
    EssentialCooldowns.register_for_updates( position_dbm_timers )
  end
end

local function on_group_changed()
  position_dbm_timers()
end

function M.Initialize()
  if not M:IsDruid() then
    M.enabled = false
    return
  end

  create_anchors()

  M:OnFirstEnterWorld( OnFirstEnterWorld )
  M:OnChatLeftSide( on_chat_left_side )
  M:OnGroupChanged( on_group_changed )
end
