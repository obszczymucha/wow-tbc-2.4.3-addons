local M = ModUi:NewModule( "guildchat" )
local frame = ChatFrame5

local function MoveAndSize()
	M:MoveFrameByPoint( frame, { "BOTTOMRIGHT", UIParent, "BOTTOMRIGHT", -18, 23 } )
	M:SetWidth( frame, 390 )
	M:SetHeight( frame, 74 )
end

function M.Initialize()
	M:OnEnterWorld( MoveAndSize )
end
