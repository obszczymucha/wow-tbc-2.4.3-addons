local M = ModUi:NewModule( "scryershandin" )
if not M then return end

local NPC_NAME = "Voren'thal the Seer"

local function OnQuestProgress()
  if UnitName( "target" ) ~= NPC_NAME then
    return
  end

  CompleteQuest()
end

local function OnQuestComplete()
  if UnitName( "target" ) ~= NPC_NAME then
    return
  end

  GetQuestReward( QuestFrameRewardPanel.itemChoice )
end

function M.Initialize()
  M:OnQuestProgress( OnQuestProgress )
  M:OnQuestComplete( OnQuestComplete )
end
