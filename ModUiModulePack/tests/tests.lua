local function ParseArgs( args )
    for player, note in ( args ):gmatch "(%w+)%s?(.*)" do
        return player, note ~= "" and note or nil
    end

    return nil, nil
end

local function test( testName, f )
    if not f() then
        error( string.format( "%s FAILED", testName ) )
    else
        print( string.format( "%s PASSED", testName ) )
    end
end

local function eq( l, r )
    if tostring( l ) == tostring( r ) then return true end

    print( string.format( "Expected: %s but was: %s", r, l ) )
    return false
end

function CountElements( t, f )
    local result = 0

    for _, v in pairs( t ) do
        if f and f( v ) or not f then
            result = result + 1
        end
    end

    return result
end

test( "parse player and note", function()
    player, note = ParseArgs( "Psikutas is a cunt." )

    return eq( player, "Psikutas" ) and eq( note, "is a cunt." )
end )

test( "parse player without a note", function()
    player, note = ParseArgs( "Psikutas" )

    return eq( player, "Psikutas" ) and eq( note, nil )
end )

test( "parse player with empty note", function()
    player, note = ParseArgs( "Psikutas " )

    return eq( player, "Psikutas" ) and eq( note, nil )

end )

test( "parse empty string", function()
    player, note = ParseArgs( "" )

    return eq( player, nil ) and eq( note, nil )
end )

local function ParsePlayer( playerLink )
    for player in ( playerLink ):gmatch "|Hplayer:(.+)|h%[.+%]|h has come online." do
        return player
    end

    return nil
end


test( "parse player has come online", function()
    player = ParsePlayer( "|Hplayer:Haxxramas|h[Haxxramas]|h has come online." )

    return eq( player, "Haxxramas" )
end )

test( "parse player has come online", function()
    player = ParsePlayer( "|Hplayer:Yarâ|h[Yarâ]|h has come online." )

    return eq( player, "Yarâ" )
end )

local function ParseItemThatSold( message )
	for itemName in ( message ):gmatch "A buyer has been found for your auction of (.+)%." do
		return itemName
	end

	return nil
end

test( "parse item that sold", function()
    local itemName = ParseItemThatSold( "A buyer has been found for your auction of Elixir of the Mongoose." )

    return eq( itemName, "Elixir of the Mongoose" )
end )

local function ParseItemThatHasBeenOutbid( message )
	for itemName in ( message ):gmatch "You have been outbid on (.+)%." do
		return itemName
	end

	return nil
end

test( "parse item that have been outbid", function()
    local itemName = ParseItemThatHasBeenOutbid( "You have been outbid on Elixir of the Mongoose." )

    return eq( itemName, "Elixir of the Mongoose" )
end )

test( "should uppercase", function()
    return eq( string.upper( "test" ), "TEST" )
end )

function sum( x, y )
    return x + y
end

test( "should add two numbers", function()
    return eq( sum( 2, 3 ), 5 )
end )

test( "should match Two-Handed Maces", function()
    local message = "Your skill in Two-Handed Maces has increased to 324."
    for skill, level in (message):gmatch "Your skill in ([%w%-%s]+) has increased to (%d+)." do
        return eq( skill, "Two-Handed Maces" ) and eq ( level, 324 )
    end

    return false
end )