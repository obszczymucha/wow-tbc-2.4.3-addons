## Interface: 20400
## Title: ModUi Module Pack
## Author: Obszczymucha
## Version: 1.0
## Notes: A pack of modules and extensions for ModUi.
## OptionalDeps: ModUi
## SavedVariables: ModUiDb

ModUiStrings.lua
extensions\Class.lua
extensions\ShadowUFTarget.lua
extensions\ShadowUF.lua
extensions\FarmingMode.lua
extensions\ForceRaid.lua
extensions\WarriorShouts.lua
extensions\TimerFormat.lua
extensions\FarmingAnchor.lua
extensions\TankTarget.lua
extensions\Debug.lua
extensions\ChatLeftSide.lua
extensions\WarriorMode.lua
extensions\ToggleMinimap.lua
extensions\OutfitterEvents.lua
modules\unitframes.lua
modules\minimap.lua
modules\chat.lua
modules\xpbarnone.lua
modules\hidebuffframe.lua
modules\togglequesttracker.lua
modules\music.lua
modules\zoneinfo.lua
modules\errorsframe.lua
modules\druidstancebutton.lua
modules\leavevehiclebutton.lua
modules\auctionnotify.lua
modules\swimbar.lua
modules\actionbars.lua
modules\missingbuffsanchor.lua
modules\questtracker.lua
modules\herbs.lua
modules\ohhaimark.lua
modules\trackitems.lua
modules\discord.lua
modules\test.lua
modules\itemid.lua
modules\raidmanager.lua
modules\skillincreased.lua
modules\target.lua
modules\warrior.lua
modules\warriorshoutmacro.lua
modules\friendinvite.lua
modules\threatmeter.lua
modules\damagemeter.lua
modules\castingbar.lua
modules\playernotes.lua
modules\farmingtimer.lua
modules\auctionsold.lua
modules\zonemap.lua
modules\minimapicons.lua
modules\tanklog.lua
modules\diedfromcrush.lua
modules\combatlog.lua
modules\characterframeanchor.lua
modules\druid.lua
modules\privatechat.lua
modules\nameplates.lua
modules\tooltip.lua
modules\moteextractor.lua
modules\dkmacros.lua
#modules\honortracker.lua
# modules\honortokenhandin.lua
modules\misctext.lua
modules\instancereset.lua
modules\battlefieldminimapnodes.lua
modules\pulltimer.lua
modules\omen.lua
modules\chatfilter.lua
modules\moveableframes.lua
modules\bgtimer.lua
modules\ctc.lua
modules\scryershandin.lua
# modules\interrupted.lua
modules\shitlist.lua
modules\aldorhandin.lua
modules\blizzardui.lua
modules\druid\frames.lua
modules\warrior\frames.lua
modules\warlock\frames.lua
modules\modui.lua
modules\raidcooldowns.lua
