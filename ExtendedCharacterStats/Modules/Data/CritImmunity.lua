---@type Data
local Data = ECSLoader:ImportModule("Data")
---@type DataUtils
local DataUtils = ECSLoader:ImportModule("DataUtils")

---@return number
function Data:GetCritByChanceValue(againstLevel)
    local baseDefense, armorDefense = UnitDefense("player");
    local talentName = select(1, GetTalentInfo(2,16,1)) -- Change numbers to match what you  want (tier, column, spec)
    local druidBonus = (talentName == "Survival of the Fittest") and select(5, GetTalentInfo(2,16,1)) or 0
    local baseCrit = (baseDefense + armorDefense) * 0.04 + GetCombatRatingBonus(16) + druidBonus

    local critChance = (againstLevel == 73 and (19.6 - baseCrit)) or
                       (againstLevel == 72 and (19.4 - baseCrit)) or
                       (againstLevel == 71 and (19.2 - baseCrit)) or
                       (19 - baseCrit) -- default lvl 70

    return critChance
end
