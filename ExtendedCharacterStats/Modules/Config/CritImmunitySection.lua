---@type Config
local Config = ECSLoader:ImportModule("Config")
local _Config = Config.private

---@type Stats
local Stats = ECSLoader:ImportModule("Stats")
---@type i18n
local i18n = ECSLoader:ImportModule("i18n")

function _Config:LoadCritImmunitySection()
    return {
        type = "group",
        order = 4,
        inline = false,
        width = '2',
        name = function() return i18n("CRIT_IMMUNITY") end,
        args = {
            showDefenseStats = {
                type = "toggle",
                order = 0,
                name = function() return i18n("CRIT_IMMUNITY_SETTINGS") end,
                desc = function() return i18n("CRIT_IMMUNITY_SETTINGS_DESC") end,
                width = '1.5',
                get = function () return ExtendedCharacterStats.profile.critImmunity.display; end,
                set = function (_, value)
                    ExtendedCharacterStats.profile.critImmunity.display = value
                    Stats:RebuildStatInfos()
                end,
            },
            againstLvl70 = {
                type = "toggle",
                order = 1,
                name = function() return i18n("AGAINST_LVL_70_SETTING") end,
                desc = function() return i18n("AGAINST_LVL_70_SETTING_DESC") end,
                width = '1.5',
                disabled = function() return (not ExtendedCharacterStats.profile.critImmunity.display); end,
                get = function () return ExtendedCharacterStats.profile.critImmunity.againstLvl70.display; end,
                set = function (_, value)
                    ExtendedCharacterStats.profile.critImmunity.againstLvl70.display = value
                    Stats:RebuildStatInfos()
                end,
            },
            againstLvl73 = {
                type = "toggle",
                order = 2,
                name = function() return i18n("AGAINST_BOSSES_SETTING") end,
                desc = function() return i18n("AGAINST_BOSSES_SETTING_DESC") end,
                width = '1.5',
                disabled = function() return (not ExtendedCharacterStats.profile.critImmunity.display); end,
                get = function () return ExtendedCharacterStats.profile.critImmunity.againstBosses.display; end,
                set = function (_, value)
                    ExtendedCharacterStats.profile.critImmunity.againstBosses.display = value
                    Stats:RebuildStatInfos()
                end,
            }
        },
    }
end
