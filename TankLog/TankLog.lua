TankLog = LibStub("AceAddon-3.0"):NewAddon("TankLog", "AceConsole-3.0", "AceTimer-3.0")
TankLog.debugFrame = ChatFrame3
TankLog.config = {
  debug = true,
  debugSpells = true
}

local PLAYER_NAME = UnitName("player")
local tankLogFrame = ChatFrame4
local showCrits = false
local hitCount = 0
local physicalCritCount = 0
local nonPhysicalCritCount = 0
local dodgeCount = 0
local parryCount = 0
local blockCount = 0
local crushCount = 0
local missCount = 0
local myHitCount = 0
local myCritCount = 0
local myMissCount = 0
local myParryCount = 0
local myDodgeCount = 0
local combatName = nil
local RED = {}
RED.red = 255
RED.green = 0
RED.blue = 0
local CRIT_COLOR = RED
local inCombat = false

local function ResetDamageCounters()
  hitCount = 0
  physicalCritCount = 0
  nonPhysicalCritCount = 0
  dodgeCount = 0
  parryCount = 0
  blockCount = 0
  crushCount = 0
  missCount = 0
  myHitCount = 0
  myCritCount = 0
  myMissCount = 0
  myParryCount = 0
  myDodgeCount = 0
  combatName = nil
end

TankLog.SystemPrint = TankLog.Print

local function DebugMsg(message)
  if not TankLog.config.debug then return end
  TankLog.debugFrame:AddMessage(format("|cff33ff99TankLog|r: %s", message))
end

local function DebugLine()
  TankLog.debugFrame:AddMessage(" ")
end

function TankLog.Print(_, message)
  tankLogFrame:AddMessage(message)
end

local function PrintTankStats(message, color)
  if not tankLogFrame then return end

  if color then
    tankLogFrame:AddMessage(message, color.red, color.green, color.blue)
  else
    tankLogFrame:AddMessage(message, 1, 0.8, 0.8, 1, 5)
  end
end

local function Engage(name)
  if combatName then return end
  combatName = name

  if inCombat then return end

  if tankLogFrame then
    tankLogFrame:SetTimeVisible(3)
  end

  inCombat = true
  PrintTankStats(" ")
  PrintTankStats(format("Engaged |cff33ff99%s|r!", name or "something"))

  for i = 1, 5 do PrintTankStats(" ") end
end

local function ReportDamageCounters()
  local crits = physicalCritCount + nonPhysicalCritCount
  local avoids = dodgeCount + parryCount + blockCount + missCount
  local total = hitCount + crits + crushCount + avoids
  local myMisses = myMissCount + myParryCount + myDodgeCount
  local myTotal = myHitCount + myCritCount + myMisses
  local lines = 0

  if total == 0 and myTotal == 0 then return end

  if tankLogFrame then
    tankLogFrame:SetTimeVisible(15)
  end

  PrintTankStats(" ")
  PrintTankStats(format("Stats for |cff33ff99%s|r:", combatName))
  lines = lines + 1

  if hitCount > 0 then
    local message = format("Received |cffffffff%d|r melee hit", hitCount)

    if hitCount > 1 then
      message = message .. "s"
    end

    message = message .. "."

    PrintTankStats(message)
    lines = lines + 1
  end

  if avoids > 0 then
    local message = format("Avoided |cffffffff%d|r hit", avoids)

    if avoids > 1 then
      message = message .. "s"
    end

    message = message .. format(" (|cffffffff%2d%%|r): ", (avoids / total) * 100)

    local needsComa = false

    if dodgeCount > 0 then
      message = message .. format("|cffffffff%d|r dodged", dodgeCount)
      needsComa = true
    end

    if parryCount > 0 then
      if needsComa then
        message = message .. ", "
      end

      message = message .. format("|cffffffff%d|r parried", parryCount)
      needsComa = true
    end

    if blockCount > 0 then
      if needsComa then
        message = message .. ", "
      end

      message = message .. format("|cffffffff%d|r blocked", blockCount)
      needsComa = true
    end

    if missCount > 0 then
      if needsComa then
        message = message .. ", "
      end

      message = message .. format("|cffffffff%d|r missed", missCount)
      needsComa = true
    end

    message = message .. "."

    PrintTankStats(message)
    lines = lines + 1
  end

  if crits > 0 then
    local critsBreakdown = ""
    local needsComa = false

    if physicalCritCount > 0 then
      critsBreakdown = critsBreakdown .. format("%d |cffffffffphysical|r", physicalCritCount)
      needsComa = true
    end

    if nonPhysicalCritCount > 0 then
      if needsComa then
        critsBreakdown = critsBreakdown .. ", "
      end

      critsBreakdown = critsBreakdown .. format("%d |cffffffffnon-physical|r", nonPhysicalCritCount)
    end

    local message = format("Received crits: |cffff2020%d|r (%s)", crits, critsBreakdown)

    PrintTankStats(message)
    lines = lines + 1
  end

  if crushCount > 0 then
    PrintTankStats(format("Received crushes: |cffdf2020%d|r", crushCount))
    lines = lines + 1
  end

  if myTotal > 0 then
    local message = format("Dealt ")

    if myHitCount > 0 or (myHitCount == 0 and myCritCount > 0) then
      message = message .. format("|cffffffff%d|r hit", myHitCount)

      if myHitCount == 0 or myHitCount > 1 then
        message = message .. "s"
      end
    end

    if myCritCount > 0 then
      message = message .. format(", |cffffffff%d|r crit", myCritCount)

      if myCritCount > 1 then
        message = message .. "s"
      end
    end

    if myMisses > 0 then
      message = message .. " ("
      local needsComa = false

      if myMissCount > 0 then
        message = message .. format("+|cffffffff%d|r missed", myMissCount)
        needsComa = true
      end

      if myParryCount > 0 then
        if needsComa then
          message = message .. ", "
        else
          message = message .. "+"
        end

        message = message .. format("|cffffffff%d|r parried", myParryCount)
        needsComa = true
      end

      if myDodgeCount > 0 then
        if needsComa then
          message = message .. ", "
        else
          message = message .. "+"
        end

        message = message .. format("|cffffffff%d|r dodged", myDodgeCount)
      end

      if myMisses > 0 then
        message = message .. ")"
      end
    end

    message = message .. "."

    PrintTankStats(message)
    lines = lines + 1
  end

  for i = 1, 6 - lines do PrintTankStats(" ") end

  tankLogFrame:Hide()
  tankLogFrame:Show()
end

local function MissToString(missType)
  if missType == "DODGE" then
    return "was dodged"
  elseif missType == "PARRY" then
    return "was parried"
  elseif missType == "BLOCK" then
    return "was blocked"
  elseif missType == "MISS" then
    return "missed"
  elseif missType == "RESIST" then
    return "resisted"
  elseif missType == "ABSORB" then
    return "was absorbed"
  elseif missType == "IMMUNE" then
    return "didn't hit (|cffffffffImmune|r)"
  else
    return format("didn't hit (|cffffffff%s|r)", missType)
  end
end

local function OnSpellMissed(spellName, sourceName, destName, missType)
  if destName == PLAYER_NAME then
    if missType == "DODGE" then
      dodgeCount = dodgeCount + 1
    elseif missType == "PARRY" then
      parryCount = parryCount + 1
    elseif missType == "BLOCK" then
      blockCount = blockCount + 1
    elseif missType == "MISS" then
      missCount = missCount + 1
    end

    Engage(sourceName)

    return
  end

  if spellName == "Taunt" then
    if sourceName == PLAYER_NAME and missType ~= "IMMUNE" then
      SendChatMessage(format("%s resisted my taunt.", destName), "SAY")
      PrintTankStats(format("My |cffffffff%s|r %s.", spellName, MissToString(missType)))
    else
      PrintTankStats(format("|cffffff99%s|r missed |cfffffffftaunt|r on |cffffff99%s|r (|cffffffff%s|r).", sourceName,
        destName, missType))
    end

    return
  end

  if sourceName ~= PLAYER_NAME then return end

  Engage(destName)

  if missType == "DODGE" then
    myDodgeCount = myDodgeCount + 1
  elseif missType == "PARRY" then
    myParryCount = myParryCount + 1
  elseif missType == "MISS" then
    myMissCount = myMissCount + 1
  end

  PrintTankStats(format("My |cffffffff%s|r %s.", spellName, MissToString(missType)))
end

local function OnSwingMissed(sourceName, destName, missType)
  if sourceName == PLAYER_NAME then
    if missType == "DODGE" then
      myDodgeCount = myDodgeCount + 1
    elseif missType == "PARRY" then
      myParryCount = myParryCount + 1
    elseif missType == "MISS" then
      myMissCount = myMissCount + 1
    end

    PrintTankStats(format("My swing %s.", MissToString(missType)))

    Engage(destName)

    return
  end

  if destName ~= PLAYER_NAME then return end

  Engage(sourceName)

  if missType == "DODGE" then
    dodgeCount = dodgeCount + 1
  elseif missType == "PARRY" then
    parryCount = parryCount + 1
  elseif missType == "BLOCK" then
    blockCount = blockCount + 1
  elseif missType == "MISS" then
    missCount = missCount + 1
  end
end

local function OnRangeMissed(sourceName, destName, missType)
  if sourceName == PLAYER_NAME then
    Engage(destName)
  end
end

local function SchoolToString(school)
  if not school then
    return "N/A"
  elseif school == 1 then
    return "Physical"
  elseif school == 2 then
    return "Holy"
  elseif school == 4 then
    return "Fire"
  elseif school == 8 then
    return "Nature"
  elseif school == 16 then
    return "Frost"
  elseif school == 32 then
    return "Shadow"
  elseif school == 64 then
    return "Arcane"
  else
    return format("Unknown: %d", school)
  end
end

local function DebugResists(resisted, blocked, absorbed, critical, glancing, crushing)
  local result = ""
  local needsComa = false

  if resisted then
    result = result .. format("resisted: |cffff9f69%s|r", resisted)
    needsComa = true
  end

  if blocked then
    if needsComa then result = result .. ", " end
    result = result .. format("blocked: |cffff9f69%s|r", blocked)
    needsComa = true
  end

  if absorbed then
    if needsComa then result = result .. ", " end
    result = result .. format("absorbed: |cffff9f69%s|r", absorbed)
    needsComa = true
  end

  if critical then
    if needsComa then result = result .. ", " end
    result = result .. format("critical: |cffff9f69%s|r", critical)
    needsComa = true
  end

  if glancing then
    if needsComa then result = result .. ", " end
    result = result .. format("glancing: |cffff9f69%s|r", glancing)
    needsComa = true
  end

  if crushing then
    if needsComa then result = result .. ", " end
    result = result .. format("crushing: |cffff9f69%s|r", crushing)
    needsComa = true
  end

  if result ~= "" then
    DebugMsg(result)
  end
end

local function DebugSpellParams(spellName, sourceName, destName, amount, school, resisted, blocked, absorbed, critical,
                                glancing, crushing)
  if not TankLog.config.debugSpells then return end

  DebugMsg(format("spellName: |cffff9f69%s|r", spellName or "N/A"))
  DebugMsg(format("sourceName: |cffff9f69%s|r, destName: |cffff9f69%s|r", sourceName or "N/A", destName or "N/A"))
  DebugMsg(format("amount: |cffff9f69%s|r, school: |cffff9f69%s|r", amount or "N/A", school or "N/A"))
  DebugResists(resisted, blocked, absorbed, critical, glancing, crushing)
  DebugLine()
end

local function OnSpellDamage(spellName, sourceName, destName, amount, school, resisted, blocked, absorbed, critical,
                             glancing, crushing)
  if sourceName == PLAYER_NAME then
    DebugSpellParams(spellName, sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing,
      crushing)

    if critical then
      if showCrits then
        PrintTankStats(format("My |cffffffff%s|r critted.", spellName))
      end

      myCritCount = myCritCount + 1
    else
      myHitCount = myHitCount + 1
    end

    Engage(destName)

    return
  end

  if destName ~= PLAYER_NAME then return end

  Engage(sourceName)
  DebugSpellParams(spellName, sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing,
    crushing)

  if critical then
    PrintTankStats(format("|cffff2020Critted|r by |cffffffff%s|r's |cffffffff%s|r (|cffffffff%s|r).", sourceName or "N/A",
      spellName or "N/A", SchoolToString(school)))

    if school == 1 then
      physicalCritCount = physicalCritCount + 1
    else
      nonPhysicalCritCount = nonPhysicalCritCount + 1
    end
  elseif crushing then
    PrintTankStats(format("|cffdf2020Crushed|r by |cffffffff%s|r's |cffffffff%s|r!", sourceName or "N/A",
      spellName or "N/A"))
    crushCount = crushCount + 1
  elseif blocked then
    blockCount = blockCount + 1
  elseif school == 1 then
    hitCount = hitCount + 1
  end

  --	if amount then ProfileSwitcher:Print( "Hit amount: " .. amount ) end
  --	if resisted then ProfileSwitcher:Print( "Hit resisted: " .. resisted ) end
  --	if blocked then ProfileSwitcher:Print( "Hit blocked: " .. blocked ) end
  --	if absorbed then ProfileSwitcher:Print( "Hit absorbed: " .. absorbed ) end
  --	if critical then ProfileSwitcher:Print( "Hit critical: " .. critical ) end
  --	if glancing then ProfileSwitcher:Print( "Hit glancing: " .. glancing ) end
  --	if crushing then ProfileSwitcher:Print( "Hit crushing: " .. crushing ) end
end

local function OnSwingDamage(sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing,
                             crushing)
  if sourceName == PLAYER_NAME then
    if critical then
      if showCrits then
        PrintTankStats("My swing critted.")
      end

      myCritCount = myCritCount + 1
    else
      myHitCount = myHitCount + 1
    end

    Engage(destName)

    return
  end

  if destName ~= PLAYER_NAME then return end

  Engage(sourceName)

  if critical then
    PrintTankStats(format("|cffff2020Critted|r by |cffffffff%s|r's swing (|cffffffff%s|r).", sourceName,
      SchoolToString(school)))

    if school == 1 then
      physicalCritCount = physicalCritCount + 1
    else
      nonPhysicalCritCount = nonPhysicalCritCount + 1
    end
  elseif crushing then
    PrintTankStats(format("|cffdf2020Crushed|r by |cffffffff%s|r's swing!", sourceName))
    crushCount = crushCount + 1
  elseif blocked then
    blockCount = blockCount + 1
  else
    hitCount = hitCount + 1
  end

  DebugResists(resisted, blocked, absorbed, critical, glancing, crushing)
  --	if amount then ProfileSwitcher:Print( "Swing amount: " .. amount ) end
  --	if resisted then ProfileSwitcher:Print( "Swing resisted: " .. resisted ) end
  --	if blocked then ProfileSwitcher:Print( "Swing blocked: " .. blocked ) end
  --	if absorbed then ProfileSwitcher:Print( "Swing absorbed: " .. absorbed ) end
  --	if critical then ProfileSwitcher:Print( "Swing critical: " .. critical ) end
  --	if glancing then ProfileSwitcher:Print( "Swing glancing: " .. glancing ) end
end

local function OnRangeDamage(sourceName, destName, amount, school, resisted, blocked, absorbed, critical, glancing,
                             crushing)
  if sourceName == PLAYER_NAME then
    Engage(destName)
  end
end

local function OnCombatLogUnfiltered(_, event, _, sourceName, _, _, destName, _, arg9, arg10, arg11, arg12, arg13, arg14,
                                     arg15, arg16, arg17, arg18, arg19, arg20)
  if event == "SPELL_MISSED" then
    OnSpellMissed(arg10, sourceName, destName, arg12)
  elseif event == "SWING_MISSED" then
    OnSwingMissed(sourceName, destName, arg9)
  elseif event == "RANGE_MISSED" then
    OnRangeMissed(sourceName, destName, arg9)
  elseif event == "SPELL_DAMAGE" then
    OnSpellDamage(arg10, sourceName, destName, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20)
  elseif event == "SWING_DAMAGE" then
    OnSwingDamage(sourceName, destName, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20)
  elseif event == "RANGE_DAMAGE" then
    OnRangeDamage(sourceName, destName, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20)
  end
end

local function eventHandler(self, event, ...)
  if event == "PLAYER_REGEN_DISABLED" then
    ResetDamageCounters()
  elseif event == "PLAYER_REGEN_ENABLED" then
    inCombat = false
    ReportDamageCounters()
  elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
    OnCombatLogUnfiltered(...)
  end
end

local function PrintShowingCrits()
  if showCrits then
    TankLog:SystemPrint("Showing crits.")
  else
    TankLog:SystemPrint("Not showing crits.")
  end
end

function TankLog.OnSlashCommand(msg)
  if not msg or msg == "" then
    PrintShowingCrits()
  end

  if msg == "toggle" then
    showCrits = not showCrits
    PrintShowingCrits()
  elseif msg == "debug" then
    TankLog.config.debug = not TankLog.config.debug

    if TankLog.config.debug then
      TankLog:SystemPrint("Debug: on")
    else
      TankLog:SystemPrint("Debug: off")
    end
  end
end

SLASH_TANKLOG1 = "/tl"
SlashCmdList["TANKLOG"] = TankLog.OnSlashCommand

local frame = CreateFrame("FRAME", "TankLogFrame")
frame:RegisterEvent("PLAYER_REGEN_DISABLED")
frame:RegisterEvent("PLAYER_REGEN_ENABLED")
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
frame:RegisterEvent("UNIT_THREAT_LIST_UPDATE")
frame:RegisterEvent("UNIT_THREAT_SITUATION_UPDATE")
frame:SetScript("OnEvent", eventHandler)
