------------------------------------------------------------------------
r45043 | kaelten | 2007-07-27 23:16:37 -0400 (Fri, 27 Jul 2007) | 1 line
Changed paths:
   M /trunk/OneBag/OneCore.lua

OneBag: removed changes.  Forcing frame strata to 'fix' a conflict between mods is not a good solution.  
------------------------------------------------------------------------
r44996 | belazor | 2007-07-27 12:28:45 -0400 (Fri, 27 Jul 2007) | 2 lines
Changed paths:
   M /trunk/OneBag/OneCore.lua

OneBag: 
- Fixed grayed out bag problem. Thanks to Varinul of WoWInterface for the fix.
------------------------------------------------------------------------
r42263 | funkydude | 2007-06-29 14:59:21 -0400 (Fri, 29 Jun 2007) | 1 line
Changed paths:
   M /trunk/OneBag
   M /trunk/OneBag/OneBag.toc
   A /trunk/OneBag/embeds.xml

OneBag: use embeds.xml
------------------------------------------------------------------------
