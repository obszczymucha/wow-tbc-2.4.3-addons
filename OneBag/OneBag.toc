﻿## Id: $Id: OneBag.toc 67228 2008-03-31 07:29:40Z xan $
## Interface: 20400
## Title: OneBag |cff7fff7f -Ace2-|r
## Notes: OneBag is a replacement for the default game bags that combines all the bags into one frame.
## Notes-zhCN: 整合背包插件，以整合方式显示你的背包。
## Notes-zhTW: 整合所有背包在一起。
## Notes-deDE: Eine Tasche um sie alle zu beherrschen, und in der Dunkelheit zu binden.
## Version: 2.0.$Revision: 67228 $
## Author: Kaelten
## X-Date: $Date: 2008-03-31 03:29:40 -0400 (Mon, 31 Mar 2008) $
## X-Category: Frame Modification
## X-eMail: kaelten@gmail.com
## X-Website: http://www.wowace.com
## X-RelSite-WoWI: 4207
## X-WoWIPortal: kaelten

## SavedVariables: OneBagDB

## OptionalDeps: Ace2, DewdropLib
## X-Embeds: Ace2, DewdropLib

embeds.xml

localizations\locals.xml

templates.xml
OneCore.lua

OneBag.xml
OneBag.lua
